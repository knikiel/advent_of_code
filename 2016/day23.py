import re
from copy import copy

def compute(instructions_, a_value = 0):
    pc = 0
    instructions = copy(instructions_)
    regs = {'a':a_value, 'b':0, 'c':0, 'd':0}
    counter = 0
    while pc < len(instructions):
        counter += 1
        if counter % 1000000 == 0:
            print(pc, instructions)
            print(regs)
        instruction = instructions[pc]
        m = re.search("cpy (-?(\w+)) (\w+)", instruction)
        if m:
            if m.group(2).isdigit():
                value = int(m.group(1))
            else:
                value = regs[m.group(1)]
            if m.group(3).isdigit():
                continue
            else:
                reg = m.group(3)
            regs[reg] = value
            pc += 1
            continue
        m = re.search("inc (\w)", instruction)
        if m:
            if m.group(1).isdigit():
                continue
            else:
                reg = m.group(1)
            regs[reg] += 1
            pc += 1
            continue
        m = re.search("dec (\w)", instruction)
        if m:
            if m.group(1).isdigit():
                continue
            else:
                reg = m.group(1)
            regs[reg] -= 1
            pc += 1
            continue
        m = re.search("jnz (\w) (\-?(\w+))", instruction)
        if m:
            # geschummelte Spezialfälle
            if (instruction == "jnz c -2" and pc-2 >= 0 and
                instructions[pc-1] == "dec c" and
                instructions[pc-2] == "inc a"):
                regs['a'] += regs['c']
                regs['c'] = 0
                pc += 1
                continue
            elif (instruction == "jnz d -2" and pc-2 >= 0 and
                instructions[pc-1] == "inc c" and
                instructions[pc-2] == "dec d"):
                regs['c'] += regs['d']
                regs['d'] = 0
                pc += 1
                continue
            elif (instruction == "jnz d -5" and pc-5 >= 0 and
                    instructions[pc-1] == "dec d" and
                    instructions[pc-2] == "jnz c -2" and
                    instructions[pc-3] == "dec c" and
                    instructions[pc-4] == "inc a" and
                    instructions[pc-5] == "cpy b c"):
                regs['a'] += regs['b'] * regs['d']
                regs['c'] = 0
                regs['d'] = 0

            # normaler Fall
            if m.group(3).isdigit():
                steps = int(m.group(2))
            else:
                steps = regs[m.group(2)]
            if m.group(1).isdigit():
                value = int(m.group(1))
            else:
                value = regs[m.group(1)]
            if value != 0:
                pc += steps
            else:
                pc += 1
            continue
        m = re.search("tgl (\w+)", instruction)
        if m:
            if m.group(1).isdigit():
                idx = int(m.group(1))
            else:
                idx = regs[m.group(1)]
            if pc + idx < 0 or pc + idx >= len(instructions):
                pc += 1
                continue
            old_instr = instructions[pc + idx]
            splits = old_instr.split(" ")
            if len(splits) == 2:
                if splits[0] == "inc":
                    new_instr = "dec " + splits[1]
                else:
                    new_instr = "inc " + splits[1]
            elif len(splits) == 3:
                if splits[0] == "jnz":
                    new_instr = "cpy " + splits[1] + " " + splits[2]
                else:
                    new_instr = "jnz " + splits[1] + " " + splits[2]
            else:
                print(splits)
                assert(False)
            instructions[pc + idx] = new_instr
            pc += 1
            #print(old_instr, new_instr)
            continue
        print(instruction)
        assert(False)
    #print(instructions)
    print(regs)
    return regs

if __name__ == "__main__":
    test_data = ["cpy 2 a", "tgl a", "tgl a", "tgl a", "cpy 1 a", "dec a", "dec a"]
    assert(compute(test_data)['a'] == 3)

    f = open("day23.input")
    instructions = [line.strip() for line in f.readlines()]
    regs = compute(instructions, 7)
    print("Inhalt von Register \'a\' (7 Eier):", regs['a'])
    regs = compute(instructions, 12)
    print("Inhalt von Register \'a\' (12 Eier):", regs['a'])
