from collections import Counter

def read_message(data):
    message_length = len(data[0])
    message = ""
    for i in range(0, message_length):
        letter_counts = Counter()
        for line in data:
            letter = line[i]
            letter_counts[letter] += 1
        most_common_letter = letter_counts.most_common(1)[0][0]    
        message += most_common_letter
    return message

def read_other_message(data):
    message_length = len(data[0])
    message = ""
    for i in range(0, message_length):
        letter_counts = Counter()
        for line in data:
            letter = line[i]
            letter_counts[letter] += 1
        most_common_letter = letter_counts.most_common()[-1][0]    
        message += most_common_letter
    return message

if __name__ == "__main__":
    test_input = ["eedadn", "drvtee", "eandsr", "raavrd", "atevrs", "tsrnev", "sdttsa", "rasrtv", "nssdts", "ntnada", "svetve", "tesnvt", "vntsnd", "vrdear", "dvrsen", "enarar"]
    assert(read_message(test_input) == "easter")
    assert(read_other_message(test_input) == "advent")

    f = open("day6.input")
    data = [line.strip() for line in f.readlines()]

    message = read_message(data)
    print("Ex 1: Rekonstruierte Nachricht:", message)

    message = read_other_message(data)
    print("Ex 2: Rekonstruierte Nachricht:", message)
