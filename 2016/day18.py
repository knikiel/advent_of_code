from collections import Counter

def count_safe_tiles(first_row, num_rows):
    last_row = '.' + first_row + '.'
    num_safe_tiles = Counter(first_row)['.']
    for j in range(num_rows-1):
        #print(last_row)
        new_row = "."
        for i in range(1, len(last_row)-1):
            lcr = last_row[i-1:i+2]
            if lcr == "^^." or lcr == "^.." or \
                    lcr == ".^^" or lcr == "..^":
                new_row += '^'
            else:
                num_safe_tiles += 1
                new_row += '.'
        last_row = new_row + "."
    #print(num_safe_tiles)
    return num_safe_tiles

assert(count_safe_tiles(".^^.^.^^^^", 10) == 38)

first_row = "......^.^^.....^^^^^^^^^...^.^..^^.^^^..^.^..^.^^^.^^^^..^^.^.^.....^^^^^..^..^^^..^^.^.^..^^..^^^.."
num_safe_tiles = count_safe_tiles(first_row, 40)
print("Anzahl sicherer Felder:", num_safe_tiles)
num_safe_tiles = count_safe_tiles(first_row, 400000)
print("Anzahl sicherer Felder, v2:", num_safe_tiles)
