import re
import itertools

def parse_instructions(lines):
    instructions = []
    for line in lines:
        m = re.search("swap position (\d+) with position (\d+)", line)
        if m:
            p1 = int(m.group(1))
            p2 = int(m.group(2))
            instructions.append(("sp", p1, p2))
            continue;
        m = re.search("swap letter (\w) with letter (\w)", line)
        if m:
            p1 = m.group(1)
            p2 = m.group(2)
            instructions.append(("sl", p1, p2))
            continue;
        m = re.search("rotate ((left)|(right)) (\d+) steps?", line)
        if m:
            p1 = m.group(1)
            p2 = int(m.group(4))
            instructions.append(("r" + p1[0], p2))
            continue;
        m = re.search("rotate based on position of letter (\w)", line)
        if m:
            p1 = m.group(1)
            instructions.append(("ro", p1))
            continue;
        m = re.search("reverse positions (\d+) through (\d+)", line)
        if m:
            p1 = int(m.group(1))
            p2 = int(m.group(2))
            instructions.append(("re", p1, p2))
            continue;
        m = re.search("move position (\d+) to position (\d+)", line)
        if m:
            p1 = int(m.group(1))
            p2 = int(m.group(2))
            instructions.append(("m", p1, p2))
            continue;
        print(line)
        assert(False)
    return instructions

def scramble(string, instructions):
    letters = list(string)
    num_letters = len(letters)
    p2 = None
    for instr in instructions:
        if len(instr) < 3:
            code, p1 = instr
        else:
            code, p1, p2 = instr
        #print(code, p1, p2)
        if code == "sp":
            old_letter = letters[p1]
            letters[p1] = letters[p2]
            letters[p2] = old_letter
        elif code == "sl":
            pos1 = letters.index(p1)
            pos2 = letters.index(p2)
            letters[pos1] = p2
            letters[pos2] = p1
        elif code == "rl":
            letters = letters[p1:] + letters[:p1]
        elif code == "rr":
            letters = letters[-p1:] + letters[:-p1]
        elif code == "ro":
            was_higher = False
            pos = letters.index(p1)
            if pos >= 4:
                was_higher = True
            letters = letters[-(pos+1):] + letters[:-(pos+1)]
            #print("1:", "".join(letters))
            #times = letters.index(p1)
            #for i in range(times):
            #letters = letters[-pos:] + letters[:-pos]
            #letters = letters[-pos:] + letters[:-pos]
            #for i in range(pos):
            #    letters = letters[-pos:] + letters[:-pos]
            #    if letters.index(p1) >= 4:
            #        was_higher = True
            #    print(i, "".join(letters))
            #pos2 = letters.index(p1)
            #if pos2 >= 4:
            #    was_higher = True
            if pos >= 4:
            #if pos2 >= 4:
            #if was_higher:
                #letters = letters[-pos:] + letters[:-pos]
                letters = letters[-1:] + letters[:-1]
                #print(">=4:", "".join(letters))
        elif code == "re":
            letters[p1:p2+1] = reversed(letters[p1:p2+1])
        elif code == "m":
            letter = letters[p1]
            del letters[p1]
            letters.insert(p2, letter)
        else:
            assert(False)
        #print("".join(letters))
    return "".join(letters)

def descramble(scrambled, instruction):
    for perm in itertools.permutations(list(scrambled)):
        if scrambled == scramble(perm, instructions):
            return "".join(perm)

test_instructions = [("sp", 4, 0), ("sl", 'd', 'b'), ("re", 0, 4),
        ("rl", 1), ("m", 1, 4), ("m", 3, 0), ("ro", 'b'), ("ro", 'd')]
test_string = "abcde"
test_scrambled = scramble(test_string, test_instructions)
assert(test_scrambled == "decab")
test_scrambled = scramble(test_string, [("rl", 2)])
assert(test_scrambled == "cdeab")

f = open("day21.input")
lines = f.readlines()
instructions = parse_instructions(lines)
scrambled = scramble("abcdefgh", instructions)
print("Scrambled input:", scrambled)
scrambled = "fbgdceah"
passcode = descramble(scrambled, instructions)
print("Descrambled input:", passcode)
