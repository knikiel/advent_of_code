def create_data(beginning, size):
    data = beginning
    while len(data) < size:
        a = data
        b = reversed(a[:])
        data = a + '0'
        for char in b:
            if char == '0':
                data += '1'
            else:
                data += '0'
    return data[:size]

def compute_checksum(data):
    old_checksum = data[:]
    while len(old_checksum) % 2 == 0:
        checksum = ""
        for i in range(len(old_checksum) // 2):
            c1, c2 = old_checksum[2*i:2*i+2]
            if c1 == c2:
                checksum += '1'
            else:
                checksum += '0'
        old_checksum = checksum
    return old_checksum

test_beginning = "10000"
test_data = create_data(test_beginning, 20)
assert(test_data == "10000011110010000111")
test_checksum = compute_checksum(test_data)
assert(test_checksum == "01100")

beginning = "00101000101111010"
data = create_data(beginning, 272)
checksum = compute_checksum(data)
print("Checksum für die erste Platte:", checksum)
data = create_data(beginning, 35651584)
checksum = compute_checksum(data)
print("Checksum für die zweite Platte:", checksum)
