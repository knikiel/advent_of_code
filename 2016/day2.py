def compute_code(data):
    keyboard_mapping = {
            (-1, -1): 1, (0, -1): 2, (1, -1): 3,
            (1, 0): 4, (0, 0): 5, (1, 0): 6,
            (-1, 1): 7, (0, 1): 8, (1, 1): 9}
    x_pos = 0
    y_pos = 0
    code = ""
    for line in data:
        for direction in line:
            if direction == 'U':
                y_pos = max(-1, y_pos-1)
            elif direction == 'D':
                y_pos = min(1, y_pos+1)
            elif direction == 'L':
                x_pos = max(-1, x_pos-1)
            elif direction == 'R':
                x_pos = min(1, x_pos+1)
        code += str(keyboard_mapping[(x_pos, y_pos)])
    return code

def compute_correct_code(data):
    keyboard_mapping = {
            (0, -2): '1',
            (-1, -1): '2', (0, -1): '3', (1, -1): '4',
            (-2, 0): '5', (1, 0): '6', (0, 0): '7', (1, 0): '8', (2, 0): '9',
            (-1, 1): 'A', (0, 1): 'B', (1, 1): 'C',
            (0, 2): 'D'}
    x_pos = -2
    y_pos = 0
    code = ""
    for line in data:
        for direction in line:
            if direction == 'U':
                y_pos = max(abs(x_pos)-2, y_pos-1)
            elif direction == 'D':
                y_pos = min(2-abs(x_pos), y_pos+1)
            elif direction == 'L':
                x_pos = max(abs(y_pos)-2, x_pos-1)
            elif direction == 'R':
                x_pos = min(2-abs(y_pos), x_pos+1)
        code += keyboard_mapping[(x_pos, y_pos)]
    return code

if __name__ == "__main__":
    f = open("day2.input")
    data = f.readlines()
    data = [line.strip() for line in data]

    assert(compute_code(["ULL", "RRDDD", "LURDL", "UUUUD"]) == "1985")

    code = compute_code(data)
    print("Code: ", code)

    assert(compute_correct_code(["ULL", "RRDDD", "LURDL", "UUUUD"]) == "5DB3")

    code = compute_correct_code(data)
    print("Correct code: ", code)
