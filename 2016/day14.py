import hashlib
import sys

def compute_hashes(salt, num_hashes, stretching = 0):
    idx = 0
    possible_index = {'0':set(), '1':set(), '2':set(), '3':set(),
            '4':set(), '5':set(), '6':set(), '7':set(), '8':set(),
            '9':set(), 'a':set(), 'b':set(), 'c':set(), 'd':set(),
            'e':set(), 'f':set()}
    indices = []
    max_idx = None
    all_smaller_found = False
    while not all_smaller_found:
        hasher = hashlib.md5()
        hasher.update((salt + str(idx)).encode("ascii"))
        hash_ = hasher.hexdigest()
        for i in range(stretching):
            hasher = hashlib.md5()
            hasher.update(hash_.encode("ascii"))
            hash_ = hasher.hexdigest()
        a, b, c, d, e = None, None, None, None, None
        triplet_found = False
        for letter in hash_:
            a, b, c, d, e = b, c, d, e, letter
            if not triplet_found and c == d == e:
                if idx not in possible_index[c]:
                    possible_index[c].add(idx)
                triplet_found = True
            if a == b == c == d == e:
                for index in filter(lambda x: x != idx and idx <= x+1000, possible_index[a]):
                    indices.append(index)
                possible_index[a] = set()
                possible_index[a].add(idx)
            if len(indices) >= num_hashes and max_idx == None:
                max_idx = idx
            if max_idx:
                all_smaller_found = True
                for letter in "0123456789abcdef":
                    new_possible_index = set()
                    for index in possible_index[letter]:
                        if index < max_idx:
                            all_smaller_found = False
                        else:
                            new_possible_index.add(index)
                    possible_index[letter] = new_possible_index
        idx += 1
    return indices

if len(sys.argv) > 1 and sys.argv[1] == "test":
    test_salt = "abc"
    indices = compute_hashes(test_salt, 64)
    indices.sort()
    print(indices, len(indices))
    assert(indices[0] == 39)
    assert(indices[63] == 22728)
    indices = compute_hashes(test_salt, 64, 2016)
    indices.sort()
    print(indices, len(indices))
    assert(indices[0] == 10)
    assert(indices[63] == 22551)
else:
    salt = "ihaygndm"
    indices = compute_hashes(salt, 64)
    indices.sort()
    print("Der 64. Index lautet:", indices[63])
    indices = compute_hashes(salt, 64, 2016)
    indices.sort()
    print("Der 64. Index lautet (mit Stretching):", indices[63])
