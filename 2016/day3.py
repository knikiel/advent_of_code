def count_possible_triangles(data):
    return sum(is_possible_triangle(*sides) for sides in data)

def is_possible_triangle(a, b, c):
    return max(a, b, c) < a + b + c - max(a, b, c)

if __name__ == "__main__":
    f = open("day3.input")
    raw_data = f.readlines()
    data = []
    for line in raw_data:
        number_strings = line.split()
        assert(len(number_strings) == 3)
        data.append(tuple(int(string) for string in number_strings))

    assert(is_possible_triangle(5, 10, 25) == False)

    num_triangles = count_possible_triangles(data)
    print("Count of possible triangles:", num_triangles)

    import numpy as np
    np_data = np.array([list(values) for values in data])
    vertical_data = np_data.transpose()
    vertical_data = vertical_data.reshape((-1, 3))

    num_triangles = count_possible_triangles(vertical_data)
    print("Count of possible triangles (vertically):", num_triangles)
