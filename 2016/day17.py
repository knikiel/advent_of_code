import heapq
import hashlib

def compute_shortest_path(passcode):
    heap = [(0, "", 0, 0)]
    while True:
        heur_dist, path, x, y = heapq.heappop(heap)
        #print(path, x, y)
        for x_offset, y_offset, direction in reachable_rooms(passcode + path):
            new_x = x + x_offset
            new_y = y + y_offset
            if new_x == -1 or new_x == 4 or new_y == -1 or new_y == 4:
                continue
            new_path = path + direction
            if new_x == new_y == 3:
                return new_path
            new_heur_dist = len(new_path) + abs(3 - new_x) + abs(3 - new_y)
            heapq.heappush(heap, (new_heur_dist, new_path, new_x, new_y))

def compute_longest_path(passcode):
    heap = [(0, "", 0, 0)]
    while True:
        heur_dist, path, x, y = heapq.heappop(heap)
        rooms = reachable_rooms(passcode + path)
        #print(path, x, y, len(rooms))
        if x == y == 3:
            return path
        for x_offset, y_offset, direction in rooms:
            new_x = x + x_offset
            new_y = y + y_offset
            if new_x == -1 or new_x == 4 or new_y == -1 or new_y == 4:
                continue
            new_path = path + direction
            new_heur_dist = -(abs(3 - new_x) + abs(3 - new_y) + len(new_path) / (len(new_path) + 1))
            if new_x == new_y == 3:
                new_heur_dist += 10000000
            heapq.heappush(heap, (new_heur_dist, new_path, new_x, new_y))

def reachable_rooms(passcode):
    hasher = hashlib.md5()
    hasher.update(passcode.encode('utf-8'))
    hash_ = hasher.hexdigest()
    reachable_rooms = []
    u, d, l, r =  hash_[:4]
    if u in 'bcdef':
        reachable_rooms.append((0, -1, 'U'))
    if d in 'bcdef':
        reachable_rooms.append((0, 1, 'D'))
    if l in 'bcdef':
        reachable_rooms.append((-1, 0, 'L'))
    if r in 'bcdef':
        reachable_rooms.append((1, 0, 'R'))
    return reachable_rooms
    

assert(compute_shortest_path("ihgpwlah") == "DDRRRD")
assert(compute_shortest_path("kglvqrro") == "DDUDRLRRUDRD")
assert(compute_shortest_path("ulqzkmiv") == "DRURDRUDDLLDLUURRDULRLDUUDDDRR")

assert(len(compute_longest_path("ihgpwlah")) == 370)
assert(len(compute_longest_path("kglvqrro")) == 492)
assert(len(compute_longest_path("ulqzkmiv")) == 830)

passcode = "dmypynyp"
shortest_path = compute_shortest_path(passcode)
print("Kuerzester Pfad:", shortest_path)

longest_path = compute_longest_path(passcode)
print("Lauenge des laengsten Pfads:", len(longest_path))

