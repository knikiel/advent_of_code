import itertools
import hashlib 
import heapq
import re
from copy import copy

def parse_data(lines):
    nodes = []
    for line in lines:
        m = re.search("node-x(\d+)-y(\d+)\s+(\d+)T\s+(\d+)", line)
        if m:
            x, y = int(m.group(1)), int(m.group(2))
            size, used = int(m.group(3)), int(m.group(4))
            nodes.append((x, y, size, used))
    return nodes

def count_viable_pairs(nodes):
    num_viable = 0
    for node1, node2 in itertools.combinations(nodes, 2):
        if (0 < node1[3] <= node2[2] - node2[3]
                or  0 < node2[3] <= node1[2] - node1[3]):
            num_viable += 1
    return num_viable
    
def transfer_data(nodes):
    max_x = 0 
    max_y = 0 
    for x, y, o1, o2 in nodes:
        max_x = max(max_x, x)
        max_y = max(max_y, y)
    full_nodes = []
    for x, y, size, used in nodes:
        if used == 0:
            x_free, y_free = x, y
        elif used > 200:
            full_nodes.append((x, y))

    heap = [(0, 0, max_x, 0, x_free, y_free)]
    hashes = set()
    while True:
        heur, num_steps, x_pos, y_pos, x_free, y_free = heapq.heappop(heap)
        #print(num_steps, x_pos, y_pos)
        if x_pos == y_pos == 0:
            return num_steps
        for new_x_free, new_y_free in [(x_free-1, y_free), (x_free+1, y_free), (x_free, y_free-1), (x_free, y_free+1)]:
            if not (0 <= new_x_free <= max_x and 0 <= new_y_free <= max_y):
                continue
            if (new_x_free, new_y_free) in full_nodes:
                continue
            if new_x_free == x_pos and new_y_free == y_pos:
                new_x_pos = x_free
                new_y_pos = y_free
            else:
                new_x_pos = x_pos
                new_y_pos = y_pos
            state = (new_x_free, new_y_free, new_x_pos, new_y_pos)
            if state in hashes:
                continue
            hashes.add(state)
            new_heur = num_steps + 1 + (new_x_pos + new_y_pos)# + (abs(new_x_free - x_pos) + abs(new_y_free - y_pos))
            heapq.heappush(heap, (new_heur, num_steps + 1, new_x_pos, new_y_pos, new_x_free, new_y_free))



test_nodes = [(0, 0, 10, 8), (0, 1, 11, 6), (0, 2, 32, 28),
        (1, 0, 9, 7), (1, 1, 8, 0), (1, 2, 11, 7),
        (2, 0, 10, 6), (2, 1, 9, 8), (2, 2, 9, 6)]
assert(transfer_data(test_nodes) == 7)

with open("day22.input") as f:
    lines = f.readlines()
nodes = parse_data(lines)
num_viable = count_viable_pairs(nodes)
print("Anzahl moeglicher Paare:", num_viable)
num_steps = transfer_data(nodes)
print("Anzahl notwendiger Moves:", num_steps)
