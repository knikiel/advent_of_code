import networkx as nx
import heapq as hq

def is_passage(x, y, fav_number):
    if x < 0 or y < 0:
        return False
    number = x*x + 3*x + 2*x*y + y + y*y + fav_number
    num_bits = 0
    while number > 0:
        num_bits += number % 2
        number = number // 2
    return num_bits % 2 == 0

def create_maze(fav_number, size):
    maze = [[0]*size for i in range(size)]
    graph = nx.Graph()
    graph.add_nodes_from(((x, y) for x in range(size) for y in range(size)))
    for x in range(size):
        for y in range(size):
            if is_passage(x, y, fav_number):
                maze[y][x] = '.'
                if y > 0 and maze[y-1][x] == '.':
                    graph.add_edge((x, y-1), (x, y))
                if x > 0 and maze[y][x-1] == '.':
                    graph.add_edge((x-1, y), (x, y))
            else:
                maze[y][x] = '#'
    return maze, graph

def print_maze(maze):
    for row in maze:
        print("".join(row))

def reach_destination(graph, destination):
    path = nx.shortest_path(graph, (1, 1), destination)
    return path

def reach_destination_lazily(fav_number, destination):
    graph = nx.Graph()
    start_pos = (1, 1)
    open_set = [(0, 0, start_pos)]
    graph.add_node(start_pos)
    while True:
        heur, distance, (x, y) = hq.heappop(open_set)
        if (x, y) == destination:
            return distance
        if (x-1, y) not in graph and is_passage(x-1, y, fav_number):
            graph.add_node((x-1, y))
            min_dist_to_dest = abs(x-1-destination[0]) + abs(y-destination[1])
            hq.heappush(open_set, (distance+1+min_dist_to_dest, distance+1, (x-1, y)))
        if (x+1, y) not in graph and is_passage(x+1, y, fav_number):
            graph.add_node((x+1, y))
            min_dist_to_dest = abs(x+1-destination[0]) + abs(y-destination[1])
            hq.heappush(open_set, (distance+1+min_dist_to_dest, distance+1, (x+1, y)))
        if (x, y-1) not in graph and is_passage(x, y-1, fav_number):
            graph.add_node((x, y-1))
            min_dist_to_dest = abs(x-destination[0]) + abs(y-1-destination[1])
            hq.heappush(open_set, (distance+1+min_dist_to_dest, distance+1, (x, y-1)))
        if (x, y+1) not in graph and is_passage(x, y+1, fav_number):
            graph.add_node((x, y+1))
            min_dist_to_dest = abs(x-destination[0]) + abs(y+1-destination[1])
            hq.heappush(open_set, (distance+1+min_dist_to_dest, distance+1, (x, y+1)))

def reach_destination_lazily(fav_number, destination):
    graph = nx.Graph()
    start_pos = (1, 1)
    open_set = [(0, 0, start_pos)]
    graph.add_node(start_pos)
    while True:
        heur, distance, (x, y) = hq.heappop(open_set)
        if (x, y) == destination:
            return distance
        if (x-1, y) not in graph and is_passage(x-1, y, fav_number):
            graph.add_node((x-1, y))
            min_dist_to_dest = abs(x-1-destination[0]) + abs(y-destination[1])
            hq.heappush(open_set, (distance+1+min_dist_to_dest, distance+1, (x-1, y)))
        if (x+1, y) not in graph and is_passage(x+1, y, fav_number):
            graph.add_node((x+1, y))
            min_dist_to_dest = abs(x+1-destination[0]) + abs(y-destination[1])
            hq.heappush(open_set, (distance+1+min_dist_to_dest, distance+1, (x+1, y)))
        if (x, y-1) not in graph and is_passage(x, y-1, fav_number):
            graph.add_node((x, y-1))
            min_dist_to_dest = abs(x-destination[0]) + abs(y-1-destination[1])
            hq.heappush(open_set, (distance+1+min_dist_to_dest, distance+1, (x, y-1)))
        if (x, y+1) not in graph and is_passage(x, y+1, fav_number):
            graph.add_node((x, y+1))
            min_dist_to_dest = abs(x-destination[0]) + abs(y+1-destination[1])
            hq.heappush(open_set, (distance+1+min_dist_to_dest, distance+1, (x, y+1)))

def reach_many_destinations(fav_number, num_steps):
    graph = nx.Graph()
    start_pos = (1, 1)
    open_set = [(0, start_pos)]
    graph.add_node(start_pos)
    min_distance = 0
    num_places = 0
    while True:
        distance, (x, y) = hq.heappop(open_set)
        min_distance = distance
        if min_distance > num_steps:
            return num_places
        num_places += 1
        if (x-1, y) not in graph and is_passage(x-1, y, fav_number):
            graph.add_node((x-1, y))
            hq.heappush(open_set, (distance+1, (x-1, y)))
        if (x+1, y) not in graph and is_passage(x+1, y, fav_number):
            graph.add_node((x+1, y))
            hq.heappush(open_set, (distance+1, (x+1, y)))
        if (x, y-1) not in graph and is_passage(x, y-1, fav_number):
            graph.add_node((x, y-1))
            hq.heappush(open_set, (distance+1, (x, y-1)))
        if (x, y+1) not in graph and is_passage(x, y+1, fav_number):
            graph.add_node((x, y+1))
            hq.heappush(open_set, (distance+1, (x, y+1)))

test_fav = 10
test_maze, test_graph = create_maze(test_fav, 10)
#print_maze(test_maze)
test_destination = (7, 4)
test_length = len(reach_destination(test_graph, test_destination))
assert(test_length-1 == 11)
test_path_length = reach_destination_lazily(test_fav, test_destination)
assert(test_path_length == 11)

fav_number = 1362
destination = (31, 39)
path_length = reach_destination_lazily(fav_number, destination)
print("Laenge des Weges zum Ziel:", path_length)

num_places = reach_many_destinations(fav_number, 50)
print("Anzahl der erreichbaren Orte:", num_places)
