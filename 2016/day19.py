from collections import deque

def get_elf_with_all_presents(num_elves):
    elves = deque((i, 1) for i in range(1, num_elves+1))
    while len(elves) > 1:
        happy_elf, num_old_presents =  elves.popleft()
        sad_elf, num_new_presents = elves.popleft()
        elves.append((happy_elf, num_old_presents + num_new_presents))
    return elves.pop()[0]

def get_elf_with_all_presents_v2(num_elves):
    elves = deque(range(1, num_elves+1))
    while len(elves) > 1:
        if len(elves) % 2 == 0:
            sad_elf = elves[len(elves) // 2]
            del elves[len(elves) // 2]
        else:
            sad_elf = elves[(len(elves) - 1) // 2]
            del elves[(len(elves) - 1) // 2]
        happy_elf =  elves.popleft()
        elves.append(happy_elf)
    return elves.pop()

def get_elf_with_all_presents_v3(num_elves):
    middle_elf = (num_elves+1) // 2
    elves = deque(range(1, middle_elf))
    opposite_elves = deque(range(middle_elf, num_elves+1))
    while len(opposite_elves) > 1:
        happy_elf =  elves.popleft()
        opposite_elves.popleft()
        opposite_elves.append(happy_elf)
        if len(elves) + 1 < len(opposite_elves):
            elves.append(opposite_elves.popleft())
        #print(elves, opposite_elves)
    return elves.pop()

assert(get_elf_with_all_presents(5) == 3)
assert(get_elf_with_all_presents_v2(5) == 2)
assert(get_elf_with_all_presents_v3(5) == 2)

num_elves = 3014603
elf = get_elf_with_all_presents(num_elves)
print("Elf mit allen Geschenken:", elf)
elf = get_elf_with_all_presents_v3(num_elves)
print("Elf mit allen Geschenken, v3:", elf)
