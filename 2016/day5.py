# -*- coding: latin-1 -*-
import hashlib

def compute_password(key):
    counter = 0
    password = ""
    while True:
        hash_value = compute_hash(key + str(counter))
        if hash_value[:5] == "00000":
            password += hash_value[5]
            if len(password) == 8:
                return password
        counter += 1
    assert(false)

def compute_improved_password(key):
    counter = 0
    password = [-1, -1, -1, -1, -1, -1, -1, -1]
    num_found = 0
    while True:
        hash_value = compute_hash(key + str(counter))
        if hash_value[:5] == "00000":
            pos = hash_value[5]
            if ord('0') <= ord(pos) < ord('8') and password[int(pos)] == -1: 
                num_found += 1
                password[int(pos)] = hash_value[6]
                if num_found == 8:
                    return "".join(password)
        counter += 1
    assert(false)

def compute_password_curses(key):
    import curses
    window = curses.initscr()
    start_x = 2
    start_y = 1
    curses.curs_set(2)
    window.move(start_y, start_x)
    window.insstr("Password wird geknackt:", curses.A_BOLD)
    window.insstr(start_y + 2, start_x, "________")
    window.box(ord('|'), ord('-'))
    window.refresh()

    counter = 0
    password = [-1, -1, -1, -1, -1, -1, -1, -1]
    num_found = 0
    while True:
        hash_value = compute_hash(key + str(counter))
        old_pos = curses.getsyx()
        window.move(start_y + 4, start_x)
        hashing_output = "[aktuell betrachteter MD5-Hash: " + hash_value + "]"
        window.deleteln()
        window.insdelln(1)
        window.insstr(hashing_output)
        window.move(*old_pos)
        window.box(ord('|'), ord('-'))
        window.refresh()
        if hash_value[:5] == "00000":
            pos = hash_value[5]
            if ord('0') <= ord(pos) < ord('8') and password[int(pos)] == -1: 
                num_found += 1
                password[int(pos)] = hash_value[6]
                window.move(start_y+2, start_x+int(pos))
                window.delch()
                window.insch(hash_value[6])
                window.refresh()
                if num_found == 8:
                    window.move(start_y + 3, start_x)
                    window.refresh()
                    return "".join(password)
        counter += 1
    assert(false)

def compute_hash(key):
    hasher = hashlib.md5()
    hasher.update(key.encode('ascii'))
    return hasher.hexdigest()    

if __name__ == "__main__":
    # Tests dauern zu lange, um sie immer auszuf�hren
    #assert(compute_hash("abc3231929")[:7] == "0000015")
    #assert(compute_hash("abc5017308")[:6] == "000008")
    #assert(compute_hash("abc5278568")[:6] == "00000f")
    #assert(compute_hash("abc5357525")[:7] == "000004e")
    #assert(compute_password("abc") == "18f47a30")
    #assert(compute_improved_password("abc") == "05ace8e3")

    input_string = "ugkcyxxp"
    #password = compute_password(input_string)
    #print("Password of door:", password)
    #password = compute_improved_password(input_string)
    #print("Password of second door:", password)
    # Optional in sch�n:
    password = compute_password_curses(input_string)
