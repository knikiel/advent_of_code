def get_min_possible_ip(possible_range, blacklist):
    possible_ranges = [possible_range]
    for forbidden_range in blacklist:
        new_possible_ranges = []
        for possible_range in possible_ranges:
            if not is_contained(possible_range, forbidden_range):
                if has_intersection(possible_range, forbidden_range):
                    new_possible_ranges.extend(subtract(possible_range, forbidden_range))
                else:
                    new_possible_ranges.append(possible_range)
        possible_ranges = new_possible_ranges
        #print(possible_ranges)
    possible_ranges.sort()
    return possible_ranges[0][0]

def count_possible_ips(possible_range, blacklist):
    possible_ranges = [possible_range]
    for forbidden_range in blacklist:
        new_possible_ranges = []
        for possible_range in possible_ranges:
            if not is_contained(possible_range, forbidden_range):
                if has_intersection(possible_range, forbidden_range):
                    new_possible_ranges.extend(subtract(possible_range, forbidden_range))
                else:
                    new_possible_ranges.append(possible_range)
        possible_ranges = new_possible_ranges
        #print(possible_ranges)
    num_possible_ips = 0
    for lower, upper in possible_ranges:
        num_possible_ips += upper - lower + 1
    return num_possible_ips

def is_contained(r1, r2):
    return r2[0] <= r1[0] <= r2[1] and r2[0] <= r1[1] <= r2[1]

def has_intersection(r1, r2):
    return r2[0] <= r1[0] <= r2[1] or r2[0] <= r1[1] <= r2[1] or r1[0] <= r2[0] <= r1[1] or r1[0] <= r2[1] <= r1[1]

def subtract(r1, r2):
    if r1 == r2:
        return []
    if is_contained(r2, r1):
        if r1[0] == r2[0]:
            return [(r2[1]+1, r1[1])]
        elif r1[1] == r2[1]:
            return [(r1[0], r2[0]-1)]
        else:
            return [(r1[0], r2[0]-1), (r2[1]+1, r1[1])]
    elif r1[0] < r2[0]:
        return [(r1[0], r2[0]-1)]
    elif r2[1] < r1[1]:
        return [(r2[1]+1, r1[1])]
    assert(False)

test_blacklist = [(5, 8), (0, 2), (4, 7)]
test_ip = get_min_possible_ip((0, 9), test_blacklist)
assert(test_ip == 3)

f = open("day20.input")
input_lines = f.readlines()
blacklist = []
for line in input_lines:
    lower, upper = line.split('-')
    blacklist.append((int(lower), int(upper)))
ip = get_min_possible_ip((0, 2**32-1), blacklist)
print("Minimale erlaubte IP:", ip)
num_ips = count_possible_ips((0, 2**32-1), blacklist)
print("Anzahl erlaubter IPs:", num_ips)
