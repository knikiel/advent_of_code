import re

def compute(instructions, c_value = 0):
    pc = 0
    regs = {'a':0, 'b':0, 'c':c_value, 'd':0}
    while pc < len(instructions):
        instruction = instructions[pc]
        m = re.search("cpy (\d+) (\w)", instruction)
        if m:
            value = int(m.group(1))
            reg = m.group(2)
            regs[reg] = value
            pc += 1
            continue
        m = re.search("cpy (\w) (\w)", instruction)
        if m:
            value = regs[m.group(1)]
            reg = m.group(2)
            regs[reg] = value
            pc += 1
            continue
        m = re.search("inc (\w)", instruction)
        if m:
            reg = m.group(1)
            regs[reg] += 1
            pc += 1
            continue
        m = re.search("dec (\w)", instruction)
        if m:
            reg = m.group(1)
            regs[reg] -= 1
            pc += 1
            continue
        m = re.search("jnz (\d+) (\-?\d+)", instruction)
        if m:
            steps = int(m.group(2))
            value = int(m.group(1))
            if value != 0:
                pc += steps
            else:
                pc += 1
            continue
        m = re.search("jnz (\w) (\-?\d+)", instruction)
        if m:
            steps = int(m.group(2))
            reg = m.group(1)
            if regs[reg] != 0:
                pc += steps
            else:
                pc += 1
            continue
        print(instruction)
        assert(False)
    return regs
if __name__ == "__main__":
    test_data = ["cpy 41 a", "inc a", "inc a", "dec a", "jnz a 2", "dec a"]
    assert(compute(test_data)['a'] == 42)

    f = open("day12.input")
    instructions = f.readlines()
    regs = compute(instructions)
    print("Inhalt von Register \'a\':", regs['a'])
    regs = compute(instructions, 1)
    print("Inhalt von Register \'a\' (c=1 initialisiert):", regs['a'])
