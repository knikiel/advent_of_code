import re

def make_bots_work(data):
    # parsing
    init_data = []
    instruction_data = []
    max_bot_id = -1
    max_output_id = -1
    for line in data:
        m = re.search("value (\d+) goes to bot (\d+)", line)
        if m:
            value = int(m.group(1))
            bot = int(m.group(2))
            if bot > max_bot_id:
                max_bot_id = bot
            init_data.append((bot, value))
            continue
        m = re.search("bot (\d+) gives low to (\w+) (\d+) and high to (\w+) (\d+)", line)
        if m:
            bot = int(m.group(1))
            type1 = m.group(2)
            id1 = int(m.group(3))
            type2 = m.group(4)
            id2 = int(m.group(5))
            if bot > max_bot_id:
                max_bot_id = bot
            if max(id1, id2) > max_output_id:
                max_output_id = max(id1, id2)
            instruction_data.append((bot, type1, id1, type2, id2))
            continue
        assert(False)
    # make them really work
    bots = [{"chips": set()} for i in range(max_bot_id + 1)]
    outputs = [None] * (max_output_id + 1)
    for bot, value in init_data:
        bots[bot]["chips"].add(value)
    for bot, type1, id1, type2, id2 in instruction_data:
        bots[bot]["instruction"] = (type1, id1, type2, id2)
    pausing_bots = list(range(max_bot_id+1))
    while len(pausing_bots) > 0:
        #print("pausing:", pausing_bots)
        #print(bots)
        working_bots = pausing_bots
        pausing_bots = []
        while len(working_bots) > 0:
            #print("working:", working_bots)
            bot = working_bots.pop()
            if len(bots[bot]["chips"]) == 2:
                #print(bots[bot])
                type1, id1, type2, id2 = bots[bot]["instruction"]
                min_val = min(bots[bot]["chips"])
                max_val = max(bots[bot]["chips"])
                if type1 == "bot":
                    bots[id1]["chips"].add(min_val)
                else:
                    outputs[id1] = min_val 
                if type2 == "bot":
                    bots[id2]["chips"].add(max_val)
                else:
                    outputs[id2] = max_val 
            else:
                pausing_bots.append(bot) 
    return {"bots": bots, "outputs": outputs}

if __name__ == "__main__":
    test_data = ["value 5 goes to bot 2",
            "bot 2 gives low to bot 1 and high to bot 0",
            "value 3 goes to bot 1",
            "bot 1 gives low to output 1 and high to bot 0",
            "bot 0 gives low to output 2 and high to output 0",
            "value 2 goes to bot 2"]
    test_result = make_bots_work(test_data)
    assert(test_result["bots"][2]["chips"] == {5, 2})
    assert(test_result["outputs"][0] == 5)
    assert(test_result["outputs"][1] == 2)
    assert(test_result["outputs"][2] == 3)

    f = open("day10.input")
    data = f.readlines()
    results = make_bots_work(data)
    for bot_id, bot in enumerate(results["bots"]):
        if bot["chips"] == {17, 61}:
            print("ID des Bots, der 17 mit 61 vergleicht:", bot_id)
    outputs = results["outputs"]
    product = outputs[0] * outputs[1] * outputs[2]
    print("Produkt der Chip-Werte in den ersten 3 Outputs:", product)
