def time_of_button_press(discs):
    # normalize
    normalized_discs = []
    for time, (num_positions, offset) in enumerate(discs):
        normalized_discs.append((num_positions, (offset+time+1) % num_positions))
    #print(normalized_discs)
    time = -1
    found = False
    while not found:
        time += 1
        found = True
        for (num_positions, offset) in normalized_discs:
            if (time + offset) % num_positions != 0:
                #print(time, num_positions, offset)
                found = False
                break 
    return time

def parse_data(data):
    discs = []
    for line in data:
        split = line.strip().split(' ')
        discs.append((int(split[3]), int(split[11][:-1])))
    return discs

test_discs = [(5, 4), (2, 1)]
test_time = time_of_button_press(test_discs)
assert(test_time == 5)
test_discs = [(5, 4), (2, 1), (3, 1)]
test_time = time_of_button_press(test_discs)
assert(test_time == 5)

f = open("day15.input")
data = f.readlines()
discs = parse_data(data)
time = time_of_button_press(discs)
print("Zeitpunkt zum Druecken:", time)
discs.append((11, 0))
time = time_of_button_press(discs)
print("Zeitpunkt zum 2. Druecken:", time)
