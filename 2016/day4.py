def parse_data(string_data):
    data = []
    for line in string_data:
        line_parts = line.strip().split("-")
        name = "-".join(line_parts[:-1])
        line_parts = line_parts[-1][:-1].split("[")
        sector_id = int(line_parts[0])
        checksum = line_parts[1]
        data.append((name, sector_id, checksum))
    #print(data)
    return data

def sum_ids_of_rooms(data):
    total = 0
    for room in data:
        total += room[1]
    return total

def is_real_room(data):
    name, sector_id, checksum = data
    if len(checksum) != 5:
        return False
    letter_counts = {}
    for letter in name:
        if letter in letter_counts:
            letter_counts[letter] += 1
        else:
            letter_counts[letter] = 1
    letter_counts = [(count, letter) for letter, count in letter_counts.items() if letter != '-'] 
    sorted_counts = sorted(letter_counts,
            key = lambda count_letter : (-count_letter[0], count_letter[1]))
    sorted_letters = [letter for count, letter in sorted_counts]
    sorted_letters = "".join(sorted_letters)
    if sorted_letters[:5] == checksum:
        return True
    else:
        return False

def filter_rooms(data):
    return [(name, sector_id) for name, sector_id, checksum in filter(is_real_room, data)]

def shift_names(data):
    return list(map(shift_name, data))

def shift_name(data):
    encrypted_name, sector_id = data
    len_alpha = ord('z') - ord('a') + 1
    shifted_name = ""
    for letter in encrypted_name:
        if letter == '-':
            new_letter = ' '
        else:
            new_letter = chr(ord('a') + (ord(letter) - ord('a') + sector_id) % len_alpha)
        shifted_name += new_letter
    return shifted_name, sector_id

if __name__ == "__main__":
    f = open("day4.input")
    string_data = f.readlines()
    data = parse_data(string_data)

    test_data_string = "totally-real-room-200[decoy]"
    test_data1 = ("aaaaa-bbb-z-y-x", 123, "abxyz")
    test_data2 = ("a-b-c-d-e-f-g-h", 987, "abcde")
    test_data3 = ("not-a-real-room", 404, "oarel")
    test_data4 = ("totally-real-room", 200, "decoy")
    all_test_data = [test_data1, test_data2, test_data3, test_data4]
    assert(parse_data([test_data_string])[0] == test_data4)
    assert(is_real_room(test_data1))
    assert(is_real_room(test_data2))
    assert(is_real_room(test_data3))
    assert(not is_real_room(test_data4))
    assert(sum_ids_of_rooms(filter_rooms(all_test_data)) == 1514)

    filtered_data = filter_rooms(data)
    sum_ids = sum_ids_of_rooms(filtered_data)
    print("Summe der IDs:", sum_ids)

    assert(shift_name(("qzmt-zixmtkozy-ivhz", 343))[0] == "very encrypted name")

    decrypted_data = shift_names(filtered_data)
    print("Entzifferte Raeume (suche nach 'northpole object storage':")
    print(decrypted_data)
