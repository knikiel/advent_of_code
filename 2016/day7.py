def supports_tls(data):
    a, b, c, d = None, None, None, None
    is_in_hypernet_sequence = False
    has_abba = False
    for letter in data:
        if letter == '[':
            is_in_hypernet_sequence = True
            a, b, c, d = None, None, None, None
            continue
        elif letter == ']':
            is_in_hypernet_sequence = False
            a, b, c, d = None, None, None, None
            continue
        a, b, c, d = b, c, d, letter
        if a == d and b == c and a != b:
            if is_in_hypernet_sequence:
                return False
            else:
                has_abba = True
    return has_abba

def supports_ssl(data):
    a, b, c = None, None, None
    is_in_hypernet_sequence = False
    abas = []
    babs = []
    for letter in data:
        if letter == '[':
            is_in_hypernet_sequence = True
            a, b, c = None, None, None
            continue
        elif letter == ']':
            is_in_hypernet_sequence = False
            a, b, c = None, None, None
            continue
        a, b, c = b, c, letter
        if a == c and a != b:
            if is_in_hypernet_sequence:
                babs.append((a, b, c))
            else:
                abas.append((a, b, c))
    for aba in abas:
        for bab in babs:
            if aba[0] == bab[1] and aba[1] == bab[0]:
                return True
    return False

if __name__ == "__main__":
    assert(supports_tls("abba[mnop]qrst"))
    assert(not supports_tls("abcd[bddb]xyyx"))
    assert(not supports_tls("aaaa[qwer]tyui"))
    assert(supports_tls("oxxoj[asdfgh]zxcvbn"))

    assert(supports_ssl("aba[bab]xyz"))
    assert(not supports_ssl("xyx[xyx]xyx"))
    assert(supports_ssl("aaa[kek]eke"))
    assert(supports_ssl("zazbz[bzb]cdb"))

    f = open("day7.input")
    data = [line.strip() for line in f.readlines()]
    num_tls_supporters = sum(map(supports_tls, data))
    print("Anzahl der IPs, die TLS unterstuetzen:", num_tls_supporters)
    num_ssl_supporters = sum(map(supports_ssl, data))
    print("Anzahl der IPs, die SSL unterstuetzen:", num_ssl_supporters)
