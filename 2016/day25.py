import re
from copy import copy

def find_input(instructions):
    a = 0
    while True:
        print("Testing a=", a)
        if compute(instructions, a):
            return a
        a += 1

def compute(instructions_, a_value = 0):
    pc = 0
    instructions = copy(instructions_)
    regs = {'a':a_value, 'b':0, 'c':0, 'd':0}
    counter = 0
    last_value = 1
    states = set()
    while pc < len(instructions):
        counter += 1
        if counter % 1000000 == 0:
            print(pc, instructions)
            print(regs)
        instruction = instructions[pc]
        m = re.search("cpy (-?(\w+)) (\w+)", instruction)
        if m:
            if m.group(2).isdigit():
                value = int(m.group(1))
            else:
                value = regs[m.group(1)]
            if m.group(3).isdigit():
                continue
            else:
                reg = m.group(3)
            regs[reg] = value
            pc += 1
            continue
        m = re.search("inc (\w)", instruction)
        if m:
            if m.group(1).isdigit():
                continue
            else:
                reg = m.group(1)
            regs[reg] += 1
            pc += 1
            continue
        m = re.search("dec (\w)", instruction)
        if m:
            if m.group(1).isdigit():
                continue
            else:
                reg = m.group(1)
            regs[reg] -= 1
            pc += 1
            continue
        m = re.search("jnz (\w) (\-?(\w+))", instruction)
        if m:
            # geschummelte Spezialfaelle
            if (instruction == "jnz c -2" and pc-2 >= 0 and
                instructions[pc-1] == "dec c" and
                instructions[pc-2] == "inc a"):
                regs['a'] += regs['c']
                regs['c'] = 0
                pc += 1
                continue
            elif (instruction == "jnz d -2" and pc-2 >= 0 and
                instructions[pc-1] == "inc c" and
                instructions[pc-2] == "dec d"):
                regs['c'] += regs['d']
                regs['d'] = 0
                pc += 1
                continue
            elif (instruction == "jnz d -5" and pc-5 >= 0 and
                    instructions[pc-1] == "dec d" and
                    instructions[pc-2] == "jnz c -2" and
                    instructions[pc-3] == "dec c" and
                    instructions[pc-4] == "inc a" and
                    instructions[pc-5] == "cpy b c"):
                regs['a'] += regs['b'] * regs['d']
                regs['c'] = 0
                regs['d'] = 0

            # normaler Fall
            if m.group(3).isdigit():
                steps = int(m.group(2))
            else:
                steps = regs[m.group(2)]
            if m.group(1).isdigit():
                value = int(m.group(1))
            else:
                value = regs[m.group(1)]
            if value != 0:
                pc += steps
            else:
                pc += 1
            continue
        m = re.search("tgl (\w+)", instruction)
        if m:
            if m.group(1).isdigit():
                idx = int(m.group(1))
            else:
                idx = regs[m.group(1)]
            if pc + idx < 0 or pc + idx >= len(instructions):
                pc += 1
                continue
            old_instr = instructions[pc + idx]
            splits = old_instr.split(" ")
            if len(splits) == 2:
                if splits[0] == "inc":
                    new_instr = "dec " + splits[1]
                else:
                    new_instr = "inc " + splits[1]
            elif len(splits) == 3:
                if splits[0] == "jnz":
                    new_instr = "cpy " + splits[1] + " " + splits[2]
                else:
                    new_instr = "jnz " + splits[1] + " " + splits[2]
            else:
                print(splits)
                assert(False)
            instructions[pc + idx] = new_instr
            pc += 1
            #print(old_instr, new_instr)
            continue
        m = re.search("out (\w+)", instruction)
        if m:
            if m.group(1).isdigit():
                value = int(m.group(1))
            else:
                value = regs[m.group(1)]
            if last_value == 1:
                if not value == 0:
                    return False 
                last_value = 0 
            elif last_value == 0:
                if not value == 1:
                    return False 
                last_value = 1 
            else:
                print(last_value)
                assert(False)
            state = (regs['a'], regs['b'], regs['c'], regs['d'])
            if state in states:
                return True
            states.add(state)
            pc += 1
            continue
        print(instruction)
        assert(False)
    print(regs)
    return False 

if __name__ == "__main__":
    f = open("day25.input")
    instructions = [line.strip() for line in f.readlines()]
    value = find_input(instructions)
    print("Input von Register \'a\':", value)
