import re
import sys

def decompress(word):
    left_to_process = word[:]
    processed = ""
    regex = re.compile(r"\((\d+)x(\d+)\)")
    while len(left_to_process) > 0:
        m = regex.search(left_to_process)
        if not m:
            processed += left_to_process
            return processed
        start_index = m.start()
        end_index = m.end()
        processed += left_to_process[:start_index]
        left_to_process = left_to_process[end_index:]
        num_letters = int(m.group(1))
        how_often = int(m.group(2))
        processed += how_often * left_to_process[:num_letters]
        left_to_process = left_to_process[num_letters:]
    return processed

def length_of_decompress_v2(word):
    left_to_process = word[:]
    processed = []
    regex = re.compile(r"\((\d+)x(\d+)\)")
    while len(left_to_process) > 0:
        m = regex.search(left_to_process)
        if not m:
            processed.append((-1, len(left_to_process)))
            break
        num_letters = int(m.group(1))
        how_often = int(m.group(2))
        start_index = m.start()
        end_index = m.end()
        num_elems = 0
        no_bracket_found = True
        if start_index > 0:
            processed.append((-1, start_index))
        for letter in left_to_process[end_index:end_index+num_letters]:
            if no_bracket_found:
                num_elems += 1
            if letter == '(':
                no_bracket_found = False
            elif letter == ')':
                no_bracket_found = True
        processed.append((num_elems, how_often))
        left_to_process = left_to_process[end_index:]
    processed.reverse()
    num_letters = expand(processed)
    return num_letters

def expand(elements):
    if len(elements) == 0:
        return 0
    elem = elements.pop()
    num_elements, how_often = elem
    if num_elements == -1:
        return how_often + expand(elements)
    children = []
    while num_elements > 0:
        num_elements_child, how_often_child = elements.pop()
        if num_elements_child == -1:
            if how_often_child > num_elements:
                children.append((-1, num_elements))
                elements.append((-1, how_often_child-num_elements))
                num_elements = 0 
            else:
                num_elements -= how_often_child
                children.append((num_elements_child, how_often_child))
        else:
            num_elements -= 1
            children.append((num_elements_child, how_often_child))
        assert(num_elements >= 0)
    children.reverse()        
    children_length = expand(children)
    return children_length * how_often + expand(elements)

if __name__ == "__main__":
    assert(decompress("ADVENT") == "ADVENT")
    assert(decompress("(3x3)XYZ") == "XYZXYZXYZ")
    assert(decompress("A(1x5)BC") == "ABBBBBC")
    assert(decompress("A(2x2)BCD(2x2)EFG") == "ABCBCDEFEFG")
    assert(decompress("(6x1)(1x3)A") == "(1x3)A")
    assert(decompress("X(8x2)(3x3)ABCY") == "X(3x3)ABC(3x3)ABCY")
    assert(decompress("X(10x2)(3x3)ABCYZ") == "X(3x3)ABCYZ(3x3)ABCYZ")
    if len(sys.argv) > 1 and sys.argv[1] == "test":
        assert(length_of_decompress_v2("(27x12)(20x12)(13x14)(7x10)(1x12)A") == 241920)
        assert(length_of_decompress_v2("(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN") == 445)
    else:
        f = open("day9.input")
        data = f.read().replace(" ", "").replace("\n", "")
        decompressed_data = decompress(data)
        print("Laenge der dekomprimierten Daten:", len(decompressed_data))
        len_decompressed_data = length_of_decompress_v2(data)
        print("Laenge der dekomprimierten Daten (v2):", len_decompressed_data)
