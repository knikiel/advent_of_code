import networkx as nx
import itertools

def compute_shortest_path(maze):
    graph = nx.Graph()
    distance_graph = nx.Graph()
    mapping = {}
    special_nodes = []
    for y, line in enumerate(maze):
        for x, char in enumerate(line):
            if char == '.' or char.isdigit():
                graph.add_node((x, y))
                if (x-1, y) in graph:
                    graph.add_edge((x, y), (x-1, y))
                if (x, y-1) in graph:
                    graph.add_edge((x, y), (x, y-1))
                if char.isdigit():
                    number = int(char)
                    distance_graph.add_node(number)
                    mapping[number] = (x, y)
                    special_nodes.append(number)

    for n1, n2 in itertools.combinations(special_nodes, 2):
        length = nx.shortest_path_length(graph, mapping[n1], mapping[n2])
        distance_graph.add_edge(n1, n2, weight = length)

    special_nodes.remove(0)
    shortest_length = 100000000
    for perm in itertools.permutations(special_nodes):
        last_node = 0
        length = 0
        for next_node in perm:
            length += distance_graph.edge[last_node][next_node]["weight"]
            last_node = next_node
        shortest_length = min(shortest_length, length)
    return shortest_length

def compute_shortest_cycle(maze):
    graph = nx.Graph()
    distance_graph = nx.Graph()
    mapping = {}
    special_nodes = []
    for y, line in enumerate(maze):
        for x, char in enumerate(line):
            if char == '.' or char.isdigit():
                graph.add_node((x, y))
                if (x-1, y) in graph:
                    graph.add_edge((x, y), (x-1, y))
                if (x, y-1) in graph:
                    graph.add_edge((x, y), (x, y-1))
                if char.isdigit():
                    number = int(char)
                    distance_graph.add_node(number)
                    mapping[number] = (x, y)
                    special_nodes.append(number)

    for n1, n2 in itertools.combinations(special_nodes, 2):
        length = nx.shortest_path_length(graph, mapping[n1], mapping[n2])
        distance_graph.add_edge(n1, n2, weight = length)

    special_nodes.remove(0)
    shortest_length = 100000000
    for perm in itertools.permutations(special_nodes):
        last_node = 0
        length = 0
        for next_node in perm:
            length += distance_graph.edge[last_node][next_node]["weight"]
            last_node = next_node
        length += distance_graph.edge[last_node][0]["weight"]
        shortest_length = min(shortest_length, length)
    return shortest_length

test_map = ["###########", "#0.1.....2#", "#.#######.#", "#4.......3#", "###########"]
length = compute_shortest_path(test_map)
assert(length == 14)

with open("day24.input") as f:
    maze = [line.strip() for line in f.readlines()]
length = compute_shortest_path(maze)
print("Kuerzester Weg, alle Punkte zu besuchen:", length)
length = compute_shortest_cycle(maze)
print("Kuerzester Weg, alle Punkte zu besuchen und wieder zurueck zu kommen:", length)
