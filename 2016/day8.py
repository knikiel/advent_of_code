import re

def create_screen(x, y):
    return x * [y * " "]

def update_screen(screen, instruction):
    m = re.search("rect (\d*)x(\d*)", instruction)
    if m:
        x = int(m.group(1))
        y = int(m.group(2))
        updated_screen = paint_rectangle(screen, x, y)
        return updated_screen
    m = re.search("rotate column x=(\d*) by (\d*)", instruction)
    if m:
        x = int(m.group(1))
        by = int(m.group(2))
        updated_screen = rotate_down(screen, x, by)
        return updated_screen
    m = re.search("rotate row y=(\d*) by (\d*)", instruction)
    if m:
        y = int(m.group(1))
        by = int(m.group(2))
        updated_screen = rotate_right(screen, y, by)
        return updated_screen
    print("Konnte nicht gelesen werden:", instruction)
    assert(False)

def paint(screen, data):
    updated_screen = screen
    for line in data:
        updated_screen = update_screen(updated_screen, line)
    return updated_screen

def paint_rectangle(screen, x, y):
    updated_screen = []
    for line in screen[:y]:
        updated_line = x * "#" + line[x:]
        updated_screen.append(updated_line)
    updated_screen = updated_screen + screen[y:]
    return updated_screen

def rotate_right(screen, x, by):
    line = screen[x]
    updated_line = line[-by:] + line[:-by]
    return screen[:x] + [updated_line] + screen[x+1:]

def rotate_down(screen, y, by):
    updated_screen = []
    last_symbols = []
    for line in screen[-by:]:
        last_symbols.append(line[y])
    for line in screen:
        updated_line = line[:y] + last_symbols[0] + line[y+1:]
        updated_screen.append(updated_line)
        last_symbols = last_symbols[1:] + [line[y]]
    return updated_screen

if __name__ == "__main__":
    test_data = ["rect 3x2", "rotate column x=1 by 1", "rotate row y=0 by 4",
        "rotate column x=1 by 1"]
    test_screen = create_screen(3, 7)
    assert(test_screen == ["       "] * 3)
    test_screen1 = update_screen(test_screen, test_data[0])
    assert(test_screen1 == ["###    ", "###    ", "       "])
    test_screen2 = update_screen(test_screen1, test_data[1])
    assert(test_screen2 == ["# #    ", "###    ", " #     "])
    test_screen3 = update_screen(test_screen2, test_data[2])
    test_screen4 = update_screen(test_screen3, test_data[3])
    assert(test_screen4 == [" #  # #", "# #    ", " #     "])

    f = open("day8.input")
    data = [line.strip() for line in f.readlines()]
    updated_screen = paint(create_screen(6, 50), data)
    num_pixels_on = len("".join(updated_screen).replace(" ", ""))
    print("Anzahl der eingeschalteten Pixel:", num_pixels_on)
    for line in updated_screen:
        print(line)
