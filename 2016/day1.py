def compute_position(instructions):
    direction = 'N'
    x_pos = 0
    y_pos = 0
    for instruction in instructions:
        d = instruction[0]
        num_steps = int(instruction[1:])
        if direction == 'N' and d == 'L' or direction == 'S' and d == 'R':
            x_pos -= num_steps
            direction = 'W'
        elif direction == 'N' and d == 'R' or direction == 'S' and d == 'L':
            x_pos += num_steps
            direction = 'O'
        elif direction == 'O' and d == 'L' or direction == 'W' and d == 'R':
            y_pos -= num_steps
            direction = 'N'
        elif direction == 'O' and d == 'R' or direction == 'W' and d == 'L':
            y_pos += num_steps
            direction = 'S'
        else:
            assert(False)
        #print(x_pos, y_pos)
    print("x: ", x_pos, "y: ", y_pos)
    return abs(x_pos) + abs(y_pos)

def compute_twice_visited_position(instructions):
    direction = 'N'
    x_pos = 0
    y_pos = 0
    visited_positions = {(0, 0)}
    for instruction in instructions:
        d = instruction[0]
        num_steps = int(instruction[1:])
        for i in range(num_steps):
            if direction == 'N' and d == 'L' or direction == 'S' and d == 'R':
                x_pos -= 1
            elif direction == 'N' and d == 'R' or direction == 'S' and d == 'L':
                x_pos += 1
            elif direction == 'O' and d == 'L' or direction == 'W' and d == 'R':
                y_pos -= 1
            elif direction == 'O' and d == 'R' or direction == 'W' and d == 'L':
                y_pos += 1
            else:
                assert(False)
            #print(x_pos, y_pos)
            if (x_pos, y_pos) in visited_positions:
                print("x: ", x_pos, "y: ", y_pos)
                return abs(x_pos) + abs(y_pos)
            else:
                visited_positions.add((x_pos, y_pos))
        if direction == 'N' and d == 'L' or direction == 'S' and d == 'R':
            direction = 'W'
        elif direction == 'N' and d == 'R' or direction == 'S' and d == 'L':
            direction = 'O'
        elif direction == 'O' and d == 'L' or direction == 'W' and d == 'R':
            direction = 'N'
        elif direction == 'O' and d == 'R' or direction == 'W' and d == 'L':
            direction = 'S'
        else:
            assert(False)
    assert(False)

if __name__ == "__main__":
    f = open("day1.input")
    text_instructions = f.read()
    instructions = text_instructions.split(", ")
    
    assert(compute_position(["L1"]) == 1)
    assert(compute_position(["L30"]) == 30)
    assert(compute_position(["R2", "L3"]) == 5)
    assert(compute_position(["R2", "R2", "R2"]) == 2)
    assert(compute_position(["R5", "L5", "R5", "R3"]) == 12)

    distance = compute_position(instructions)
    print("Ex 1: distance:", distance)

    assert(compute_twice_visited_position(["R8", "R4", "R4", "R8"]) == 4)

    distance = compute_twice_visited_position(instructions)
    print("Ex 2: distance:", distance)
