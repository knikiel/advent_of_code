#!/usr/bin/python3
# encoding = utf-8

# adventofcode.com/day/24
# Day 24: It Hangs in the Balance
# It's Christmas Eve, and Santa is loading up the sleigh for this year's deliveries, but first he must have finde the proper balance.
# 1) What is the lowest quantum entanglement of a correct packing?
# 2) What is the lowest quantum entanglement in a packing of 4 sections?


import random


def read_packages(input_packages):
    packages = []
    for line in input_packages.splitlines():
        packages.append(int(line))
    return packages    


def find_best_packing_heuristic(packages):
    iterations = 1000 * len(packages) * len(packages)
    sum_weights = sum(packages)
    if sum_weights % 3 != 0:
        print("Sum of package weights not divisible by 3!")
        exit()
    else:
        print("Sum of package weights:", sum_weights)
    target_weight = sum_weights // 3
    min_quantum_entangle = 1
    for weight in packages:
        min_quantum_entangle *= (weight+1)  // 2
    min_number = len(packages)
    shuffled_weights = packages.copy()
    for _ in range(iterations):
        random.shuffle(shuffled_weights)
        num_packages1 = 0
        qu_ent1 = 1
        sum_packing1 = 0
        num_packages2 = 0
        qu_ent2 = 1
        sum_packing2 = 0
        num_packages3 = 0
        qu_ent3 = 1
        sum_packing3 = 0
        for weight in shuffled_weights:
            if weight + sum_packing1 <= target_weight:
                num_packages1 += 1
                qu_ent1 *= weight
                sum_packing1 += weight
            elif weight + sum_packing2 <= target_weight:
                num_packages2 += 1
                qu_ent2 *= weight
                sum_packing2 += weight
            elif weight + sum_packing3 <= target_weight:
                num_packages3 += 1
                qu_ent3 *= weight
                sum_packing3 += weight
            else:
                break
        if sum_packing1 == sum_packing2 == sum_packing3:
            packings = [(num_packages1, qu_ent1), (num_packages2, qu_ent2),
                    (num_packages3, qu_ent3)]
            packings.sort()
            if packings[0] < (min_number, min_quantum_entangle):
                print("New minimal quantum entanglement with 3 groups:", packings[0][1])
                min_number = packings[0][0]
                min_quantum_entangle = packings[0][1]
    return min_quantum_entangle
        

def find_best_packing(packages):
    return find_best_packing_heuristic(packages)

def find_best_packing_heuristic_v2(packages):
    iterations = 200 * len(packages) * len(packages)
    sum_weights = sum(packages)
    if sum_weights % 4 != 0:
        print("Sum of package weights not divisible by 4!")
        exit()
    else:
        print("Sum of package weights:", sum_weights)
    target_weight = sum_weights // 4
    min_quantum_entangle = 1
    for weight in packages:
        min_quantum_entangle *= (weight+2)  // 3
    min_number = len(packages)
    shuffled_weights = packages.copy()
    for _ in range(iterations):
        if random.choice([0,1]) == 0:
            random.shuffle(shuffled_weights)
        else:
            shuffled_weights.sort()
            idx = random.choice(range(len(packages)))
            temp = shuffled_weights[idx]
            del shuffled_weights[idx]
            shuffled_weights.append(temp)
            while random.choice([0,1]) == 0:
                idx = random.choice(range(len(packages)))
                temp = shuffled_weights[idx]
                del shuffled_weights[idx]
                shuffled_weights.append(temp)
            shuffled_weights.reverse()
            #print(shuffled_weights)
        num_packages1 = 0
        qu_ent1 = 1
        sum_packing1 = 0
        num_packages2 = 0
        qu_ent2 = 1
        sum_packing2 = 0
        num_packages3 = 0
        qu_ent3 = 1
        sum_packing3 = 0
        num_packages4 = 0
        qu_ent4 = 1
        sum_packing4 = 0
        for weight in shuffled_weights:
            if weight + sum_packing1 <= target_weight:
                num_packages1 += 1
                qu_ent1 *= weight
                sum_packing1 += weight
            elif weight + sum_packing2 <= target_weight:
                num_packages2 += 1
                qu_ent2 *= weight
                sum_packing2 += weight
            elif weight + sum_packing3 <= target_weight:
                num_packages3 += 1
                qu_ent3 *= weight
                sum_packing3 += weight
            elif weight + sum_packing4 <= target_weight:
                num_packages4 += 1
                qu_ent4 *= weight
                sum_packing4 += weight
            else:
                break
        if sum_packing1 == sum_packing2 == sum_packing3 == sum_packing4:
            packings = [(num_packages1, qu_ent1), (num_packages2, qu_ent2),
                    (num_packages3, qu_ent3), (num_packages4, qu_ent4)]
            packings.sort()
            if packings[0] < (min_number, min_quantum_entangle):
                print("New minimal quantum entanglement with 4 groups:", packings[0][1])
                min_number = packings[0][0]
                min_quantum_entangle = packings[0][1]
    return min_quantum_entangle


def find_best_packing_v2(packages):
    return find_best_packing_heuristic_v2(packages)


def main():
    with open("day24.input") as f:
        input_packages = f.read()
    packages = read_packages(input_packages)
    min_qu_entangle = find_best_packing(packages)
    print("Exercise 1:", min_qu_entangle)
    min_qu_entangle_v2 = find_best_packing_v2(packages)
    print("Exercise 2:", min_qu_entangle_v2)


if __name__ == "__main__":
    main()
