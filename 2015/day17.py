#!/usr/bin/python3
# encoding = utf-8

# adventofcode.com/day/17
# Day 17: No Such Thing as Too Much
# The Elves bought too much eggnog again, you have to refill it!
# 1) How many combinations of containers are possible?
# 2) How many combinations of the minimal number of containers?


import itertools as it

def read_containers(in_containers_text):
    containers = []
    for line in in_containers_text.splitlines():
        containers.append(int(line))
    return containers


def count_fitting_combinations(liters, containers):
    num_fitting_combinations = 0
    num_min_fitting_combinations = 0
    min_num_containers = 1000000
    for combination in it.product(*it.repeat([0, 1], len(containers))):
        values = 0
        for i, value in enumerate(containers):
            values += value * combination[i]
        if values == liters:
            num_fitting_combinations += 1
            num_containers = sum(combination)
            if num_containers <= min_num_containers:
                if num_containers < min_num_containers:
                    min_num_containers = num_containers
                    num_min_fitting_combinations = 0
                num_min_fitting_combinations += 1
    return num_fitting_combinations, num_min_fitting_combinations


def main():
    with open("day17.input") as f:
        input_containers = f.read()
    containers = read_containers(input_containers)
    num_combinations, num_min_combs = count_fitting_combinations(150, containers)
    print("Exercise 1: " + str(num_combinations))
    print("Exercise 2: " + str(num_min_combs))


if __name__ == "__main__":
    main()
