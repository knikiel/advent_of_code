#!/usr/bin/python3
# encoding = utf-8

# adventofcode.com/day/12
# Day 12: JSAbacusFramework.io
# The Elves' new account software uses a strange file format based on JSON.
# 1) What is the sum of all the numbers in the file?

import json
import re
import sys


def sum_numbers(json_string):
    regex = r'[-]?[\d]+'
    return sum([int(number) for number in re.findall(regex, json_string)])


def sum_numbers_without_red(json_string):
    def is_red(obj):
        if type(obj) == dict:
            for value in obj.values():
                if value == "red":
                    return True
        return False
    def without_red_maps(obj):
        if not (type(obj) == dict or type(obj) == list):
            return obj
        if type(obj) == list:
            accumulator = list()
            for elem in obj:
                if is_red(elem):
                    continue
                accumulator.append(without_red_maps(elem))
        else:  # dict
            accumulator = dict()
            for key, value in obj.items():
                if is_red(value):
                    continue
                accumulator[key] = without_red_maps(value)
        return accumulator
    json_object = [json.loads(json_string)]
    new_json_object = without_red_maps(json_object)
    return sum_numbers(json.dumps(new_json_object)[1:-1])


def test():
    assert sum_numbers('[1,2,3]') == sum_numbers('{"a":2,"b":4}') == 6
    assert sum_numbers('"a":[-1,1]') == sum_numbers('[-1,{"a":1}]') == 0
    assert sum_numbers('[]') == sum_numbers('{}') == 0
    assert sum_numbers('[1,2,3]') == sum_numbers('{"a":2,"b":4}') == 6
    assert sum_numbers_without_red('[1,{"c":"red","b":2},3]') == 4
    assert sum_numbers_without_red('{"d":"red","e":[1,2,3,4],"f":5}') == 0
    assert sum_numbers_without_red('[1,"red",5]') == 6
    print("Tests successful!")


if __name__ == "__main__":
    if len(sys.argv) > 1:
        if sys.argv[1] == "test":
            test()
            exit()
    with open("day12.input") as f:
        input_json = f.read()
    count = sum_numbers(input_json)
    print("Exercise 1: " + str(count))
    count_without_red = sum_numbers_without_red(input_json)
    print("Exercise 2: " + str(count_without_red))
