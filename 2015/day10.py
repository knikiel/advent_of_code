#!/usr/bin/python3
# encoding = utf-8

# adventofcode.com/day/10
# Day 10: Elves Look, Elves Say
# Santa's Elves play a game called look-and-say.
# 1) What is the lenght of the 40th number in the sequence?
# 1) What is the lenght of the 50th number in the sequence?

def next_number(key): 
    next_key = ""
    curr_digit = key[0]
    num_digits = 0
    for digit in key:
        if digit == curr_digit:
            num_digits += 1
        else:
            next_key += str(num_digits) + curr_digit
            curr_digit = digit
            num_digits = 1
    next_key += str(num_digits) + curr_digit
    return next_key


def play(key, game_length):
    sequence = [key]
    for i in range(game_length):
        sequence.append(next_number(sequence[i]))
    return sequence


def test():
    assert next_number("1") == "11"
    assert next_number("21") == "1211"
    assert next_number("111221") == "312211"

test()

if __name__ == "__main__":
    input_key = "1321131112"
    sequence = play(input_key, 50)
    len_number = len(sequence[40-1])
    print("Exercise 1: " + str(len_number))
    len_number_v2 = len(sequence[-1])
    print("Exercise 1: " + str(len_number_v2))
