#!/usr/bin/python3
# encoding = utf-8

# Day 21: RPG Simulator 20XX
# Little Henry Case got a new video game for Christmas.
# 1) How can he beat the boss with the least amount of money spent?
# 1) How can he lose againts the boss with the most amount of money spent?


from collections import namedtuple

Weapon = namedtuple("Weapon", ["cost", "damage"])
Armor = namedtuple("Armor", ["cost", "armor"])
Ring = namedtuple("Ring", ["cost", "damage", "armor"])
Stats = namedtuple("Stats", ["hit_points", "damage", "armor"])


def read_stats(input_stats):
    words = input_stats.split()
    stats = Stats(int(words[2]), int(words[4]), int(words[6]))
    return stats


def is_player_winning(player_stats, boss_stats):
    real_player_damage = max(player_stats.damage - boss_stats.armor, 1)
    real_boss_damage = max(boss_stats.damage - player_stats.armor, 1)
    turns_till_player_dead = player_stats.hit_points // real_boss_damage
    hp_left = player_stats.hit_points - turns_till_player_dead * real_boss_damage
    if hp_left > 0:
        turns_till_player_dead += 1
    turns_till_boss_dead = boss_stats.hit_points // real_player_damage
    hp_left = boss_stats.hit_points - turns_till_boss_dead * real_player_damage
    if hp_left > 0:
        turns_till_boss_dead += 1
    return turns_till_boss_dead <= turns_till_player_dead


def choose_equipment(boss_stats):
    weapons = [Weapon(8, 4), Weapon(10, 5), Weapon(25, 6), Weapon(40, 7), Weapon(74, 8)]
    armors = [Armor(13, 1), Armor(31, 2), Armor(53, 3), Armor(75, 4), Armor(102, 5)]
    no_armor = Armor(0, 0)
    armors_or_no_armor = armors.copy()
    armors_or_no_armor.append(no_armor)
    rings = [Ring(25, 1, 0), Ring(50, 2, 0), Ring(100, 3, 0),
            Ring(20, 0, 1), Ring(40, 0, 2), Ring(80, 0, 3)]
    rings_and_no_ring = rings.copy()
    no_ring = Ring(0, 0, 0)
    rings_and_no_ring.append(no_ring)
    player_stats = Stats(100, 0, 0)
    min_cost = 100000000
    for weapon in weapons:
        cost = weapon.cost
        damage_val = weapon.damage
        for armor in armors_or_no_armor:
            cost += armor.cost
            armor_val = armor.armor
            for ring1 in rings_and_no_ring:
                cost += ring1.cost
                if cost > min_cost:
                    cost -= ring1.cost
                    continue
                damage_val += ring1.damage
                armor_val += ring1.armor
                for ring2 in rings_and_no_ring:
                    if ring1 == ring2 and ring1 != no_ring:
                        continue
                    cost += ring2.cost
                    if cost >= min_cost:
                        cost -= ring2.cost
                        continue
                    damage_val += ring2.damage
                    armor_val += ring2.armor
                    player_stats = Stats(100, damage_val, armor_val)
                    if is_player_winning(player_stats, boss_stats):
                        print("equipment: " + str(weapon)+ " " +str(armor)+ " " +str(ring1)+ " " +str(ring2))
                        min_cost = cost
                    cost -= ring2.cost
                    damage_val -= ring2.damage
                    armor_val -= ring2.armor
                cost -= ring1.cost
                damage_val -= ring1.damage
                armor_val -= ring1.armor
            cost -= armor.cost
    return min_cost


def choose_equipment_badly(boss_stats):
    weapons = [Weapon(8, 4), Weapon(10, 5), Weapon(25, 6), Weapon(40, 7), Weapon(74, 8)]
    armors = [Armor(13, 1), Armor(31, 2), Armor(53, 3), Armor(75, 4), Armor(102, 5)]
    no_armor = Armor(0, 0)
    armors_or_no_armor = armors.copy()
    armors_or_no_armor.append(no_armor)
    rings = [Ring(25, 1, 0), Ring(50, 2, 0), Ring(100, 3, 0),
            Ring(20, 0, 1), Ring(40, 0, 2), Ring(80, 0, 3)]
    rings_and_no_ring = rings.copy()
    no_ring = Ring(0, 0, 0)
    rings_and_no_ring.append(no_ring)
    player_stats = Stats(100, 0, 0)
    max_cost = -1
    for weapon in weapons:
        cost = weapon.cost
        damage_val = weapon.damage
        for armor in armors_or_no_armor:
            cost += armor.cost
            armor_val = armor.armor
            for ring1 in rings_and_no_ring:
                cost += ring1.cost
                damage_val += ring1.damage
                armor_val += ring1.armor
                for ring2 in rings_and_no_ring:
                    if ring1 == ring2 and ring1 != no_ring:
                        continue
                    cost += ring2.cost
                    if cost <= max_cost:
                        cost -= ring2.cost
                        continue
                    damage_val += ring2.damage
                    armor_val += ring2.armor
                    player_stats = Stats(100, damage_val, armor_val)
                    if not is_player_winning(player_stats, boss_stats):
                        print("equipment: " + str(weapon)+ " " +str(armor)+ " " +str(ring1)+ " " +str(ring2))
                        max_cost = cost
                    cost -= ring2.cost
                    damage_val -= ring2.damage
                    armor_val -= ring2.armor
                cost -= ring1.cost
                damage_val -= ring1.damage
                armor_val -= ring1.armor
            cost -= armor.cost
    return max_cost


def main():
    input_stats = "Hit Points: 109\nDamage: 8\nArmor: 2"
    boss_stats = read_stats(input_stats)
    money_spent = choose_equipment(boss_stats)
    print("Exercise 1: " + str(money_spent))
    money_spent_badly = choose_equipment_badly(boss_stats)
    print("Exercise 2: " + str(money_spent_badly))


if __name__ == "__main__":
    main()
