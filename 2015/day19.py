#!/usr/bin/python3
# encoding = utf-8

import random
import re
import sys

# adventofcode.com/day/19
# Day 19: Medicine for Rudolph
# Rudolph is sick, but Red-Nosed Reindeer biology is quite peculiar.
# 1) Calibrate the medicin plant!
# 2) What is the number of steps to produce the medicin molecule?

def read_instructions(instructions_text):
    lines = instructions_text.splitlines()
    idx_molecule = 0
    rule_regex = re.compile(r"(\w+) => ([A-Z][a-z]?)+")
    seq_regex = re.compile(r"([A-Z][a-z]?)")
    rules = {}
    for i, line in enumerate(lines):
        match = rule_regex.match(line)
        if not match:
            idx_molecule = i+1
            break
        lhs = match.group(1)
        rhs = seq_regex.findall(line)
        if not lhs == "e":
            del rhs[0]
        if not lhs in rules:
            rules[lhs] = []
        rules[lhs].append(rhs)
    molecule = seq_regex.findall(lines[idx_molecule])
    return rules, molecule


def process_molecules(rules, old_molecules):
    seq_regex = re.compile(r"([A-Z][a-z]?|e)")
    prev_molecules = old_molecules
    next_molecules = set()
    for molecule_text in prev_molecules:
        molecule = seq_regex.findall(molecule_text)
        for j, atom in enumerate(molecule):
            if atom in rules:
                for rhs in rules[atom]:
                    new_molecule = molecule.copy()
                    new_molecule[j:j+1] = rhs
                    next_molecules.add("".join(new_molecule))
    return next_molecules 


def count_possible_molecules(rules, input_molecule, total_replacements):
    molecules = set(["".join(input_molecule)])
    for i in range(total_replacements):
        molecules = process_molecules(rules, molecules)
    return len(molecules)


def steps_till_molecule(rules, result_molecule):
    result_text = "".join(result_molecule)
    molecules = set(["e"])
    num_steps = 0
    while True:
        num_steps += 1
        molecules = process_molecules(rules, molecules)
        if result_text in molecules:
            return num_steps
        if num_steps >= 5:
            print(len(molecules))


def heuristic_steps_till_molecule(rules, result_molecule):
    # output: mapping of atoms to reversed rules (lhs, prev. and next atoms)
    def reverse_rules(rules):
        reversed_rules = {}
        for atom in result_molecule:
            reversed_rules[atom] = []
        for lhs, rules in rules.items():
            for rule in rules:
                for i, atom in enumerate(rule):
                    reversed_rule = {"prev": [], "next": []} 
                    for idx_prev in range(i-1, -1, -1):
                        prev = rule[idx_prev]
                        reversed_rule["prev"].append(prev)
                    for idx_next in range(i+1, len(rule)):
                        next_ = rule[idx_next]
                        reversed_rule["next"].append(next_)
                    reversed_rule["lhs"] = lhs
                    if not atom in reversed_rules:
                        reversed_rules[atom] = []
                    reversed_rules[atom].append(reversed_rule)
        return reversed_rules

    reversed_rules = reverse_rules(rules)
    #print(reversed_rules)
    min_num_steps = [len(result_molecule)+1]

    # output: list of (lhs, left_idx, right_idx) of rules
    def replacing_lhsides(molecule, i):
        suitable_lhsides = []
        if molecule[i] in reversed_rules:
            possible_rules = reversed_rules[molecule[i]]
            #print(molecule[i], possible_rules)
            for rule in possible_rules:
                rule_ok = True
                left_idx = i
                for prev_atom in rule["prev"]:
                    left_idx -= 1
                    if left_idx < 0 or molecule[left_idx] != prev_atom:
                        rule_ok = False
                right_idx = i
                for next_atom in rule["next"]:
                    right_idx += 1
                    if right_idx >= len(molecule) or molecule[right_idx] != next_atom:
                        rule_ok = False
                if rule_ok:
                    suitable_lhsides.append((rule["lhs"], left_idx, right_idx))
        #print(suitable_lhsides)
        return suitable_lhsides
    # output: molecule with replaced sequence of atoms, as specified by lhs
    def replace_sub(molecule, lhs):
        new_molecule = molecule.copy()
        atom, left_idx, right_idx = lhs
        #print("Replacing:", atom, left_idx, right_idx)
        new_molecule[left_idx:right_idx+1] = [atom]
        return new_molecule
    def thorough_step(molecule, num_steps):
        #print("Molecule:", molecule)
        if molecule == ["e"]:
            print("Found new best:", num_steps) 
            min_num_steps[0] = num_steps
            return
        elif num_steps+1 >= min_num_steps[0]:         
            #print("Too much steps.") 
            return
        num_steps += 1
        for i, atom in enumerate(molecule):
            for lhs in replacing_lhsides(molecule, i):
                 replaced_molecule = replace_sub(molecule, lhs)
                 thorough_step(replaced_molecule, num_steps)
    def random_step(molecule, num_steps):
        if molecule == ["e"]:
            print("Found new best:", num_steps) 
            min_num_steps[0] = num_steps
            return
        if num_steps+1 >= min_num_steps[0]:
            print("Too much steps.") 
            return
        elif len(molecule) <= 6:
            return thorough_step(molecule, num_steps)
        num_steps += 1
        molecule_size = len(molecule)
        for _ in range(max(10, molecule_size//2)):
            idx_atom = random.randrange(0, molecule_size)
            for lhs in replacing_lhsides(molecule, idx_atom):
                 replaced_molecule = replace_sub(molecule, lhs)
                 random_step(replaced_molecule, num_steps)

    random_step(result_molecule, 0)
    return min_num_steps[0]


def test():
    print("Testing starting...")
    test_instructions = "H => HO\nH => OH\nO => HH\ne => H\ne => O\n\nHOH"
    rules, molecule = read_instructions(test_instructions)
    assert rules == {"H":[["H","O"], ["O","H"]], "O":[["H","H"]], "e":[["H"],["O"]]}
    assert molecule == ["H", "O", "H"]
    print("Tests for exercise 1...")
    assert count_possible_molecules(rules, molecule, 1) == 4
    assert count_possible_molecules(rules, ["H","O","H","O","H","O"], 1) == 7
    print("Tests for exercise 2...")
    assert steps_till_molecule(rules, molecule) == 3
    assert steps_till_molecule(rules, ["H","O","H","O","H","O"]) == 6
    assert heuristic_steps_till_molecule(rules, ["H","O","H","O","H","O"]) == 6
    print("Testing finished!")


def main():
    with open("day19.input") as f:
        input_instructions = f.read()
    rules, molecule = read_instructions(input_instructions)
    num_molecules = count_possible_molecules(rules, molecule, 1)
    print("Exercise 1:", str(num_molecules))
    #num_steps = steps_till_molecule(rules, molecule)  # no solution, does not terminate early enough
    num_steps = heuristic_steps_till_molecule(rules, molecule)
    print("Exercise 2:", str(num_steps))


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "test":
        test()
    else:
        main()
