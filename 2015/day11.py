#!/usr/bin/python3
# encoding = utf-8

# adventofcode.com/day/11
# Day 11: Corporate Policy
# Santa's previous password expired, and he needs help choosing a new one.
# 1) What should his next password be?
# 2) And after that?

def is_password_valid(password):
    # 1) no invalid characters i, o, l
    if any(char in "iol" for char in password):
        return False
    # 2) sequence of three increasing letters
    sequence_found = False
    sequence_length = 1
    prev_char = password[0]
    for char in password[1:]:
        if ord(prev_char)+1 == ord(char):
            sequence_length += 1
        else:
            sequence_length = 1
        if sequence_length == 3:
            sequence_found = True
            break
        prev_char = char
    if not sequence_found:
        return False
    # 3) two different, nonoverlapping pairs of letters
    num_pairs_found = 0
    pair_letter = None
    prev_char = password[0]
    for char in password[1:]:
        if char == prev_char:
            if char != pair_letter:
                num_pairs_found += 1
                pair_letter = char
        prev_char = char
    return num_pairs_found >= 2


def compute_next_password(old_password):
    word_list = list(old_password)
    while True:
        for i in range(len(word_list)):
            curr_char = word_list[-i-1]
            if curr_char == 'z':
                word_list[-i-1] = 'a'
            else:  # 'a' <= curr_char <= 'y'
                word_list[-i-1] = chr(ord(curr_char)+1)
                break
        possible_password = "".join(word_list)
        if is_password_valid(possible_password):
            return possible_password

def test():
    assert not is_password_valid("hijklmmn")
    assert not is_password_valid("abbceffg")
    assert not is_password_valid("abbcegjk")
    assert is_password_valid("abcdffaa")
    assert is_password_valid("ghjaabcc")
    assert compute_next_password("abcdefgh") == "abcdffaa"
    # next line work, but test requires to much time
    #assert compute_next_password("ghijklmn") == "ghjaabcc"

    
test()


if __name__ == "__main__":
    input_password = "hxbxwxba"
    next_password = compute_next_password(input_password)
    print("Exercise 1: " + next_password)
    next_next_password = compute_next_password(next_password)
    print("Exercise 2: " + next_next_password)
