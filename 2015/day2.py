#!usr/bin/python3
#encoding = utf-8

# http://adventofcode.com/day/2
# Day 2: I Was Told there Would Be No Math
# Elves have to order some wrapping paper and ribbons for the presents.
# 1) How many square feet do they need?
# 2) How many total feet of ribbon should they order?

with open("day2.input") as f:
    dimensions_input = f.read()

def compute_wrapping_paper_consumption(l, w, h):
    lw = l*w
    wh = w*h
    lh = l*h
    if lw <= wh and lw <= lh:
        min_face = lw
    elif wh <= lw and wh <= lh:
        min_face = wh
    else:
        min_face = lh
    return 2*lw + 2*wh + 2*lh + min_face

def compute_total_wrapping_paper_consumption(dimensions):
    total_amount = 0
    for line in dimensions.splitlines():
        l, w, h = line.split("x")
        total_amount += compute_wrapping_paper_consumption(int(l), int(w), int(h))
    return total_amount

def compute_ribbon_consumption(l, w, h):
    if l <= h and w <= h:
        min1 = l
        min2 = w
    elif w <= l and h <= l:
        min1 = w
        min2 = h
    else:
        min1 = l
        min2 = h
    volume = l*w*h
    return 2*min1 + 2*min2 + volume

def compute_total_ribbon_consumption(dimensions):
    total_amount = 0
    for line in dimensions.splitlines():
        l, w, h = line.split("x")
        total_amount += compute_ribbon_consumption(int(l), int(w), int(h))
    return total_amount

if __name__ == "__main__":
    amount = compute_total_wrapping_paper_consumption(dimensions_input)
    print("Aufgabe 1: " + str(amount))
    amount = compute_total_ribbon_consumption(dimensions_input)
    print("Aufgabe 2: " + str(amount))
