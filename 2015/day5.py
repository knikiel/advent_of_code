#!/usr/bin/python3
# encoding = utf-8

# adventofcode.com/day/5
# Day 5: Doesn't He HaveIntern-Elves For This?
# Santa has a very strict Definition of nice strings.
# 1) How many strings in the text are nice?
# 2) How many strings in the text are nice according to his new definition?

with open("day5.input") as f:
    text_input = f.read()


def is_nice_string(string):
    forbidden_substrings = ["ab", "cd", "pq", "xy"]
    prev_char = None
    num_vowels = 0
    letter_twice = False
    for char in string:
        if char in "aeiou":
            num_vowels += 1
        if prev_char:
            if prev_char == char:
                letter_twice = True
            two_chars = prev_char + char
            if two_chars in forbidden_substrings: 
                return False
        prev_char = char
    return num_vowels >= 3 and letter_twice


def is_nice_string_v2(string):
    found_substrings = set()
    substring_twice_found = False
    separating_letter_found = False
    prev_prev_char = None
    prev_char = None
    overlapping = False
    for char in string:
        if prev_char:
            last_two_chars = prev_char + char
            if not overlapping or not prev_prev_char == prev_char == char:
                if last_two_chars in found_substrings:
                    substring_twice_found = True
                else:
                    overlapping = True
            else:
                overlapping = False
            found_substrings.add(last_two_chars) 
            if prev_prev_char:
                if prev_prev_char == char:
                    separating_letter_found = True
        prev_prev_char = prev_char
        prev_char = char
    return substring_twice_found and separating_letter_found


def number_nice_strings(text):
    count = 0
    for word in text.split():
        if is_nice_string(word):
            count += 1
    return count


def number_nice_strings_v2(text):
    count = 0
    for word in text.split():
        if is_nice_string_v2(word):
            count += 1
    return count


def test():
    assert is_nice_string("ugknbfddgicrmopn")
    assert is_nice_string("aaa")
    assert not is_nice_string("jchzalrnumimhmhp")
    assert not is_nice_string("haegwjzuvuyypxyu")
    assert not is_nice_string("dvszwmarrgswjxmb")

    assert is_nice_string_v2("qjhvhtzxzqqjkmpb")
    assert is_nice_string_v2("xxyxx")
    assert not is_nice_string_v2("uurcxstgmygtbstg")
    assert not is_nice_string_v2("ieodomkazucvgmuy")


if __name__ == "__main__": 
    test()
    count = number_nice_strings(text_input)
    print("Exercise 1: " + str(count))
    count_v2 = number_nice_strings_v2(text_input)
    print("Exercise 2: " + str(count_v2))
