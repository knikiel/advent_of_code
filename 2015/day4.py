#!/usr/bin/python3
# encoding = utf-8

# http://adventofcode.com/day/4
# Day 4: The Ideal Stocking Stuffer
# Santa wants to gift AdventCoins for Christmas but he still has to mine them.
# 1) Help Santa mine an AdventCoin!
# 2) Help Santa mine anouther kind of AdventCoin!

import hashlib

secret_key_input = "ckczppom"

def mine_advent_coin(secret_key):
    next_number = 0
    while True: 
        md5 = hashlib.md5()
        secret_key_extended = secret_key + str(next_number)
        md5.update(secret_key_extended.encode("utf-8"))
        hash_string = md5.hexdigest()
        if hash_string.startswith("00000"):
            return next_number
        next_number += 1

def mine_advent_coin_v2(secret_key):
    next_number = 0
    while True: 
        md5 = hashlib.md5()
        secret_key_extended = secret_key + str(next_number)
        md5.update(secret_key_extended.encode("utf-8"))
        hash_string = md5.hexdigest()
        if hash_string.startswith("000000"):
            return next_number
        next_number += 1

def test():
    assert mine_advent_coin("abcdef") == 609043
    assert mine_advent_coin("pqrstuv") == 1048970

if __name__ == "__main__":
    test()
    advent_coin = mine_advent_coin(secret_key_input)
    print("Exercise 1: " + str(advent_coin))
    advent_coin_v2 = mine_advent_coin_v2(secret_key_input)
    print("Exercise 2: " + str(advent_coin_v2))
