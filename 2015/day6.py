#!/usr/bin/python3
# encoding = utf-8

# adventofcode.com/day/6
# Day 6: Probably a Fire Hazard
# You follow Santa's instructions to light your christmas lights.
# 1) How many lights are lit?
# 2) What is the total brightness of the light according to the correct translations?

with open("day6.input") as f:
    instructions_input = f.read()


def extract_coordinates(word):
    x_str, y_str = word.split(',')
    return int(x_str), int(y_str)


def manipulate_light_grid(grid, from_pos, to_pos, function):
    from_x, from_y = from_pos
    to_x, to_y = to_pos
    for y in range(from_y, to_y+1):
        row = grid[y]
        for x in range(from_x, to_x+1):
            row[x] = function(row[x])


def follow_instruction(light_grid, instruction):
    words = instruction.split()
    if words[0] == "turn":
        if words[1] == "on":
            from_pos = extract_coordinates(words[2])
            to_pos = extract_coordinates(words[4])
            manipulate_light_grid(light_grid, from_pos, to_pos, lambda cell: True)
        elif words[1] == "off":
            from_pos = extract_coordinates(words[2])
            to_pos = extract_coordinates(words[4])
            manipulate_light_grid(light_grid, from_pos, to_pos, lambda cell: False)
    else:  # toggle
        from_pos = extract_coordinates(words[1])
        to_pos = extract_coordinates(words[3])
        manipulate_light_grid(light_grid, from_pos, to_pos, lambda cell: not cell)


def number_lit_lights(instructions):
    light_grid = list()
    for i in range(0,1000):
        light_row = list()
        for j in range(0, 1000):
            light_row.append(False)
        light_grid.append(light_row)
    for instruction in instructions.splitlines():
        follow_instruction(light_grid, instruction)
    count = 0
    for light_row in light_grid:
        for lit in light_row:
            if lit:
                count += 1
    return count


def follow_instruction_v2(light_grid, instruction):
    words = instruction.split()
    if words[0] == "turn":
        if words[1] == "on":
            from_pos = extract_coordinates(words[2])
            to_pos = extract_coordinates(words[4])
            manipulate_light_grid(light_grid, from_pos, to_pos, lambda cell: cell+1)
        elif words[1] == "off":
            from_pos = extract_coordinates(words[2])
            to_pos = extract_coordinates(words[4])
            manipulate_light_grid(light_grid, from_pos, to_pos, lambda cell: max(cell-1, 0))
    else:  # toggle
        from_pos = extract_coordinates(words[1])
        to_pos = extract_coordinates(words[3])
        manipulate_light_grid(light_grid, from_pos, to_pos, lambda cell: cell+2)


def number_lit_lights_v2(instructions):
    light_grid = list()
    for i in range(0,1000):
        light_row = list()
        for j in range(0, 1000):
            light_row.append(0)
        light_grid.append(light_row)
    for instruction in instructions.splitlines():
        follow_instruction_v2(light_grid, instruction)
    total_brightness = 0
    for light_row in light_grid:
        for brightness in light_row:
            total_brightness += brightness
    return total_brightness

if __name__ == "__main__":
    count = number_lit_lights(instructions_input)
    print("Exercise 1: " + str(count))
    count_v2 = number_lit_lights_v2(instructions_input)
    print("Exercise 2: " + str(count_v2))


