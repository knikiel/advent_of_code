#!/usr/bin/python3
# encoding = utf-8

import re

def read_aunts(in_aunt_text):
    regex = re.compile("Sue (\d+): (\w+): (\d+), (\w+): (\d+), (\w+): (\d+)")
    aunts = []
    for aunt_text in in_aunt_text.splitlines():
        match = regex.match(aunt_text)
        aunt = {
            "id": int(match.group(1)),
            "properties": {
                match.group(2): int(match.group(3)),
                match.group(4): int(match.group(5)),
                match.group(6): int(match.group(7)),
            }}
        aunts.append(aunt)
    return aunts


def find_aunt(in_aunts, in_contents):
    possible_aunts = []
    for aunt in in_aunts:
        is_possible_aunt = True
        for key, value in aunt["properties"].items():
            if value != in_contents[key]:
                is_possible_aunt = False
                break
        if is_possible_aunt:
            possible_aunts.append(aunt)
    return possible_aunts[0]["id"]
    

def find_aunt_v2(in_aunts, in_contents):
    possible_aunts = []
    for aunt in in_aunts:
        is_possible_aunt = True
        for key, value in aunt["properties"].items():
            if key == "cats" or key == "trees":
                if not value > in_contents[key]:
                    is_possible_aunt = False
                    break
            elif key == "pomeranians" or key == "goldfish":
                if not value < in_contents[key]:
                    is_possible_aunt = False
                    break
            else:
                if not value == in_contents[key]:
                    is_possible_aunt = False
                    break
        if is_possible_aunt:
            possible_aunts.append(aunt)
    return possible_aunts[0]["id"]


def main():
    with open("day16.input") as f:
        input_aunts = f.read()
    contents = {
            "children": 3,
            "cats": 7,
            "samoyeds": 2,
            "pomeranians": 3,
            "akitas": 0,
            "vizslas": 0,
            "goldfish": 5,
            "trees": 3,
            "cars": 2,
            "perfumes": 1
            }
    aunts = read_aunts(input_aunts)
    aunt_id = find_aunt(aunts, contents)
    aunt_id_v2 = find_aunt_v2(aunts, contents)
    print("Exercise 1: " + str(aunt_id))
    print("Exercise 2: " + str(aunt_id_v2))


if __name__ == "__main__":
    main()
