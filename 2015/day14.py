#!/usr/bin/python3
# encoding = utf-8

# Day 14: Reindeer Olympics
# The Reindeer Olympics take place this year and the reindeer race for the win.
# 1) What distance has the winning reindeer travelled?
# 2) How many points has the winning reindeer collected?

import re
import sys


def read_stats(stats_text):
    regex = re.compile(r"(\w+) can fly (\d+) km/s for (\d+) seconds, but then must rest for (\d+) seconds.")
    stats = []
    for stats_line in stats_text.splitlines():
        match = regex.match(stats_line)
        speed = int(match.group(2))
        flying_time = int(match.group(3))
        resting_time = int(match.group(4))
        stats.append({"speed":speed, "flying_time":flying_time, "resting_time":resting_time})
    return stats


def max_distance(stats, total_time):
    distances = [0 for x in stats]
    points = [0 for x in stats]
    times_till_rest = [stat["flying_time"] for stat in stats]
    for time in range(1, total_time+1):
        for i in range(len(stats)):
            if times_till_rest[i] == 0:
                times_till_rest[i] = -stats[i]["resting_time"]+1
            elif times_till_rest[i] < 0:
                times_till_rest[i] += 1
                if times_till_rest[i] == 0:
                    times_till_rest[i] = stats[i]["flying_time"]
            else:
                distances[i] += stats[i]["speed"]    
                times_till_rest[i] -= 1
        max_dist = max(distances)
        for i in range(len(stats)):
            if distances[i] == max_dist:
                points[i] += 1
    max_points = max(points)
    return max_dist, max_points


def test():
    print("Test starting...")
    # (speed[km/s], flying_time[s], resting_time[s])
    test_data = [{"speed":14, "flying_time":10, "resting_time":127},
            {"speed":16, "flying_time":11, "resting_time":162}]
    assert max_distance(test_data, 1000) == 1120
    assert max_points(test_data, 1000) == 689
    print("Test successful!")


def main():
    with open("day14.input") as f:
        input_stats = f.read()
    stats = read_stats(input_stats)
    winning_distance, winning_points = max_distance(stats, 2503)
    print("Exercise 1: " + str(winning_distance))
    print("Exercise 2: " + str(winning_points))


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "test":
        test()
    else:
        main()
