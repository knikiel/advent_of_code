#!/usr/bin/python3
# encoding = utf-8

# Day 8: Matchsticks
# Santa uses a digital copy of the present list during his deliveries.
# 1) How much addition space on his hard drive does he need for the special characters?
# 2) Encode the encoded list!

# does not test if text is encoded correctly
def count_characters(text):
    working_text = text.splitlines()
    total_real_chars = 0
    total_bytes = 0
    for line in working_text:
        num_chars_to_ignore = 0
        prev_char = None
        for char in line:
            total_bytes += 1
            if num_chars_to_ignore == 0:
                if char == '"':
                    pass
                elif prev_char != None and prev_char == '\\':
                    if char == 'x':
                        num_chars_to_ignore = 2  # hexcode
                    else:  # char == '\\' or char == '"':
                        char = None
                elif char == '\\':
                    total_real_chars += 1
                else:  # simple character
                    total_real_chars += 1
            else:
                num_chars_to_ignore -= 1
            prev_char = char
    return total_real_chars, total_bytes
            

def encode(text):
    encoded_text = '"'
    for char in text:
        if char == '\\' or char == '"':
            encoded_text += '\\'
        encoded_text += char
    encoded_text += '"'
    return encoded_text


def encode_total_text(text):
    lines = text.splitlines()
    encoded_lines = list()
    for line in lines:
        encoded_lines.append(encode(line))
    return '\n'.join(encoded_lines)


def test():
    assert count_characters(r'""') == (0, 2)
    assert count_characters(r'"abc"') == (3, 5)
    assert count_characters(r'"aaa\"aaa"') == (7, 10)
    assert count_characters(r'"\x27"') == (1, 6)
    assert count_characters(r'"a\\bc\"d\x27ef"') == (9, 16)
    assert encode(r'""') == r'"\"\""'
    assert encode(r'"abc"') == r'"\"abc\""'
    assert encode(r'"aaa\"aaa"') == r'"\"aaa\\\"aaa\""'
    assert encode(r'"\x27"') == r'"\"\\x27\""'

test()

if __name__ == "__main__":
    with open("day8.input") as f:
        input_text = f.read()
    total_real_chars, total_bytes = count_characters(input_text)
    print("Exercise 1: " + str(total_bytes - total_real_chars))
    encoded_input_text = encode_total_text(input_text)
    total_real_chars_v2, total_bytes_v2 = count_characters(encoded_input_text)
    print("Exercise 2: " + str(total_bytes_v2 - total_real_chars_v2))
