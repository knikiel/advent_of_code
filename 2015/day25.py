#!/usr/bin/python3
# encoding = utf-8

# adventofcode.com/day/25
# Day 25: Let it Snow
# Santa is booting up his weather machine but can't find the passwords.
# 1) What is the correct password according to the method described on the service phone?

import sys

def compute_password(target_row, target_col):
    first_password = 20151125
    password = first_password
    row, col = 1, 1
    max_row, max_col = 1, 1
    while not (row == target_row and col == target_col):
        if row == 1:
            max_row += 1
            row = max_row
            #max_col = col
            col = 1
        else: 
            row -= 1
            col += 1
        password = password * 252533 % 33554393
    return password



def test():
    assert compute_password(1, 1) == 20151125
    assert compute_password(2, 1) == 31916031
    assert compute_password(1, 2) == 18749137
    assert compute_password(6, 6) == 27995004


def main():
    row, col = 3010, 3019
    password = compute_password(row, col)
    print("Exercise 1:", password)

if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "test":
        test()
    else:
        main()
