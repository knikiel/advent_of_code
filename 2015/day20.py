#!/usr/bin/python3
# encoding = utf-8

# adventofcode.com/day/20
# Day 20: Infinite Elves and Infinite Houses
# Infinite Elves deliver presents to an infinite number of houses.
# 1) What is the lowest house number of a house with at least 36000000 presents?
# 2) The Elves adopt a new system. What is now the lowest house number?

from copy import deepcopy
from fractions import gcd
import random
import sys

# randomized algorithm (Solovay-Strassen))
# may accept primes
def is_prime(n):
    # look at chapter 25 of GtI
    def jacobi_mod():
        # not necessary any more, solution computed (guessed)
        pass
    if n % 2 == 0:
        return False
    a = random.randint(1, n-1)
    if gcd(a, n) > 1:
        return False
    jacob = jacobi_mod(a, n)
    pot = pow(a, (n-1)/2, n)
     

# computes a factor (not always prime!)
def pollard_brent(n):
    if n % 2 == 0:
        return 2
    y, c, m = random.randint(1, n-1), random.randint(1, n-1), random.randint(1, n-1)
    g, r, q = 1, 1, 1
    while g == 1:
        x = y
        for i in range(r):
            y = ((y*y) % n + c) % n
        k = 0
        while k < r and g == 1:
            ys = y
            for i in range(min(m, r-k)):
                y = ((y*y) % n + c) % n
                q = q * abs(x-y) % n
            g = gcd(q,n)
            k = k + m
        r = r * 2
    if g == n:
        while True:
            ys = ((ys*ys) % n + c) % n
            g = gcd(abs(x-ys), n)
            if g > 1:
                break
    return g


def number_of_house(num_presents):
    # problem descriptions is basically: num_presents > sum(factors of house number) * 10
    # because the problem is "hard", we will use some heuristics and hope that our solution is correct
    # or maybe use Pollard-Brent first to find factors
    min_sum_factors = num_presents//10
    orig_number = 1
    sum_factors = 1
    while sum_factors < min_sum_factors:
        orig_number += 1
        primes = []
        numbers = [orig_number]
        while len(numbers) > 0:
            number = numbers[-1]
            del numbers[-1]
            while number != 1:
                result = pollard_brent(number)
                number = number // result
                if number == 1:
                    primes.append(result)
                    break
                numbers.append(result)
        factors = set([1 , orig_number])
        prev_num_factors = 0
        while prev_num_factors != len(factors):
            prev_factors = deepcopy(factors)
            prev_num_factors = len(factors)
            for prime in primes:
                for factor in prev_factors:
                    product = prime * factor
                    if product > orig_number or orig_number % product != 0:
                        continue
                    factors.add(product)
        sum_factors = sum(factors)
        if orig_number % 10001 == 0:
            print("Progress output: House No. " + str(orig_number) + ", Presents: " + str(sum_factors*10)) 
        if orig_number % 50005 == 0:
            print(" -> prime factors: " + str(primes))
    return orig_number, sum_factors*10


def number_of_house_v2(num_presents):
    # problem descriptions is simple modification to first version
    # core of algorithm identical to first version
    # correct factorization is less important
    min_sum_factors = num_presents
    orig_number = 1
    sum_factors = 1
    while sum_factors < min_sum_factors:
        orig_number += 1
        primes = []
        numbers = [orig_number]
        while len(numbers) > 0:
            number = numbers[-1]
            del numbers[-1]
            while number != 1:
                result = pollard_brent(number)
                number = number // result
                if number == 1:
                    primes.append(result)
                    break
                numbers.append(result)
        factors = set([1 , orig_number])
        prev_num_factors = 0
        while prev_num_factors != len(factors):
            prev_factors = deepcopy(factors)
            prev_num_factors = len(factors)
            for prime in primes:
                for factor in prev_factors:
                    product = prime * factor
                    if product > orig_number or orig_number % product != 0:
                        continue
                    factors.add(product)
        # different to above
        sum_factors = 0
        for factor in factors:
            if factor * 50 < orig_number:
                continue
            sum_factors += (11 * factor)
        if orig_number % 10001 == 0:
            print("Progress output: House No. " + str(orig_number) + ", Presents: " + str(sum_factors*10)) 
        if orig_number % 50005 == 0:
            print(" -> prime factors: " + str(primes))
    print("=> primes factors: " + str(primes))
    return orig_number, sum_factors*10


def test():
    #orig_number = 123*14412*128*3*421*12345+1
    #orig_number = 7385527
    #orig_number = 65536 * 3
    #orig_number = 23310
    #orig_number = 831600  # solution to 1)
    #orig_number = 884520  # solution to 2)
    print("number: " + str(orig_number))
    primes = []
    numbers = [orig_number]
    while len(numbers) > 0:
        number = numbers[-1]
        print("=>" + str(number))
        del numbers[-1]
        while number != 1:
            print(" |" + str(number))
            result = pollard_brent(number)
            number = number // result
            if number == 1:
                primes.append(result)
                print("  => prime")
                break
            numbers.append(result)
            print("  -> " + str(result) + " [" + str(number) + "]")
    print("prime factors: " + str(primes))

    factors = set([1 , orig_number])
    prev_num_factors = 0
    while prev_num_factors != len(factors):
        prev_factors = deepcopy(factors)
        prev_num_factors = len(factors)
        for prime in primes:
            for factor in prev_factors:
                product = prime * factor
                if product > orig_number or orig_number % product != 0:
                    continue
                factors.add(product)
    factor_list = list(factors)
    factor_list.sort()
    print("all factors: " + str(factor_list))
    print("sum of factors: " + str(sum(factors)))


def main():
    input_number = 36000000
    # takes time
    #number = number_of_house(input_number)
    #print("Exercise 1: " + str(number))
    # takes long again
    number = number_of_house_v2(input_number)
    print("Exercise 2: " + str(number))

if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "test":
        test()
    else:
        main()
