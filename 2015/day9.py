#!/usr/bin/python3
# encoding = utf-8

# adventofcode/day/9
# Day 9: All in a Single Night
# Santa has an updated list of locations to visit and not much time.
# 1) What is the shortest path to visit all the cities?
# 2) And the longest? Santa wants to show off.


import sys

def read_graph(distances):
    def read_edge(graph, line):
        words = line.split()
        source_name = words[0]
        target_name = words[2]
        distance = int(words[4])
        source = graph.get(source_name)
        if source == None:
            source = dict()
            graph[source_name] = source
        target = graph.get(target_name)
        if target == None:
            target = dict()
            graph[target_name] = target
        source[target_name] = distance
        target[source_name] = distance
    graph = {}
    for line in distances.splitlines():
        read_edge(graph, line)
    return graph

def compute_tsp_path(graph):
    def shortest_tsp_path(graph, city, cities_to_visit):
        if len(cities_to_visit) == 0:
            return 0
        distances = list()
        for next_city_name in cities_to_visit:
            distance_to_next = city.get(next_city_name) 
            if distance_to_next == None:
                continue
            other_cities = cities_to_visit.copy()
            other_cities.remove(next_city_name)
            distance_from_next = shortest_tsp_path(graph, \
                    graph[next_city_name], other_cities)
            if distance_from_next == sys.maxsize:
                continue
            distance = distance_to_next + distance_from_next
            distances.append(distance)
        if len(distances) == 0:
            distances.append(sys.maxsize)
        return min(distances)
    cities = list(graph.keys())
    source = dict()
    for city in cities:
        source[city] = 0
    return shortest_tsp_path(graph, source, cities)


def compute_longest_tsp_path(graph):
    def longest_tsp_path(graph, city, cities_to_visit):
        if len(cities_to_visit) == 0:
            return 0
        distances = list()
        for next_city_name in cities_to_visit:
            distance_to_next = city.get(next_city_name) 
            if distance_to_next == None:
                continue
            other_cities = cities_to_visit.copy()
            other_cities.remove(next_city_name)
            distance_from_next = longest_tsp_path(graph, \
                    graph[next_city_name], other_cities)
            if distance_from_next == -1:
                continue
            distance = distance_to_next + distance_from_next
            distances.append(distance)
        if len(distances) == 0:
            distances.append(-1)
        return max(distances)
    cities = list(graph.keys())
    source = dict()
    for city in cities:
        source[city] = 0
    return longest_tsp_path(graph, source, cities)


if __name__ == "__main__":
    with open("day9.input") as f:
        input_distances = f.read()
    graph_repr = read_graph(input_distances)   
    shortest_path = compute_tsp_path(graph_repr)
    print("Exercise 1: " + str(shortest_path))
    longest_path = compute_longest_tsp_path(graph_repr)
    print("Exercise 2: " + str(longest_path))
