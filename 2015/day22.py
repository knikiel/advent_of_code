#!/usr/bin/python3
# encoding = utf-8

# adventofcode.com/day/22
# Day 22: Wizard Simulator 20XX
# Little Henry Case wants to play as a mage now.
# 1) What is the least amount of spent mana to beat the boss?
# 2) And in hard mode?

from collections import namedtuple
from itertools import product, permutations, repeat
import random 

Stats = namedtuple("Stats", ["hit_points", "damage"])
Spell = namedtuple("Spell", ["name", "cost"])
DamageSpell = namedtuple("DamageSpell", ["name", "cost", "total_damage"])


def is_player_winning(used_spells, boss_stats, hard_mode = False):
    player_mana = 500
    player_hit_points = 50
    boss_hit_points = boss_stats.hit_points
    boss_damage = boss_stats.damage
    turns_left_poison = 0
    turns_left_shield = 0
    turns_left_recharge = 0
    for spell in used_spells:
        # begin of player's turn
        if hard_mode:
            player_hit_points -= 1
            if player_hit_points <= 0:
                return False
        if turns_left_poison > 0:
            boss_hit_points -= 3
            turns_left_poison -= 1
        if turns_left_shield > 0:
            turns_left_shield -= 1
        if turns_left_recharge > 0:
            player_mana += 101
            turns_left_recharge -= 1
        if boss_hit_points <= 0:
            return True
        # player casts a spell
        player_mana -= spell.cost
        if player_mana < 0:
            #print("Not enough mana!")
            return False
        if spell.name == "Magic Missile":
            boss_hit_points -= 4
        elif spell.name == "Drain":
            boss_hit_points -= 2
            player_hit_points += 2
        elif spell.name == "Shield":
            if turns_left_shield > 0:
                return False
            turns_left_shield = 6
        elif spell.name == "Poison":
            if turns_left_poison > 0:
                return False
            turns_left_poison = 6
        elif spell.name == "Recharge":
            if turns_left_recharge > 0:
                return False
            turns_left_recharge = 5
        else:
            raise Exception()
        if boss_hit_points <= 0:
            return True
        # begin of boss's turn
        if turns_left_poison > 0:
            boss_hit_points -= 3
            turns_left_poison -= 1
        if turns_left_shield > 0:
            player_armor = 7
            turns_left_shield -= 1
        else:
            player_armor = 0
        if turns_left_recharge > 0:
            player_mana += 101
            turns_left_recharge -= 1
        if boss_hit_points <= 0:
            return True
        # boss attacks
        player_hit_points -= max(boss_damage - player_armor, 1)
        if player_hit_points <= 0:
            return False
    return False

# too many permutations
def vanquish_boss_naively(boss_stats):
    all_spells = [Spell("Magic Missile", 53), Spell("Drain", 73), Spell("Shield", 113),
            Spell("Poison", 173), Spell("Recharge", 229)]  # must stay ordered by cost
    # do not waste time by using too few spells
    num_spells = boss_stats.hit_points // 4 + 1
    while True:
        print("Number of used spells: ", num_spells)
        for used_spells in product(*repeat(all_spells, num_spells)):
            print("->Used spells:")
            for spell in used_spells:
                print(spell.name, end=', ')
            print()
            for used_spells_ordered in permutations(used_spells):
                if is_player_winning(used_spells_ordered, boss_stats):
                    return spent_mana
        num_spells += 1
    # do not even bother with return value


# use a clever strategy
def vanquish_boss(boss_stats):
    # strategy based on heuristics
    missile = Spell("Magic Missile", 53)
    drain = Spell("Drain", 73)
    shield = Spell("Shield", 113),
    poison = Spell("Poison", 173)
    recharge = Spell("Recharge", 229)
    num_missiles = 0
    num_drains = 0
    num_shields = 0
    num_poisons = 0
    num_recharges = 0
    min_num_turns = boss_stats.hit_points // 4 + 1
    while True:
        pass
        # TODO
        #num_recharges = (spent_mana - 500) // 505
        #total_spent_mana = spent_mana + (spent_mana - 500) * recharge.cost
        #num_turns_till_boss_dead = boss_stats.hit_points - num_poisons * 
        #boss_damage
        #num_turns_till_player_dead = (50 - (num_shields * max(7 - boss_stats.damage, 1)))
        #if num_turns_till_player_dead >= num_turns_till_boss_dead:
        #    return total_spend_mana


def vanquish_boss_randomly(boss_stats, total_tries, hard_mode = False):
    missile = DamageSpell("Magic Missile", 53, 4)
    drain = DamageSpell("Drain", 73, 2)
    poison = DamageSpell("Poison", 173, 6 * 3)
    shield = Spell("Shield", 113)
    recharge = Spell("Recharge", 229)
    all_damage_spells = [drain, missile, poison]
    damage_to_deal = boss_stats.hit_points
    min_mana_used = 10000000
    for i in range(total_tries):
        #print("Trial", i)
        num_missiles = 0
        num_poisons = 0
        num_drains = 0
        damage = 0
        while damage < damage_to_deal:
            random_spell = random.choice(all_damage_spells)
            damage += random_spell.total_damage
            if random_spell == missile:
                num_missiles += 1
            elif random_spell == drain:
                num_drains += 1
            elif random_spell == poison:
                num_poisons += 1
        random_variable = random.choice([0, 0, 0, 1])
        num_drains += random_variable
        for num_shields in range((num_missiles + num_drains + num_poisons) // 3 + 1):
            mana_used = num_missiles * missile.cost + num_poisons * poison.cost + num_drains * drain.cost
            mana_used += num_shields * shield.cost
            num_recharges = 0
            while num_recharges * (5 * 101) + 500 < mana_used:
                num_recharges += 1
                mana_used += recharge.cost
            #print("Total mana used:", mana_used)
            if mana_used >= min_mana_used:
                break
            for j in range(4):
                spells = []
                num_shields_left = num_shields
                turns_to_next_shield = 1
                num_recharges_left = num_recharges
                turns_to_next_recharge = 1
                num_poisons_left = num_poisons
                turns_to_next_poison = 1
                num_drains_left = num_drains
                num_missiles_left = num_missiles
                while (num_shields_left > 0 or num_recharges_left > 0 
                        or num_poisons_left > 0 or num_drains_left > 0):
                    turns_to_next_shield -= 1
                    turns_to_next_recharge -= 1
                    turns_to_next_poison -= 1
                    possible_spells = []
                    if num_shields_left > 0 and turns_to_next_shield <= 0:
                        possible_spells.extend([shield, shield, shield])
                    if num_recharges_left > 0 and turns_to_next_recharge <= 0:
                        possible_spells.extend([recharge, recharge, recharge])
                    if num_poisons_left > 0 and turns_to_next_poison <= 0:
                        possible_spells.extend([poison, poison])
                    if num_drains_left > 0:
                        possible_spells.extend([drain])
                    if len(possible_spells) > 0:
                        chosen_spell = random.choice(possible_spells)
                        spells.append(chosen_spell)
                        if chosen_spell == shield:
                            num_shields_left -= 1
                            turns_to_next_shield = 3
                        elif chosen_spell == recharge:
                            num_recharges_left -= 1
                            turns_to_next_recharge = 3
                        elif chosen_spell == poison:
                            num_poisons_left -= 1
                            turns_to_next_poison = 3
                        elif chosen_spell == drain:
                            num_drains_left -= 1
                    else:
                        num_missiles_left -= 1
                        spells.append(missile)
                for k in range(num_missiles_left):
                    spells.append(missile)
                #print("Spells for fight:", [spell.name for spell in spells])
                if is_player_winning(spells, boss_stats, hard_mode):
                    print("Better spells:", [spell.name for spell in spells], "\n => mana used:", mana_used)
                    min_mana_used = mana_used
                    break
                elif hard_mode and is_player_winning(spells, boss_stats, False):
                    #print("Better spells:", [spell.name for spell in spells], "\n => mana used:", total_mana_used)
                    #print("Death by attrition")
                    pass
    return min_mana_used



def main():
    boss_stats = Stats(55, 8)  # directly from my input file
    #boss_stats = Stats(51, 9)  # other input
    mana_spent = vanquish_boss_randomly(boss_stats, 10000)
    print("Exercise 1: ", mana_spent)
    mana_spent_in_hard_mode = vanquish_boss_randomly(boss_stats, 10000, True)
    print("Exercise 2: ", mana_spent_in_hard_mode)


if __name__ == "__main__":
    main() 
