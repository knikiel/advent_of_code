#!/usr/bin/python3
# encoding = utf-8

# adventofcode.com/day/23
# Day 23: Opening the Turing Lock
# Little Jane Marie just got her very first computer for christmas.
# 1) What is the output of the example program?
# 2) What is the output with a different starting state?

import sys

def read_program(program_text):
    def decode(r):
        if r == "a":
            return 0
        else:
            return 1
    lines = program_text.splitlines()
    program = []
    for line in lines:
        words = line.split()
        opcode = words[0]
        instruction = {}
        if opcode == "hlf":
            instruction["r"] = decode(words[1])
        elif opcode == "tpl":
            instruction["r"] = decode(words[1])
        elif opcode == "inc":
            instruction["r"] = decode(words[1])
        elif opcode == "jmp":
            instruction["offset"] = int(words[1])
        elif opcode == "jie":
            instruction["r"] = decode(words[1][:-1])
            instruction["offset"] = int(words[2])
        elif opcode == "jio":
            instruction["r"] = decode(words[1][:-1])
            instruction["offset"] = int(words[2])
        else:
            print("Wrong opcode!")
            exit()
        instruction["name"] = opcode
        program.append(instruction)
    return program


def init_state():
    return {"pc": 0, "a": 0, "b": 0}


def run_program(program, initial_state = init_state()):
    #print(initial_state)
    regs = [initial_state["a"], initial_state["b"]]
    pc = initial_state["pc"]
    while pc < len(program):
        instruction = program[pc]
        name = instruction["name"]
        if name == "hlf":
            r = instruction["r"]
            regs[r] = regs[r] // 2
        elif name == "tpl":
            r = instruction["r"]
            regs[r] = regs[r] * 3
        elif name == "inc":
            r = instruction["r"]
            regs[r] += 1
        elif name == "jmp":
            offset = instruction["offset"]
            pc = pc + offset-1
        elif name == "jie":
            r = instruction["r"]
            offset = instruction["offset"]
            if regs[r] % 2 == 0:
                pc = pc + offset-1
        elif name == "jio":
            r = instruction["r"]
            offset = instruction["offset"]
            if regs[r] == 1:
                pc = pc + offset-1
        else:
            print("Wrong opcode!")
            exit()
        pc += 1
        #print(instruction)
        #print("  =>", pc, regs)
    return {"pc": pc, "a": regs[0], "b": regs[1]}


def main():
    with open("day23.input") as f:
        input_program = f.read()
    program = read_program(input_program)
    end_state = run_program(program)
    print("Exercise 1: ", end_state["b"])
    other_state = init_state()
    other_state["a"] = 1
    end_state_v2 = run_program(program, other_state)
    print("Exercise 2: ", end_state_v2["b"])


def test():
    test_program = "inc a\njio a, +2\ntpl a\ninc a"
    program = read_program(test_program)
    state = run_program(program)
    assert state["a"] == 2

if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "test":
        test()
    else:
        main()
