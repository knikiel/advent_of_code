#!/usr/bin/python3
# encoding = utf-8

# adventofcode/day/18
# Day 18: Like a GIF For your Yard
# For a new winning lighting configuration, you have to resort to lighting animations.
# 1) How many lights are on after 100 steps?

import copy
import sys

# output has dummy columns and at the beginning and the end
def read_configuration(configuration_text):
    lines = configuration_text.splitlines()
    lights = [[]]
    for char in range(len(lines[0])+2):
        lights[-1].append(False)
    for line in lines:
        lights.append([False])
        for char in line:
            is_on = (char == '#')
            lights[-1].append(is_on)
        lights[-1].append(False)
    lights.append([])
    for char in range(len(lines[0])+2):
        lights[-1].append(False)
    configuration = {
            "num_rows": len(lights)-2,
            "num_cols": len(lights[0])-2,
            "lights": lights
        }
    return configuration


def configuration_after_n_steps(starting_conf, total_steps):
    prev_conf = copy.deepcopy(starting_conf)
    for num_steps in range(total_steps):
        next_conf = copy.deepcopy(prev_conf)
        for i in range(1, starting_conf["num_rows"]+1):
            for j in range(1, starting_conf["num_cols"]+1):
                lights = prev_conf["lights"]
                this_light = lights[i][j]
                neighbors = [lights[i-1][j-1], lights[i-1][j], lights[i-1][j+1],
                        lights[i][j-1], lights[i][j+1],
                        lights[i+1][j-1], lights[i+1][j], lights[i+1][j+1]]
                sum_lights = sum(neighbors)
                if this_light and (sum_lights == 2 or sum_lights == 3):
                    next_conf["lights"][i][j] = True  # stay on
                elif (not this_light) and (sum_lights == 3):
                    next_conf["lights"][i][j] = True  # turn on
                else:
                    next_conf["lights"][i][j] = False
        prev_conf = next_conf
    return next_conf


def configuration_with_fixed_corners(starting_conf, total_steps):
    num_rows = starting_conf["num_rows"]
    num_cols = starting_conf["num_cols"]
    prev_conf = copy.deepcopy(starting_conf)
    for i in range(1, num_rows+1):
        for j in range(1, num_cols+1):
            if (i == 1 or i == num_rows) and (j == 1 or j == num_cols):
                prev_conf["lights"][i][j] = True  # stay on
    next_conf = copy.deepcopy(prev_conf)
    for num_steps in range(total_steps):
        for i in range(1, num_rows+1):
            for j in range(1, num_cols+1):
                if (i == 1 or i == num_rows) and (j == 1 or j == num_cols):
                    next_conf["lights"][i][j] = True  # fixed
                    continue
                lights = prev_conf["lights"]
                this_light = lights[i][j]
                neighbors = [lights[i-1][j-1], lights[i-1][j], lights[i-1][j+1],
                        lights[i][j-1], lights[i][j+1],
                        lights[i+1][j-1], lights[i+1][j], lights[i+1][j+1]]
                sum_lights = sum(neighbors)
                if this_light and (sum_lights == 2 or sum_lights == 3):
                    next_conf["lights"][i][j] = True  # stay on
                elif (not this_light) and (sum_lights == 3):
                    next_conf["lights"][i][j] = True  # turn on
                else:
                    next_conf["lights"][i][j] = False
        prev_conf = next_conf
        next_conf = copy.deepcopy(prev_conf)
    return next_conf


def count_lights(conf):
    return sum(map(sum, conf["lights"]))


def write_configuration(conf):
    text = ""
    for i in range(1, conf["num_rows"]+1):
        for j in range(1, conf["num_cols"]+1):
            char = '#' if conf["lights"][i][j] else '.'
            text += char
        text += '\n'
    return text
    
def test():
    print("Testing:")
    starting_conf = ".#.#.#\n...##.\n#....#\n..#...\n#.#..#\n####..\n"
    print(starting_conf)
    conf = read_configuration(starting_conf)
    next_conf = configuration_after_n_steps(conf, 1)
    next_text = write_configuration(next_conf)
    print(next_text)
    conf_fixed = configuration_with_fixed_corners(conf, 0)
    print(write_configuration(conf_fixed))
    next_conf_fixed = configuration_with_fixed_corners(conf, 1)
    print(write_configuration(next_conf_fixed))
    assert write_configuration(read_configuration(starting_conf)) == starting_conf
    print("Testing finished!")

def main():
    with open("day18.input") as f:
        input_configuration = f.read()
    starting_conf = read_configuration(input_configuration)
    ending_conf = configuration_after_n_steps(starting_conf, 100)
    num_lights = count_lights(ending_conf)
    print("Exercise 1: " + str(num_lights))
    ending_conf_fixed = configuration_with_fixed_corners(starting_conf, 100)
    num_lights_fixed = count_lights(ending_conf_fixed)
    print("Exercise 2: " + str(num_lights_fixed))


if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "test":
        test()
    else:
        main()
