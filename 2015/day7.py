#!/usr/bin/python3
# encoding = utf-8

# adventofcode.com/day/7
# Day 7: Some Assembly Required
# Santa brought Bobby some logic circuits but he is still too young.
# 1) What is the output value of the wire 'a'?
# 2) What is the output value of 'a' if 'b' is set differently?


with open("day7.input") as f:
    input_circuit = f.read()


def compute_output(gate, state):
    words = gate.split()
    if "AND" in words:  # a AND b -> c
        a = words[0]
        b = words[2]
        c = words[4]
        if a.isdigit():
            a = int(a)
        else:
            if not a in state:
                return False
            a = state[a]
        if b.isdigit():
            b = int(b)
        else:
            if not b in state:
                return False
            b = state[b]
        state[c] = a & b
    elif "OR" in words:  # a OR b -> c
        a = words[0]
        b = words[2]
        c = words[4]
        if not (a in state and b in state):
            return False
        state[c] = state[a] | state[b]
    elif "LSHIFT" in words:  # a LSHIFT i -> b
        a = words[0]
        i = int(words[2])
        b = words[4]
        if not a in state:
            return False
        state[b] = (state[a] << i) & 0xFFFF
    elif "RSHIFT" in words:  # a RSHIFT i -> b
        a = words[0]
        i = int(words[2])
        b = words[4]
        if not a in state:
            return False
        state[b] = state[a] >> i
    elif "NOT" in words:  # NOT a -> b
        a = words[1]
        b = words[3]
        if not a in state:
            return False
        unary_complement = 0xFFFF ^ state[a]
        state[b] = unary_complement
    else:  # i -> a
        i = words[0]
        if i.isdigit():
            i = int(i)
        else:
            if not i in state:
                return False
            i = state[i]
        a = words[2]
        state[a] = i
    return True


def simulate_circuit(circuit):
    state = dict()
    gates = circuit.splitlines()
    while len(gates) > 0:
        next_gates = list()
        for gate in gates:
            if not compute_output(gate, state):
                next_gates.append(gate)
        if len(gates) == len(next_gates):
            return state
        gates = next_gates
    return state


def rewire_b(circuit, new_value):
    gates = circuit.splitlines()
    for i, gate in enumerate(gates):
        words = gate.split()
        if words[-1] == 'b':
            del gates[i]
            break
    gates.append(str(new_value) + " -> b")
    rewired_circuit = '\n'.join(gates)
    return rewired_circuit

def test():
    test_circuit = \
        "123 -> x\n" \
        "x AND y -> d\n" \
        "x OR y -> e\n" \
        "x LSHIFT 2 -> f\n" \
        "456 -> y\n" \
        "y RSHIFT 2 -> g\n" \
        "NOT x -> hi\n" \
        "NOT y -> i"
    test_result = simulate_circuit(test_circuit)
    assert test_result['x'] == 123
    assert test_result['y'] == 456
    assert test_result['d'] == 72
    assert test_result['e'] == 507
    assert test_result['f'] == 492
    assert test_result['g'] == 114
    assert test_result['hi'] == 65412
    assert test_result['i'] == 65079


if __name__ == "__main__":
    test()
    output = simulate_circuit(input_circuit)
    print("Exercise 1: " + str(output["a"]))
    rewired_circuit = rewire_b(input_circuit, output["a"])
    new_output = simulate_circuit(rewired_circuit)
    print("Exercise 2: " + str(new_output["a"]))
