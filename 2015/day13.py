# adventofcode.com/day/13
# Day 13: Knights of the Dinner Table
# In the last christmases the seating positions of the people in your family were not quite right.
# 1) What are the best seating positions for this year?

import itertools
import re
import sys


def parse_matrix(text):
    regex = re.compile(r"(\w+) would (gain|lose) (\d+) happiness units by sitting next to (\w+).")
    matrix = {}
    for line in input_matrix.splitlines():
        match = regex.match(line)
        who = match.group(1)
        whom = match.group(4)
        diff = int(match.group(3))
        if match.group(2) == "lose":
            diff *= -1
        if not who in matrix:
            matrix[who] = {}
        matrix[who][whom] = diff 
    return matrix

def max_happiness(matrix):
    # find the positions around the table
    max_happiness = -sys.maxsize
    for perm in itertools.permutations(list(matrix)):
        sum_happiness = 0
        for i in range(len(perm)):
            who = perm[i]
            whom = perm[(i+1) % len(perm)]
            sum_happiness += matrix[who][whom] + matrix[whom][who]
        max_happiness = max(sum_happiness, max_happiness)
    return max_happiness


def add_me(old_matrix):
    other_people = list(old_matrix.keys())
    new_matrix = old_matrix.copy()
    new_matrix["me"] = {}
    for person in other_people:
        new_matrix["me"][person] = 0
        new_matrix[person]["me"] = 0
    return new_matrix
    

if __name__ == "__main__":
    with open("day13.input") as f:
        input_matrix = f.read()
    matrix = parse_matrix(input_matrix)
    happiness = max_happiness(matrix)
    print("Exercise 1: " + str(happiness))
    mod_matrix = add_me(matrix)
    corrected_happiness = max_happiness(mod_matrix)
    print("Exercise 2: " + str(corrected_happiness))

