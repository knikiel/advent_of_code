#!/usr/bin/python3
# encoding = utf-8

# adventofcode.com/day/15
# Day 15: Science for Hungry People
# It's time to make some cookies, and you cans still add some ingredients.
# 1) What is the best cookie you can make (withoud regard to calories)?
# 2) What is the best cookie with exactly 500 cal?


import itertools as it
import re


def read_ingredients(ingredients_text):
    regex = re.compile(r"(\w+): capacity (-?\d+), durability (-?\d+), flavor (-?\d+), texture (-?\d+), calories (-?\d+)")
    ingredients = []
    for line in ingredients_text.splitlines():
        match = regex.match(line)
        ingredients.append({
                "capacity":int(match.group(2)),
                "durability":int(match.group(3)),
                "flavor":int(match.group(4)),
                "texture":int(match.group(5)),
                "calories":int(match.group(6)),
                })
    return ingredients


class distribute_on:
    def __init__(self, what, on):
        self.__what = what
        self.__iter = it.product(*([[i for i in range(what+1)]] * on))
    
    def __iter__(self):
        return self

    def __next__(self):
        while True:
            next_distr = self.__iter.__next__()
            if sum(next_distr) == self.__what:
                return next_distr


def best_score(ingredients):
    num_teaspoons = 100
    num_calories = 500
    max_score = 0
    max_with_calories = 0
    for distribution in distribute_on(num_teaspoons, len(ingredients)):
        sum_capacity = 0
        sum_durability = 0
        sum_flavor = 0
        sum_texture = 0
        sum_calories = 0
        for i, ingr in enumerate(ingredients):
            sum_capacity += ingr["capacity"] * distribution[i]
            sum_durability += ingr["durability"] * distribution[i]
            sum_flavor += ingr["flavor"] * distribution[i]
            sum_texture += ingr["texture"] * distribution[i]
            sum_calories += ingr["calories"] * distribution[i]
        score = max(sum_capacity,0) * max(sum_durability,0) \
                * max(sum_flavor,0) * max(sum_texture,0)
        max_score = max(score, max_score)
        if sum_calories == num_calories:
            max_with_calories = max(score, max_with_calories)
    return max_score, max_with_calories


def main():
    with open("day15.input") as f:
        input_ingredients = f.read()
    ingredients = read_ingredients(input_ingredients)
    score, score_calories = best_score(ingredients)
    print("Exercise 1: " + str(score))
    print("Exercise 2: " + str(score_calories))


if __name__ == "__main__":
    main()
