package de.knikiel.aoc2007.days

object Day1 : Day {
    fun computeChecksum(numbers: List<Int>) : Int {
        var sum = 0
        for (i in 0 until numbers.size-1) {
            if (numbers[i] == numbers[i+1]) {
                sum += numbers[i]
            }
        }
        if (numbers.last() == numbers.first()) {
            sum += numbers.last()
        }
        return sum
    }

    fun computeDifficultChecksum(numbers: List<Int>) : Int {
        val step = numbers.size / 2

        var sum = 0
        for (i in 0 until numbers.size) {
            if (numbers[i] == numbers[(i+step) % numbers.size]) {
                sum += numbers[i]
            }
        }
        return sum
    }

    override fun computeSolution(input: List<String>) : String {
        val numbers = ArrayList<Int>()
        for (line in input) {
            for (digit in line) {
                numbers.add(digit.toString().toInt())
            }
        }

        val output = StringBuilder()
        var checksum = computeChecksum(numbers)
        output.append("Simple checksum: $checksum")
        output.append("\n\n")

        checksum = computeDifficultChecksum(numbers)
        output.append("Difficult checksum: $checksum")
        return output.toString()
    }
}

