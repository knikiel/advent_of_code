package de.knikiel.aoc2007.days

interface Day {
    fun computeSolution(input: List<String>) : String
}