package de.knikiel.aoc2007.days

object Day13 : Day {
    data class Scanner(val layer: Int, val range: Int)

    fun computeSeverity(scanners: Array<Scanner>, delay: Int = 0) : Int {
        var totalSeverity = 0
        for (scanner in scanners) {
            if ((scanner.layer + delay) % (2 * (scanner.range-1)) == 0) {
                val localSeverity = scanner.layer * scanner.range
                totalSeverity += localSeverity
            }
        }
        return totalSeverity
    }

    fun isDiscovered(scanners: Array<Scanner>, delay: Int) : Boolean {
        var totalSeverity = 0
        for (scanner in scanners) {
            if ((scanner.layer + delay) % (2 * (scanner.range-1)) == 0) {
                return true
            }
        }
        return false
    }

    fun computeTotalDelay(scanners: Array<Scanner>) : Int {
        var delay = 0
        while (isDiscovered(scanners, delay)) {
            delay += 1
        }
        return delay
    }

    override fun computeSolution(input: List<String>): String {
        val scanners = input.map {
            val layerAndRange = it.split(":")
            Scanner(layerAndRange[0].trim().toInt(), layerAndRange[1].trim().toInt())
        }.toTypedArray()

        val severity = computeSeverity(scanners)
        val delay = computeTotalDelay(scanners)

        val output = StringBuilder()
        output.append("Severity of scanner configuration: $severity\n\n")
        output.append("Minimal delay for safe passage: $delay\n")
        return output.toString()
    }
}