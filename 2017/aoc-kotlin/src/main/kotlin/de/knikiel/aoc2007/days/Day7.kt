package de.knikiel.aoc2007.days

object Day7 : Day {

    data class Rule(val bottom: String, val weight: Int, val tops: Array<String>) {
        override fun equals(other: Any?): Boolean {
            if (other == null || !(other is Rule)) {
                return false
            }
            return bottom == other.bottom && weight == other.weight
                && tops.contentEquals(other.tops)
        }
    }

    fun readRules(lines: List<String>) : Array<Rule> {
        val rules = ArrayList<Rule>(lines.size)
        val regex = Regex("""(\w+) \((\d+)\)( -> (\w+, )*(\w+))?""")
        for (line in lines) {
            val match = regex.find(line)
            if (match == null) {
                continue
            }
            val groups = match.groupValues
            val bottom = groups[1]
            val weight = groups[2].toInt()
            val tops =
                    groups[3].removePrefix(" ->")
                            .split(", ")
                            .map { s -> s.trim() }
                            .filter { s -> s != "" }
                            .toTypedArray()
            //println("$bottom [$weight]")
            //println("  ${tops.joinToString(", ")}")
            rules.add(Rule(bottom, weight, tops))
        }
        return rules.toTypedArray()
    }

    data class Tree(val weight: Int, val children: List<Tree>) {

        companion object {
            fun create(rules: Array<Rule>) : Tree {
                val bottomRule = searchBottomRule(rules)
                return Tree(bottomRule!!.weight, create(rules, bottomRule!!.tops))
            }

            private fun create(rules: Array<Rule>, tops: Array<String>) : ArrayList<Tree> {
                val children = ArrayList<Tree>(tops.size)
                for (top in tops) {
                    for (rule in rules) {
                        if (rule.bottom == top) {
                            val tree = Tree(rule.weight, create(rules, rule.tops))
                            children.add(tree)
                        }
                    }
                }
                return children
            }
        }
    }

    fun searchBottomRule(rules: Array<Rule>) : Rule? {
        val allTops = rules.toMutableList()
                .flatMap({ rule -> rule.tops.toHashSet() })
        for (rule in rules) {
            if (!(rule.bottom in allTops)) {
                return rule
            }
        }
        return null
    }

    fun searchBottom(rules: Array<Rule>) : String {
        val bottomRule = searchBottomRule(rules)
        return bottomRule?.bottom ?: "ERROR: NOTHING FOUND"
    }

    private fun sumTreeWeights(tree: Tree) : Int {
        val sumChildrenWeights = tree.children.map { sumTreeWeights(it) }.sum()
        return tree.weight + sumChildrenWeights
    }

    fun determineCorrectWeight(tree: Tree) : Int {
        for (child in tree.children) {
            val childWeight = determineCorrectWeight(child)
            if (childWeight > 0) {
                return childWeight
            }
        }

        // test if children are unbalanced
        val childrenWeights = tree.children.map { sumTreeWeights(it) }
        val weightNumbers = HashMap<Int, Int>(childrenWeights.size)
        for (childWeight in childrenWeights) {
            val count = weightNumbers[childWeight] ?: 0
            weightNumbers[childWeight] = count + 1
        }
        if (weightNumbers.size > 1) {
            val wrongWeight = weightNumbers.minBy { it.value }!!.key
            val rightWeight = weightNumbers.maxBy { it.value }!!.key
            for ((i, child) in tree.children.withIndex()) {
                val childWeight = childrenWeights[i]
                if (childWeight == wrongWeight) {
                    return child.weight + (rightWeight - wrongWeight)
                }
            }
        }

        // nothing wrong in this subtree
        return 0
    }

    override fun computeSolution(input: List<String>): String {
        val rules = readRules(input)

        val bottom = searchBottom(rules)
        val correctWeight = determineCorrectWeight(Tree.create(rules))

        val output = StringBuilder()
        output.append("bottom of tower: $bottom\n\n")
        output.append("corrected weight of some child: $correctWeight\n")

        return output.toString()
    }
}