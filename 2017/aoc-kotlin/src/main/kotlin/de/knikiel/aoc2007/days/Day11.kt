package de.knikiel.aoc2007.days

import kotlin.math.abs

object Day11 : Day {
    //NOTE: see https://www.redblobgames.com/grids/hexagons for a nice overview about hex grids

    data class Axial(val x: Int, val y: Int, val z: Int) {
        fun getDistanceToCenter() : Int {
            return (abs(x) + abs(y) + abs(z)) / 2
        }

        operator fun plus(other: Axial) : Axial {
            return Axial(x + other.x, y + other.y, z + other.z)
        }
    }

    enum class Direction {
        NW, N, NE, SW, S, SE;

        fun move() : Axial {
            return when (this) {
                NW -> Axial(0, 1, -1)
                N -> Axial(1, 0, -1)
                NE -> Axial(1, -1, 0)
                SW -> Axial(-1, 1, 0)
                S -> Axial(-1, 0, 1)
                SE -> Axial(0, -1, 1)
            }
        }
    }

    class DirectionFormatException(string: String) : RuntimeException("""'$string' is not a direction.""")

    fun String.toDirection() : Direction {
        return when (this) {
            "nw" -> Direction.NW
            "n" -> Direction.N
            "ne" -> Direction.NE
            "sw" -> Direction.SW
            "s" -> Direction.S
            "se" -> Direction.SE
            else -> throw DirectionFormatException(this)
        }
    }

    fun followDirections(directions: List<Direction>) : Axial {
        var position = Axial(0, 0, 0)
        for (dir in directions) {
            position += dir.move()
        }
        return position
    }

    fun computeMaxDistance(directions: List<Direction>) : Int {
        var position = Axial(0, 0, 0)
        var maxDistance = 0
        for (dir in directions) {
            position += dir.move()
            if (position.getDistanceToCenter() > maxDistance) {
                maxDistance = position.getDistanceToCenter()
            }
        }
        return maxDistance
    }

    override fun computeSolution(input: List<String>): String {
        val directions = input[0].split(",").map { it.toDirection() }

        val endPos = followDirections(directions)
        val distance = endPos.getDistanceToCenter()

        val maxDistance = computeMaxDistance(directions)

        val output = StringBuilder()
        output.append("Distance to center: $distance\n\n")
        output.append("Maximal distance to center: $maxDistance\n")
        return output.toString()
    }
}