package de.knikiel.aoc2007.days

object Day4 : Day {
    fun countValidPhrases(phraseLines : List<List<String>>) : Int {
        var numCorrectLines = 0
        val bag = mutableSetOf<String>()
        for (phrases in phraseLines) {
            var hasDoublePhrase = false
            for (phrase in phrases) {
                if (bag.contains(phrase)) {
                    hasDoublePhrase = true
                    break
                }
                bag.add(phrase)
            }
            if (!hasDoublePhrase) {
                numCorrectLines += 1
            }
            bag.clear()
        }
        return numCorrectLines
    }

    fun countValidAnagramPhrases(phraseLines: List<List<String>>) : Int {
        fun computeCanonical(str: String) : String {
            val chars = CharArray(str.length)
            for ((i, ch) in str.withIndex()) {
                chars[i] = ch
            }
            chars.sort()
            return chars.joinToString("")
        }

        val processedPhraseLines = ArrayList<ArrayList<String>>()
        for (phraseLine in phraseLines) {
            processedPhraseLines.add(ArrayList<String>())
            for (phrase in phraseLine) {
                processedPhraseLines.last().add(computeCanonical(phrase))
            }
        }

        var numCorrectLines = 0
        val bag = mutableSetOf<String>()
        for (phrases in processedPhraseLines) {
            var hasDoublePhrase = false
            for (phrase in phrases) {
                if (bag.contains(phrase)) {
                    hasDoublePhrase = true
                    break
                }
                bag.add(phrase)
            }
            if (!hasDoublePhrase) {
                numCorrectLines += 1
            }
            bag.clear()
        }
        return numCorrectLines
    }

    override fun computeSolution(input: List<String>) : String {
        val phraseLines = ArrayList<ArrayList<String>>()
        for (line in input) {
            phraseLines.add(ArrayList<String>())
            phraseLines.last().addAll(line.split(" "))
        }

        val output = StringBuilder()
        val numCorrectLines = countValidPhrases(phraseLines)
        output.append("Number of correct phrases: $numCorrectLines")
        output.append("\n\n")

        val numCorrectAnagramLines = countValidAnagramPhrases(phraseLines)
        output.append("Number of correct anagram phrases: $numCorrectAnagramLines")

        return output.toString()
    }
}