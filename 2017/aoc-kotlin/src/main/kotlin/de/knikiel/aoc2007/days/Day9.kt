package de.knikiel.aoc2007.days

object Day9 : Day {
    sealed class GroupTree {
        data class Group(val children: ArrayList<GroupTree>) : GroupTree()
        data class Garbage(val garbage: String) : GroupTree()
    }

    data class TreeEndIndex(val tree: GroupTree, val idx: Int)

    fun parse(string: String) : GroupTree {
        val (tree, endIdx) = parseGroup(string, 0)
        return tree
    }

    fun parseGroup(string: String, idx: Int) : TreeEndIndex {
        var currIdx = idx + 1
        val children = arrayListOf<GroupTree>()
        while (true) {
            val char = string[currIdx]
            if (char == ',') {
                currIdx += 1
            }
            else if (char == '{') {
                val (child, endIdx) = parseGroup(string, currIdx)
                children.add(child)
                currIdx = endIdx + 1
            }
            else if (char == '}') {
                return TreeEndIndex(GroupTree.Group(children), currIdx)
            }
            else if (char == '<') {
                val (child, endIdx) = parseGarbage(string, currIdx)
                children.add(child)
                currIdx = endIdx + 1
            }
            else {
                break
            }
        }
        return TreeEndIndex(GroupTree.Group(arrayListOf()), -1)
    }

    fun parseGarbage(string: String, idx: Int) : TreeEndIndex {
        var currIdx = idx + 1
        while (true) {
            val char = string[currIdx]
            if (char == '>') {
                return TreeEndIndex(GroupTree.Garbage(string.substring(idx, currIdx+1)),
                        currIdx)
            }
            else if (char == '!') {
                currIdx += 2
            }
            else {
                currIdx += 1
            }
        }
        return TreeEndIndex(GroupTree.Garbage(""), -1)
    }

    fun computeScore(tree: GroupTree, localScore: Int = 1) : Int {
        return when (tree) {
            is GroupTree.Group -> {
                val childrenSum = tree.children.map {
                        computeScore(it, localScore + 1)
                    }.sum()
                localScore + childrenSum
            }
            is GroupTree.Garbage -> {
                0
            }
        }
    }

    fun countGarbage(tree: GroupTree) : Int {
        return when (tree) {
            is GroupTree.Group -> {
                tree.children.map {
                    countGarbage(it)
                }.sum()
            }
            is GroupTree.Garbage -> {
                val string = tree.garbage
                var length = 0
                var idx = 1
                while (idx < string.length - 1) {
                    if (string[idx] != '!') {
                        length += 1
                        idx += 1
                    }
                    else {
                        idx += 2
                    }
                }
                length
            }
        }
    }

    override fun computeSolution(input: List<String>): String {
        val tree = parse(input[0])

        val score = computeScore(tree)
        val count = countGarbage(tree)

        val output = StringBuilder()
        output.append("Score of tree: $score\n\n")
        output.append("Garbage count of tree: $count\n")
        return output.toString()
    }
}