package de.knikiel.aoc2007.days

typealias NodeId = Int

object Day12 : Day {

    class Graph(val numNodes: Int) {

        private val adjacencies = Array<ArrayList<NodeId>>(numNodes) {ArrayList()}

        companion object {
            fun parse(adjacencyLines: List<String>) : Graph {
                val graph = Graph(adjacencyLines.count())
                for (line in adjacencyLines) {
                    var parts = line.split(" ")
                    val node = parts[0].toInt()
                    val neighbors = parts.drop(2).map {it.removeSuffix(",").toInt()}
                    for (neighbor in neighbors) {
                        if (neighbor >= node) {
                            // edge between nodes not in graph yet
                            graph.addUndirectedEdge(node, neighbor)
                        }
                    }
                }
                return graph
            }
        }

        fun addUndirectedEdge(u: NodeId, v: NodeId) {
            adjacencies[u].add(v)
            adjacencies[v].add(u)
        }

        fun neighbors(v: NodeId) : List<NodeId> {
            return adjacencies[v]
        }
    }

    /// Search for the connected component of source.
    /// Implements DFS.
    fun computeConnectedComponent(graph: Graph, source: NodeId) : List<NodeId> {
        val componentStack = ArrayList<NodeId>()
        val visited = Array<Boolean>(graph.numNodes) { false }
        componentStack.add(source)
        visited[source] = true

        val connectedComponent = ArrayList<NodeId>()
        while (componentStack.isNotEmpty()) {
            val nextNode = componentStack.last()
            componentStack.removeAt(componentStack.size-1)

            connectedComponent.add(nextNode)

            for (neighbor in graph.neighbors(nextNode)) {
                if (!visited[neighbor]) {
                    componentStack.add(neighbor)
                    visited[neighbor] = true
                }
            }
        }

        return connectedComponent
    }

    fun computeNumberConnectedComponents(graph: Graph) : Int {

        val visited = Array<Boolean>(graph.numNodes) { false }
        var numConnectedComponents = 0
        for (source in 0 until graph.numNodes)
        {
            if (visited[source] == true) {
                continue
            }

            val componentStack = ArrayList<NodeId>()

            componentStack.add(source)
            visited[source] = true

            val connectedComponent = ArrayList<NodeId>()
            while (componentStack.isNotEmpty()) {
                val nextNode = componentStack.last()
                componentStack.removeAt(componentStack.size - 1)

                connectedComponent.add(nextNode)

                for (neighbor in graph.neighbors(nextNode)) {
                    if (!visited[neighbor]) {
                        componentStack.add(neighbor)
                        visited[neighbor] = true
                    }
                }
            }

            numConnectedComponents += 1
        }

        return numConnectedComponents
    }

    override fun computeSolution(input: List<String>): String {
        val graph = Graph.parse(input)
        val connectedComponent = computeConnectedComponent(graph, 0)
        val sizeCC = connectedComponent.size
        val numCCs = computeNumberConnectedComponents(graph)

        val output = StringBuilder()
        output.append("Size of connected component of 0: $sizeCC\n\n")
        output.append("Number of connected components: $numCCs\n")
        return output.toString()
    }
}