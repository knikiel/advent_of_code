package de.knikiel.aoc2007.days

object Day8 : Day {
    enum class Operation {
        INCREMENT,
        DECREMENT
    }
    enum class Comparison {
        LESS,
        GREATER,
        EQUAL,
        LEQ,
        GEQ,
        NEQ
    }
    data class Instruction (
        val op: Operation,
        val dest: String,
        val times: Int,
        val comp: Comparison,
        val reg: String,
        val value: Int
    )

    val INSTRUCTION_REGEX = Regex("""(\w+) (inc|dec) (\-?\d+) if (\w+) (.*) (\-?\d+)""")

    fun parseProgram(input: List<String>) : Array<Instruction> {
        val instructions = Array<Instruction>(input.size) { idx ->
            val line = input[idx]
            val matches = INSTRUCTION_REGEX.matchEntire(line)!!.groupValues
            val op = when (matches[2]) {
                "inc" -> Operation.INCREMENT
                "dec" -> Operation.DECREMENT
                // unreachable
                else -> Operation.INCREMENT
            }
            val dest = matches[1]
            val times = matches[3].toInt()
            val comp = when (matches[5]) {
                "<" -> Comparison.LESS
                ">" -> Comparison.GREATER
                "==" -> Comparison.EQUAL
                "<=" -> Comparison.LEQ
                ">=" -> Comparison.GEQ
                "!=" -> Comparison.NEQ
                // unreachable
                else -> Comparison.EQUAL
            }
            val reg = matches[4]
            val value = matches[6].toInt()
            Instruction(op, dest, times, comp, reg, value)
        }
        return instructions
    }

    fun executeInstruction(registers: MutableMap<String, Int>, instruction: Instruction) : Int {
        val regValue = registers.getOrPut(instruction.reg, {0})
        val condition = when (instruction.comp) {
                Comparison.LESS -> regValue < instruction.value
                Comparison.GREATER -> regValue > instruction.value
                Comparison.EQUAL -> regValue == instruction.value
                Comparison.LEQ -> regValue <= instruction.value
                Comparison.GEQ -> regValue >= instruction.value
                Comparison.NEQ -> regValue != instruction.value
            }
        var destValue = registers[instruction.dest] ?: 0
        if (condition) {
             destValue = when (instruction.op) {
                    Operation.INCREMENT -> destValue + instruction.times
                    Operation.DECREMENT -> destValue - instruction.times
                }
        }
        registers[instruction.dest] = destValue
        return destValue
    }

    fun executeProgram(instructions: Array<Instruction>) : MutableMap<String, Int> {
        val registers = HashMap<String, Int>()
        for (instruction in instructions) {
            executeInstruction(registers, instruction)
        }
        return registers
    }

    fun keepMaxValue(instructions: Array<Instruction>) : Int {
        val registers = HashMap<String, Int>()
        var max = Int.MIN_VALUE
        for (instruction in instructions) {
            val value = executeInstruction(registers, instruction)
            if (value > max) {
                max = value
            }
        }
        return max
    }

    fun searchMaxValue(registers: MutableMap<String, Int>) : Int {
        return registers.maxBy { it.value }!!.value
    }

    override fun computeSolution(input: List<String>): String {
        val instructions = parseProgram(input)

        val registers = executeProgram(instructions)
        val max = searchMaxValue(registers)

        val maxInterim = keepMaxValue(instructions)

        val output = StringBuilder()
        output.append("max value in registers: $max\n\n")
        output.append("max value in computation: $maxInterim\n")
        return output.toString()
    }
}