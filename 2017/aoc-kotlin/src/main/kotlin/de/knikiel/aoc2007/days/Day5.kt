package de.knikiel.aoc2007.days

object Day5 : Day {
    fun searchExit(instructions : IntArray) : Int {
        var idxInstruction = 0
        var numSteps = 0
        while (idxInstruction in 0 until instructions.size) {
            val nextIdxInstruction = idxInstruction + instructions[idxInstruction]
            instructions[idxInstruction] += 1
            idxInstruction = nextIdxInstruction
            numSteps += 1
        }
        return numSteps
    }

    fun searchExitDifficult(instructions : IntArray) : Int {
        var idxInstruction = 0
        var numSteps = 0
        while (idxInstruction in 0 until instructions.size) {
            val instruction = instructions[idxInstruction]
            val nextIdxInstruction = idxInstruction + instruction
            if (instruction >= 3) {
                instructions[idxInstruction] -= 1
            }
            else {
                instructions[idxInstruction] += 1
            }
            idxInstruction = nextIdxInstruction
            numSteps += 1
        }
        return numSteps
    }


    override fun computeSolution(input: List<String>): String {
        val instructions = input.map { it.toInt() }.toIntArray()
        val copiedInstructions = instructions.copyOf()

        val numSteps = searchExit(instructions)
        val numStepsDifficult = searchExitDifficult(copiedInstructions)

        val output = StringBuilder()
        output.append("Number of steps: $numSteps\n\n")
        output.append("Number of steps (difficult): $numStepsDifficult\n")
        return output.toString()
    }
}