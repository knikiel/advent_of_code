package de.knikiel.aoc2007.days

object Day6 : Day {
    data class CycleInfo (val numberCycles: Int, val lengthLoop: Int)

    fun nextStep(memoryBlocks: IntArray){
        // search slot to redistribute
        var idxMax = -1
        var max = -1
        for ((i, count) in memoryBlocks.withIndex()) {
            if (count > max) {
                idxMax = i
                max = count
            }
        }

        // redistribute
        memoryBlocks[idxMax] = 0
        var numDistributing = max
        var idxDestination = (idxMax + 1) % memoryBlocks.size
        while (numDistributing > 0) {
            memoryBlocks[idxDestination] += 1
            numDistributing -= 1
            idxDestination = (idxDestination + 1) % memoryBlocks.size
        }
    }

    fun countCycles(memoryBlocks: IntArray) : CycleInfo {
        var numCycles = 0
        val oldConfigurations = ArrayList<IntArray>()
        while (true) {
            // add to known configurations
            oldConfigurations.add(memoryBlocks.copyOf())

            // compute new configuration
            nextStep(memoryBlocks)
            numCycles += 1

            // test if old configuration is reached
            for ((i, configuration) in oldConfigurations.withIndex()) {
                if (memoryBlocks.contentEquals(configuration)) {
                    return CycleInfo(numCycles,
                            numCycles - i)
                }
            }
        }
    }


    override fun computeSolution(input: List<String>): String {
        val splitInput = input[0].split("\t")
        val memoryBlocks = splitInput
                .map(String::toInt)
                .toIntArray()
        val cycleInfo = countCycles(memoryBlocks)

        val output = StringBuilder()
        output.append("Number of steps: ${cycleInfo.numberCycles}\n\n")
        output.append("Length of loop: ${cycleInfo.lengthLoop}\n")

        return output.toString()
    }
}