package de.knikiel.aoc2007.days

object Day2 : Day {
    fun computeChecksum(numberLists: List<List<Int>>): Int {
        var sum = 0
        for (numberList in numberLists) {
            var min = Int.MAX_VALUE;
            var max = Int.MIN_VALUE;
            for (number in numberList) {
                if (number > max) {
                    max = number
                }
                if (number < min) {
                    min = number
                }
            }
            sum += max - min
        }
        return sum
    }

    fun computeDifficultChecksum(numberLists: List<List<Int>>): Int {
        var sum = 0
        for (numbers in numberLists) {
            line@ for ((i, number) in numbers.withIndex()) {
                for (prevIdx in 0 until i) {
                    val prevNumber = numbers[prevIdx]
                    if (number % prevNumber == 0) {
                        sum += number / prevNumber
                        break@line;
                    } else if (prevNumber % number == 0) {
                        sum += prevNumber / number
                        break@line;
                    }
                }
            }
        }
        return sum
    }


    override fun computeSolution(input: List<String>) : String {
        val numberLists = ArrayList<List<Int>>()
        for (line in input) {
            //NOTE: workaround for 'split' not working
            var startPos = 0
            val numberStrings = ArrayList<String>()
            for ((i, c) in line.withIndex()) {
                if (c == ' ' || c == '\t') {
                    numberStrings += line.substring(startPos, i)
                    startPos = i + 1
                }
            }
            numberStrings += line.substring(startPos)

            val numbers = ArrayList<Int>()
            for (numberString in numberStrings) {
                numbers += numberString.toInt()
            }
            numberLists += numbers
        }

        val output = StringBuilder()
        var checksum = computeChecksum(numberLists)
        output.append("Simple checksum: $checksum")
        output.append("\n\n")

        checksum = computeDifficultChecksum(numberLists)
        output.append("Difficult checksum: $checksum")
        return output.toString()
    }
}