package de.knikiel.aoc2007.days

object Day10 : Day {

    fun reverseSubArray(array: IntArray, numElements: Int) {
        if (numElements in 0 .. 1) {
            return
        }
        val midIdx = (numElements - 1) / 2
        for (i in 0..midIdx) {
            val otherIdx = numElements - 1 - i
            val temp = array[i]
            array[i] = array[otherIdx]
            array[otherIdx] = temp
        }
    }

    fun shiftArray(srcArray: IntArray, destArray: IntArray, numElementsLeft: Int) {
        val lenArray = srcArray.size
        for (i in 0 until lenArray) {
            destArray[i] = srcArray[(i + numElementsLeft) % lenArray]
        }
    }

    fun knotArray(array: IntArray, lengths: IntArray) : Int {
        var workingArray = array
        var helpArray = IntArray(array.size)
        var skipLength = 0
        var oldStartIdx = 0
        for (length in lengths) {
            reverseSubArray(workingArray, length)
            shiftArray(workingArray, helpArray, length + skipLength)
            oldStartIdx = (100 * array.size + oldStartIdx - (length + skipLength)) % array.size
            val tempArray = workingArray
            workingArray = helpArray
            helpArray = tempArray
            skipLength += 1
        }
        for (i in 0 until array.size) {
            array[i] = workingArray[i]
        }
        return oldStartIdx
    }

    fun computeHash(knottedArray: IntArray, oldStartIdx: Int) : Int {
        return knottedArray[oldStartIdx] * knottedArray[(oldStartIdx + 1) % knottedArray.size]
    }

    fun parseAsciiCharacters(string: String) : IntArray {
        return string.map { it.toInt() }.toIntArray()
    }

    fun computeFinalLenghts(asciiLengths: IntArray) : IntArray {
        val finalLengths = IntArray(64 * (asciiLengths.size + 5))
        for (i in 0 until 64) {
            val startIdx = i * (asciiLengths.size + 5)
            for ((offset, length) in asciiLengths.withIndex()) {
                finalLengths[startIdx + offset] = length
            }
            finalLengths[startIdx + asciiLengths.size] = 17
            finalLengths[startIdx + asciiLengths.size + 1] = 31
            finalLengths[startIdx + asciiLengths.size + 2] = 73
            finalLengths[startIdx + asciiLengths.size + 3] = 47
            finalLengths[startIdx + asciiLengths.size + 4] = 23
        }
        return finalLengths
    }

    fun computeDenseAsciiHash(sparseHash: IntArray) : String {
        val denseHash = IntArray(sparseHash.size / 16)
        for (i in 0 until denseHash.size) {
            val windowIdx = i * 16
            var xorValue = 0
            for (j in 0 until 16) {
                xorValue = xorValue.xor(sparseHash[windowIdx + j])
            }
            denseHash[i] = xorValue
        }

        val denseAsciiHash = denseHash.map {
            (when (it / 16) {
                0 -> "0"
                1 -> "1"
                2 -> "2"
                3 -> "3"
                4 -> "4"
                5 -> "5"
                6 -> "6"
                7 -> "7"
                8 -> "8"
                9 -> "9"
                10 -> "a"
                11 -> "b"
                12 -> "c"
                13 -> "d"
                14 -> "e"
                15 -> "f"
                else -> "X"
            }).plus(when (it % 16) {
                0 -> "0"
                1 -> "1"
                2 -> "2"
                3 -> "3"
                4 -> "4"
                5 -> "5"
                6 -> "6"
                7 -> "7"
                8 -> "8"
                9 -> "9"
                10 -> "a"
                11 -> "b"
                12 -> "c"
                13 -> "d"
                14 -> "e"
                15 -> "f"
                else -> "X"
            })
        }.joinToString("")
        return denseAsciiHash
    }

    override fun computeSolution(input: List<String>): String {
        // exercise 1
        val array = IntArray(256) { i -> i }
        val lengths = input[0].split(",").map { it.toInt() }.toIntArray()

        val oldStartIdx = knotArray(array, lengths)
        val hashValue = computeHash(array, oldStartIdx)

        // exercise 2
        val otherArray = IntArray(256) { i -> i }
        val asciiLengths = parseAsciiCharacters(input[0])
        val finalLengths = computeFinalLenghts(asciiLengths)

        val oldStartIdx2 = knotArray(otherArray, finalLengths)
        val arrayCopy = otherArray.copyOf()
        shiftArray(arrayCopy, otherArray, oldStartIdx2)
        val denseAsciiHash = computeDenseAsciiHash(otherArray)

        val output = StringBuilder()
        output.append("Hash value: $hashValue\n\n")
        output.append("Dense Hash value: $denseAsciiHash\n")
        return output.toString()
    }
}