package de.knikiel.aoc2007.days

import kotlin.math.absoluteValue

object Day3 : Day {
    fun computeMemorySteps(pos: Int) : Int {
        if (pos == 1) {
            return 0
        }

        var low = 1
        var high = 1
        var squareIdx = 0
        var squareLength = 1
        while (true) {
            if (pos in low..high) {
                break
            }
            squareIdx += 1
            low = high + 1
            if (squareIdx == 1) {
                high = 9
            }
            else {
                high = high + squareLength + 8
            }
            squareLength = high - low + 1
        }

        // compute number of steps to fetch
        val firstSquareMid = low + squareIdx - 1
        var minDistanceToSquareMid = Int.MAX_VALUE
        for (i in 0..3) {
            val squareMid = firstSquareMid + squareLength / 4 * i
            //val distance = Math.abs(squareMid - pos).
            val distance = (squareMid - pos).absoluteValue
            if (distance < minDistanceToSquareMid) {
                minDistanceToSquareMid = distance
            }
        }
        val distance = squareIdx + minDistanceToSquareMid
        return distance
    }


    fun computeFirstLargerPosition(value: Int) : Int {
        if (value == 1) {
            return 1
        }

        val values = ArrayList<Int>()
        // dummy value
        values.add(-1)

        // value of mid to elements of first ring
        values.add(1)
        for (idx in 2 .. 9) {
            values.add(1)
        }

        var low = 2
        var high = 9
        var squareIdx = 1
        var squareLength = high - low + 1
        while (true) {
            val nextLow = high + 1
            val nextHigh = high + squareLength + 8
            val nextSquareLength = nextHigh - nextLow + 1

            // initialize next ring values
            for (pos in nextLow..nextHigh) {
                values.add(0)
            }

            var posNextRing = high + 2
            var stepsToCorner = squareLength / 4
            for (pos in low..high) {
                stepsToCorner -= 1
                val currValue = values[pos]
                if (currValue > value) {
                    return currValue
                }
                if (stepsToCorner == 0) {
                    // in any corner
                    posNextRing += 1
                    values[posNextRing - 2] += currValue
                    values[posNextRing - 1] += currValue
                    values[posNextRing] += currValue
                    if (pos != high) {
                        // not in last corner of ring
                        values[posNextRing + 1] += currValue
                        values[posNextRing + 2] += currValue
                        values[pos+1] += currValue
                    }
                    else {
                        // in last corner of ring
                        values[nextLow] += currValue
                        values[nextLow + 1] += currValue
                    }
                    posNextRing += 2
                    stepsToCorner = squareLength / 4
                }
                else {
                    // in straight segment
                    values[pos+1] += currValue
                    values[posNextRing - 1] += currValue
                    values[posNextRing] += currValue
                    values[posNextRing + 1] += currValue
                    if (stepsToCorner == 1 && pos != high - 1) {
                        // before corner, except for last corner
                        values[pos+2] += currValue
                    }
                    if (pos == low) {
                        // first position of ring
                        values[high-1] += currValue
                        values[high] += currValue
                    }
                    posNextRing += 1
                }
            }

            // next ring
            squareIdx += 1
            low = nextLow
            high = nextHigh
            squareLength = nextSquareLength
        }
    }

    override fun computeSolution(input: List<String>) : String {
        val pos = input[0].toInt()
        val value = pos

        val output = StringBuilder()
        val numSteps = computeMemorySteps(pos)
        output.append("number of steps: $numSteps")
        output.append("\n\n")

        val firstLargerPos = computeFirstLargerPosition(value)
        output.append("first larger value: $firstLargerPos")
        return output.toString()
    }
}