package de.knikiel.aoc2007.days

import de.knikiel.aoc2007.days.Day10.computeDenseAsciiHash
import de.knikiel.aoc2007.days.Day10.computeFinalLenghts
import de.knikiel.aoc2007.days.Day10.knotArray
import de.knikiel.aoc2007.days.Day10.parseAsciiCharacters
import de.knikiel.aoc2007.days.Day10.shiftArray

object Day14 : Day {

    fun computeLineHash(lineString: String) : String {
        val intArray = parseAsciiCharacters(lineString)
        val hashInput = computeFinalLenghts(intArray)
        val array = IntArray(256) { it }
        val oldStartIdx = knotArray(array, hashInput)
        val arrayCopy = array.copyOf()
        shiftArray(arrayCopy, array, oldStartIdx)
        val lineHash = computeDenseAsciiHash(array)
        return lineHash
    }

    fun computeMemoryHash(key: String, numLines: Int = 128) : String {
        val hash = StringBuilder()
        for (lineIdx in 0 until numLines) {
            hash.append(computeLineHash(key + "-" + lineIdx))
        }
        return hash.toString()
    }

    fun toBits(hexString: String) : String {
        return hexString.map {
            when (it) {
                '0' -> "0000"
                '1' -> "0001"
                '2' -> "0010"
                '3' -> "0011"
                '4' -> "0100"
                '5' -> "0101"
                '6' -> "0110"
                '7' -> "0111"
                '8' -> "1000"
                '9' -> "1001"
                'a' -> "1010"
                'b' -> "1011"
                'c' -> "1100"
                'd' -> "1101"
                'e' -> "1110"
                'f' -> "1111"
                else -> "xxxx"
            }
        }.fold("", { acc, bits -> acc + bits } )
    }

    fun countBits(bitString: String) : Int {
        return bitString.filter { it == '1' }.count()
    }

    private fun fillBitfield(bitfield: Array<IntArray>, bitString: String) {
        for (i in 0 until bitString.length) {
            val row = i / 128
            val col = i % 128
            bitfield[row][col] = if (bitString[i] == '1') {
                1
            }
            else {
                0
            }
        }
    }


    fun countRegions(bitString: String): Int {
        val bitfield = Array<IntArray>(128, { IntArray(128, {Int.MIN_VALUE}) })
        fillBitfield(bitfield, bitString)

        // fill grid graph
        val graph = Day12.Graph(128 * 128)
        for (row in 0 until 128) {
            for (col in 0 until 128) {
                if (bitfield[row][col] == 1) {
                    if (row > 0 && bitfield[row-1][col] == 1) {
                        graph.addUndirectedEdge((row - 1) * 128 + col, row * 128 + col)
                    }
                    if (col > 0 && bitfield[row][col-1] == 1) {
                        graph.addUndirectedEdge(row * 128 + col - 1, row * 128 + col)
                    }
                }
            }
        }

        val numZeros = 128 * 128 - countBits(bitString)
        val numRegions = Day12.computeNumberConnectedComponents(graph) - numZeros

        return numRegions
    }

    override fun computeSolution(input: List<String>): String {
        val hexString = computeMemoryHash(input[0])
        val bitString = toBits(hexString)

        val numBits = countBits(bitString)
        val numRegions = countRegions(bitString)

        val output = StringBuilder()
        output.append("number of bits: $numBits\n\n")
        output.append("number of regions: $numRegions\n")
        return output.toString()
    }

}