package de.knikiel.aoc2007.days

object Day16 : Day {

    sealed class Instruction {
        data class Spin(val count: Int) : Instruction()
        data class Exchange(val pos1: Int, val pos2: Int) : Instruction()
        data class Partner(val char1: Char, val char2: Char) : Instruction()

        companion object {
            fun parse(string: String) : Instruction {
                when (string[0]) {
                    's' -> {
                        val count = string.substring(1).toInt()
                        return Spin(count)
                    }
                    'x' -> {
                        val substring = string.substring(1)
                        val positions = substring.split("/").map { it.toInt() }
                        return Exchange(positions[0], positions[1])
                    }
                    'p' -> {
                        return Partner(string[1], string[3])
                    }
                    else -> {}
                }
                return Spin(-1)
            }
        }
    }

    fun step(programs: CharArray, instruction: Instruction) {
        when (instruction) {
            is Instruction.Spin -> {
                val count = instruction.count
                val copy = programs.copyOf()
                val size = programs.size
                for (i in 0 until count) {
                    programs[i] = copy[size - count + i]
                }
                for (i in 0 until size - count) {
                    programs[i + count] = copy[i]
                }
            }
            is Instruction.Exchange -> {
                val pos1 = instruction.pos1
                val pos2 = instruction.pos2
                val swap = programs[pos1]
                programs[pos1] = programs[pos2]
                programs[pos2] = swap
            }
            is Instruction.Partner -> {
                val char1 = instruction.char1
                val char2 = instruction.char2
                programs.forEachIndexed {i, char ->
                    if (char == char1) {
                        programs[i] = char2
                    }
                    else if (char == char2) {
                        programs[i] = char1
                    }
                }
            }
        }
    }

    fun generatePrograms(count: Int) : CharArray {
        return (0 until count).map { 'a' + it }.toCharArray()
    }

    fun danceAllSteps(programs: CharArray, instructions: Array<Instruction>) {
        for (instruction in instructions) {
            step(programs, instruction)
        }
    }

    override fun computeSolution(input: List<String>): String {
        val instructions = input[0].split(",").map { Instruction.parse(it) }.toTypedArray()
        val programs = generatePrograms(16)
        var programsCopy = programs.joinToString("")

        danceAllSteps(programs, instructions)
        val programsString = programs.joinToString("")

        val cache = mutableMapOf<String, String>()
        for (i in 0 until 1_000_000_000) {
            val origPrograms = programsCopy
            if (cache.containsKey(origPrograms)) {
                programsCopy = cache[origPrograms]!!
            } else {
                val programs = programsCopy.map { it }.toCharArray()
                danceAllSteps(programs, instructions)
                val laterPrograms = programs.joinToString("")
                cache[origPrograms] = laterPrograms
                programsCopy = laterPrograms
            }
        }
        val lastProgramsString = programsCopy

        val output = StringBuilder()
        output.append("Programs at end: $programsString\n\n")
        output.append("Programs at the very end: $lastProgramsString\n")
        return output.toString()
    }
}