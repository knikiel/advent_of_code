package de.knikiel.aoc2007.days

object Day15 : Day {

    class Generator(var value: Long, val factor: Int) {

        fun next() : Int {
            value = (value * factor) % 2147483647
            return value.toInt()
        }

        fun nextHard(divisor: Int) : Int {
            while (next() % divisor != 0) {}
            return value.toInt()
        }

        companion object {

            val GENERATOR1_FACTOR = 16807
            val GENERATOR2_FACTOR = 48271

            val GENERATOR1_DIVISOR = 4
            val GENERATOR2_DIVISOR = 8

            fun countLowerWordMatches(gen1: Generator, gen2: Generator) : Int {
                var value1 = gen1.value
                var value2 = gen2.value
                if ((value1 xor value2) and 0xFFFF == 0L) {
                    return 1
                }
                return 0
            }

            fun countLowerWordMatches(startValue1: Long , startValue2: Long, numIterations: Int) : Int {
                val gen1 = Generator(startValue1, GENERATOR1_FACTOR)
                val gen2 = Generator(startValue2, GENERATOR2_FACTOR)

                val count = (0 until numIterations).asSequence().map {
                    gen1.next()
                    gen2.next()
                    Generator.countLowerWordMatches(gen1, gen2)
                }.filter { it == 1 }.count()

                return count
            }

            fun countLowerWordMatchesHard(startValue1: Long , startValue2: Long, numIterations: Int) : Int {
                val gen1 = Generator(startValue1, GENERATOR1_FACTOR)
                val gen2 = Generator(startValue2, GENERATOR2_FACTOR)

                val count = (0 until numIterations).asSequence().map {
                    gen1.nextHard(GENERATOR1_DIVISOR)
                    gen2.nextHard(GENERATOR2_DIVISOR)
                    Generator.countLowerWordMatches(gen1, gen2)
                }.filter { it == 1 }.count()

                return count
            }
        }
    }

    override fun computeSolution(input: List<String>) : String {
        val startValue1 = input[0].split(" ")[4].toLong()
        val startValue2 = input[1].split(" ")[4].toLong()

        val count = Generator.countLowerWordMatches(startValue1, startValue2, 40_000_000)
        val countHard = Generator.countLowerWordMatchesHard(startValue1, startValue2, 5_000_000)

        val output = StringBuilder()
        output.append("Number of matching lower words: $count\n\n")
        output.append("Number of matching lower words (hard): $countHard\n")

        return output.toString()
    }
}