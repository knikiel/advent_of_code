import de.knikiel.aoc2007.days.*
import java.nio.file.Files
import java.nio.file.Paths

fun main(args: Array<String>) {
    if (args.size == 0) {
        print("Need a day [1-24] as first parameter!")
        return
    }
    val day = Integer.parseInt(args[0])
    val has_input = (args.size == 2)

    val input =
        if (!has_input) {
            val path =  Paths.get("input/day$day.txt")
            Files.readAllLines(path)
        } else {
            val list = ArrayList<String>()
            list.add(args[1])
            list
        }

    try {
        val output = when (day) {
            1 -> Day1.computeSolution(input)
            2 -> Day2.computeSolution(input)
            3 -> Day3.computeSolution(input)
            4 -> Day4.computeSolution(input)
            5 -> Day5.computeSolution(input)
            6 -> Day6.computeSolution(input)
            7 -> Day7.computeSolution(input)
            8 -> Day8.computeSolution(input)
            9 -> Day9.computeSolution(input)
            10 -> Day10.computeSolution(input)
            11 -> Day11.computeSolution(input)
            12 -> Day12.computeSolution(input)
            13 -> Day13.computeSolution(input)
            14 -> Day14.computeSolution(input)
            //15 -> Day15.computeSolution(input)
            16 -> Day16.computeSolution(input)
            else -> "NOT A USEFUL DAY, SIR!"
        }
        println(output)
    }
    catch (e: Exception) {
        println("Error: Something is wrong with the input.")
    }
}