package de.knikiel.aoc2007.days

import de.knikiel.aoc2007.days.Day10.computeDenseAsciiHash
import de.knikiel.aoc2007.days.Day10.computeFinalLenghts
import de.knikiel.aoc2007.days.Day10.knotArray
import de.knikiel.aoc2007.days.Day10.parseAsciiCharacters
import org.junit.Test

import org.junit.Assert.*

class Day10Test {

    @Test
    fun reverseSubArray() {
        val array = intArrayOf(0, 1, 2, 3, 4)
        Day10.reverseSubArray(array, 3)
        assertArrayEquals(intArrayOf(2, 1, 0, 3, 4), array)
    }

    @Test
    fun shiftArray() {
        val array = intArrayOf(0, 1, 2, 3, 4)
        val destArray = intArrayOf(0, 0, 0, 0, 0)
        Day10.shiftArray(array, destArray, 3)
        assertArrayEquals(intArrayOf(3, 4, 0, 1, 2), destArray)
    }

    @Test
    fun knotArray() {
        val array = intArrayOf(0, 1, 2, 3, 4)
        val lengths = intArrayOf(3, 4, 1, 5)
        val oldStartIdx = Day10.knotArray(array, lengths)
        assertEquals(1, oldStartIdx)
        assertArrayEquals(intArrayOf(0, 3, 4, 2, 1), array)
    }

    @Test
    fun computeHash() {
        val array = intArrayOf(0, 3, 4, 2, 1)
        val hashValue = Day10.computeHash(array, 1)
        assertEquals(12, hashValue)
    }

    @Test
    fun parseAsciiCharacters() {
        val asciiCodes = Day10.parseAsciiCharacters("1,2,3")
        val solution = intArrayOf(49,44,50,44,51)
        assertArrayEquals(solution, asciiCodes)
    }

    @Test
    fun denseHashComputation() {
        val otherArray = IntArray(256) { it }
        val asciiLengths = parseAsciiCharacters("")
        assertEquals(0, asciiLengths.size)
        val finalLengths = computeFinalLenghts(asciiLengths)
        assertEquals(64 * 5, finalLengths.size)

        val oldStartIdx = knotArray(otherArray, finalLengths)
        assertTrue(oldStartIdx > 0)
        val arrayCopy = otherArray.copyOf()
        Day10.shiftArray(arrayCopy, otherArray, oldStartIdx)
        val denseAsciiHash = computeDenseAsciiHash(otherArray)
        assertEquals(32, denseAsciiHash.length)
        assertEquals("a2582a3a0e66e6e86e3812dcb672a272", denseAsciiHash)
    }
}