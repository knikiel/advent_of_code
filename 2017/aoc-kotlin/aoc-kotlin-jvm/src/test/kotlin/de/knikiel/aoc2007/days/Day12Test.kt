package de.knikiel.aoc2007.days

import org.junit.Test

import org.junit.Assert.*

class Day12Test {

    @Test
    fun computeConnectedComponent() {
        val graphLines = listOf(
                "0 <-> 2",
                "1 <-> 1",
                "2 <-> 0, 3, 4",
                "3 <-> 2, 4",
                "4 <-> 2, 3, 6",
                "5 <-> 6",
                "6 <-> 4, 5"
        )
        val graph = Day12.Graph.parse(graphLines)
        val numNodesInCC = Day12.computeConnectedComponent(graph, 0).size
        assertEquals(6, numNodesInCC)
    }
}