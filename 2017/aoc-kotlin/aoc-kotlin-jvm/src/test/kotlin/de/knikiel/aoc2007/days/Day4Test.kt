package de.knikiel.aoc2007.days

import org.junit.Test

import org.junit.Assert.*

class Day4Test {

    @Test
    fun countValidPhrases() {
        assertEquals(1,
                Day4.countValidPhrases(listOf(listOf("aa", "bb", "cc", "dd", "ee"))))
        assertEquals(0,
                Day4.countValidPhrases(listOf(listOf("aa", "bb", "cc", "dd", "aa"))))
        assertEquals(1,
                Day4.countValidPhrases(listOf(listOf("aa", "bb", "cc", "dd", "aaa"))))
    }

    @Test
    fun countValidAnagramPhrases() {
        assertEquals(1,
                Day4.countValidAnagramPhrases(listOf(listOf("abcde", "fghij"))))
        assertEquals(0,
                Day4.countValidAnagramPhrases(listOf(listOf("abcde", "xyz", "ecdab"))))
        assertEquals(1,
                Day4.countValidAnagramPhrases(listOf(listOf("a", "ab", "abc", "abd", "abf", "abj"))))
        assertEquals(1,
                Day4.countValidAnagramPhrases(listOf(listOf("iiii", "oiii", "ooii", "oooi", "oooo"))))
        assertEquals(0,
                Day4.countValidAnagramPhrases(listOf(listOf("oiii", "ioii", "iioi", "iiio"))))
    }
}