package de.knikiel.aoc2007.days

import org.junit.Test

import org.junit.Assert.*

class Day13Test {
    val testInput = arrayOf(
            Day13.Scanner(0, 3),
            Day13.Scanner(1, 2),
            Day13.Scanner(4, 4),
            Day13.Scanner(6, 4)
    )

    @Test
    fun computeSeverity() {
        val severity = Day13.computeSeverity(testInput)
        assertEquals(24, severity)
    }

    @Test
    fun computeTotalDelay() {
        val delay = Day13.computeTotalDelay(testInput)
        assertEquals(10, delay)
    }
}