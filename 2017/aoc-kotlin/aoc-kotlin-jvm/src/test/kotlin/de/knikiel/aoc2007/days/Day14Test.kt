package de.knikiel.aoc2007.days

import org.junit.Test

import org.junit.Assert.*

class Day14Test {

    @Test
    fun computeLineHash() {
        val lineHash = Day14.computeLineHash("flqrgnkx-0")
        assertEquals(32, lineHash.length)
    }

    @Test
    fun toBits() {
        val lineHash = Day14.computeLineHash("flqrgnkx-0")
        val bitString = Day14.toBits(lineHash)
        assertEquals(128, bitString.length)
    }

    @Test
    fun countBits() {
        val linesHash = Day14.computeMemoryHash("flqrgnkx")
        val bitString = Day14.toBits(linesHash)
        val numBits = Day14.countBits(bitString)
        assertEquals(8108, numBits)
    }

    @Test
    fun countRegions() {
        val linesHash = Day14.computeMemoryHash("flqrgnkx")
        val bitString = Day14.toBits(linesHash)
        val numRegions = Day14.countRegions(bitString)
        assertEquals(1242, numRegions)
    }
}