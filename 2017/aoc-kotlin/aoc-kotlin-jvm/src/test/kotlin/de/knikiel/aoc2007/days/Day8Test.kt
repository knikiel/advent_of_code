package de.knikiel.aoc2007.days

import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

class Day8Test {
    private val instructions = arrayOf(
        Day8.Instruction(Day8.Operation.INCREMENT, "b", 5,
                Day8.Comparison.GREATER, "a", 1),
        Day8.Instruction(Day8.Operation.INCREMENT, "a", 1,
                Day8.Comparison.LESS, "b", 5),
        Day8.Instruction(Day8.Operation.DECREMENT, "c", -10,
                Day8.Comparison.GEQ, "a", 1),
        Day8.Instruction(Day8.Operation.INCREMENT, "c", -20,
                Day8.Comparison.EQUAL, "c", 10)
    )

    @Test
    fun testInstructionRegex() {
        val instructionLine = "b inc 5 if a > 1"
        assertNotNull(Day8.INSTRUCTION_REGEX.matchEntire(instructionLine))
    }

    @Test
    fun parseProgram() {
        val input = listOf(
            "b inc 5 if a > 1",
            "a inc 1 if b < 5",
            "c dec -10 if a >= 1",
            "c inc -20 if c == 10"
        )
        val readInstructions = Day8.parseProgram(input)
        assertArrayEquals(instructions, readInstructions)
    }

    @Test
    fun executeInstruction() {
        val registers = HashMap<String, Int>()
        Day8.executeInstruction(registers, instructions[0])
        assertEquals(0, registers["a"])
        assertEquals(0, registers["b"])

        Day8.executeInstruction(registers, instructions[1])
        assertEquals(1, registers["a"])
    }

    @Test
    fun executeProgram() {
        val registers = Day8.executeProgram(instructions)
        assertEquals(1, registers["a"])
        assertEquals(0, registers["b"])
        assertEquals(-10, registers["c"])
    }

    @Test
    fun searchMaxValue() {
        val registers = Day8.executeProgram(instructions)
        assertEquals(1, Day8.searchMaxValue(registers))
    }
}