package de.knikiel.aoc2007.days

import org.junit.Test

import org.junit.Assert.*

class Day6Test {
    @Test
    fun nextStep() {
        val memoryBlocks = intArrayOf(0, 2, 7, 0)

        Day6.nextStep(memoryBlocks)
        assertEquals(9, memoryBlocks.sum())
        assertArrayEquals(intArrayOf(2, 4, 1, 2), memoryBlocks)

        Day6.nextStep(memoryBlocks)
        assertEquals(9, memoryBlocks.sum())
        assertArrayEquals(intArrayOf(3, 1, 2, 3), memoryBlocks)


        Day6.nextStep(memoryBlocks)
        Day6.nextStep(memoryBlocks)
        Day6.nextStep(memoryBlocks)
        assertArrayEquals(intArrayOf(2, 4, 1, 2), memoryBlocks)
    }

    @Test
    fun countCycles() {
        val memoryBlocks = intArrayOf(0, 2, 7, 0)
        val cycleInfo = Day6.countCycles(memoryBlocks)

        assertEquals(5, cycleInfo.numberCycles)
        assertEquals(4, cycleInfo.lengthLoop)
    }
}