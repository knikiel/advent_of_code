package de.knikiel.aoc2007.days

import org.junit.Assert.assertEquals
import org.junit.Test

internal class Day1Test {
    @Test
    fun computeChecksum() {
        assertEquals(
                3, Day1.computeChecksum(listOf(1, 1, 2, 2)))
        assertEquals(
                4, Day1.computeChecksum(listOf(1, 1, 1, 1)))
        assertEquals(
                4, Day1.computeChecksum(listOf(1, 1, 1, 1)))
        assertEquals(
                0, Day1.computeChecksum(listOf(1, 2, 3, 4)))
        assertEquals(
                9, Day1.computeChecksum(listOf(9, 1, 2, 1, 2, 1, 2, 9)))
    }

    @Test
    fun computeDifficultChecksum() {
        assertEquals(
                6, Day1.computeDifficultChecksum(listOf(1, 2, 1, 2)))
        assertEquals(
                0, Day1.computeDifficultChecksum(listOf(1, 2, 2, 1)))
        assertEquals(
                4, Day1.computeDifficultChecksum(listOf(1, 2, 3, 4, 2, 5)))
        assertEquals(
                12, Day1.computeDifficultChecksum(listOf(1, 2, 3, 1, 2, 3)))
        assertEquals(
                4, Day1.computeDifficultChecksum(listOf(1, 2, 1, 3, 1, 4, 1, 5)))
    }
}