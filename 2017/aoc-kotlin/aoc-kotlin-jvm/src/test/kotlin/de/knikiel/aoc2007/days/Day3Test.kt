package de.knikiel.aoc2007.days

import org.junit.Test

import org.junit.Assert.*

class Day3Test {

    @Test
    fun computeMemorySteps() {
        assertEquals(0, Day3.computeMemorySteps(1))
        assertEquals(2, Day3.computeMemorySteps(3))
        assertEquals(3, Day3.computeMemorySteps(12))
        assertEquals(2, Day3.computeMemorySteps(23))
        assertEquals(31, Day3.computeMemorySteps(1024))
    }

    /*
    @Test
    fun computeValue() {
        assertEquals(1, Day3.computeValue(1))
        assertEquals(1, Day3.computeValue(2))
        assertEquals(2, Day3.computeValue(3))
        assertEquals(4, Day3.computeValue(4))
        assertEquals(5, Day3.computeValue(5))
    }
    */
}