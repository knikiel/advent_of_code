package de.knikiel.aoc2007.days

import org.junit.Test

import org.junit.Assert.*

class Day2Test {
    @Test
    fun computeChecksum() {
        val row1 = listOf(5, 1, 9, 5)
        val row2 = listOf(7, 5, 3)
        val row3 = listOf(2, 4, 6, 8)
        assertEquals(8, Day2.computeChecksum(listOf(row1)))
        assertEquals(4, Day2.computeChecksum(listOf(row2)))
        assertEquals(6, Day2.computeChecksum(listOf(row3)))
        assertEquals(18, Day2.computeChecksum(listOf(row1, row2, row3)))
    }

    @Test
    fun computeDifficultChecksum() {
        val row1 = listOf(5, 9, 2, 8)
        val row2 = listOf(9, 4, 7, 3)
        val row3 = listOf(3, 8, 6, 5)
        assertEquals(4, Day2.computeDifficultChecksum(listOf(row1)))
        assertEquals(3, Day2.computeDifficultChecksum(listOf(row2)))
        assertEquals(2, Day2.computeDifficultChecksum(listOf(row3)))
        assertEquals(9, Day2.computeDifficultChecksum(listOf(row1, row2, row3)))
    }
}