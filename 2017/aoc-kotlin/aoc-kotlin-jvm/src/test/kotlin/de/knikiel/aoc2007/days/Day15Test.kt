package de.knikiel.aoc2007.days

import de.knikiel.aoc2007.days.Day15.Generator
import de.knikiel.aoc2007.days.Day15.Generator.Companion.GENERATOR1_DIVISOR
import de.knikiel.aoc2007.days.Day15.Generator.Companion.GENERATOR1_FACTOR
import de.knikiel.aoc2007.days.Day15.Generator.Companion.GENERATOR2_DIVISOR
import de.knikiel.aoc2007.days.Day15.Generator.Companion.GENERATOR2_FACTOR
import org.junit.Test
import kotlin.test.assertEquals

class Day15Test {
    @Test
    fun testGeneratedSequence() {
        val gen1 = Generator(65, GENERATOR1_FACTOR)
        val gen2 = Generator(8921, GENERATOR2_FACTOR)

        // test generated sequences
        val seq1 = (0..4).map { gen1.next() }
        val seq2 = (0..4).map { gen2.next() }

        assertEquals(arrayListOf(1092455, 1181022009, 245556042, 1744312007, 1352636452), seq1)
        assertEquals(arrayListOf(430625591, 1233683848, 1431495498, 137874439, 285222916), seq2)
    }

    @Test
    fun testLowerWordCounting() {
        val gen1 = Generator(65, GENERATOR1_FACTOR)
        val gen2 = Generator(8921, GENERATOR2_FACTOR)

        // test matching lower words
        val count = Generator.countLowerWordMatches(65, 8921, 5)

        assertEquals(1, count)
    }

    @Test
    fun testGeneratedSequenceHard() {
        val gen1 = Generator(65, GENERATOR1_FACTOR)
        val gen2 = Generator(8921, GENERATOR2_FACTOR)

        // test generated sequences
        val seq1 = (0..4).map { gen1.nextHard(GENERATOR1_DIVISOR) }
        val seq2 = (0..4).map { gen2.nextHard(GENERATOR2_DIVISOR) }

        assertEquals(arrayListOf(1352636452, 1992081072, 530830436, 1980017072, 740335192), seq1)
        assertEquals(arrayListOf(1233683848, 862516352, 1159784568, 1616057672, 412269392), seq2)
    }

    @Test
    fun testLowerWordCountingHard() {
        val gen1 = Generator(65, GENERATOR1_FACTOR)
        val gen2 = Generator(8921, GENERATOR2_FACTOR)

        // test matching lower words
        val count = Generator.countLowerWordMatchesHard(65, 8921, 5)

        assertEquals(0, count)
    }
}