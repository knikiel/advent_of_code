package de.knikiel.aoc2007.days

import org.junit.Test

import org.junit.Assert.*

class Day11Test {
    val NW = Day11.Direction.NW
    val N = Day11.Direction.N
    val NE = Day11.Direction.NE
    val SW = Day11.Direction.SW
    val S = Day11.Direction.S
    val SE = Day11.Direction.SE

    @Test
    fun followDirections() {
        var directions = listOf(NE, NE, NE)
        var position = Day11.followDirections(directions)
        var distance = position.getDistanceToCenter()
        assertEquals(3, distance)

        directions = listOf(NE, NE, SW, SW)
        position = Day11.followDirections(directions)
        distance = position.getDistanceToCenter()
        assertEquals(0, distance)

        directions = listOf(SE, SW, SE, SW, SW)
        position = Day11.followDirections(directions)
        distance = position.getDistanceToCenter()
        assertEquals(3, distance)
    }
}