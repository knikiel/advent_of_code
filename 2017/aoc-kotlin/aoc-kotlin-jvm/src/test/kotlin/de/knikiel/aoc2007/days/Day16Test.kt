package de.knikiel.aoc2007.days

import org.junit.Test

import org.junit.Assert.*

class Day16Test {

    @Test
    fun danceAllSteps() {
        val programs = Day16.generatePrograms(5)
        val instructions = arrayOf(
                Day16.Instruction.Spin(1),
                Day16.Instruction.Exchange(3, 4),
                Day16.Instruction.Partner('e', 'b')
        )
        Day16.danceAllSteps(programs, instructions)

        assertArrayEquals("baedc".toCharArray(), programs)
    }
}