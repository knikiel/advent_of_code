package de.knikiel.aoc2007.days

import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

class Day7Test {
    var rules: Array<Day7.Rule>? = null

    @Before
    fun before() {
        val input =
                listOf("pbga (66)", "xhth (57)", "ebii (61)", "havc (66)", "ktlj (57)",
                        "fwft (72) -> ktlj, cntj, xhth", "qoyq (66)", "padx (45) -> pbga, havc, qoyq",
                        "tknk (41) -> ugml, padx, fwft", "jptl (61)", "ugml (68) -> gyxo, ebii, jptl",
                        "gyxo (61)", "cntj (57)")
        rules = Day7.readRules(input)
    }

    @Test
    fun readRules() {
        val rule1 = Day7.Rule("fwft", 72, arrayOf("ktlj", "cntj", "xhth"))
        val rule2 = Day7.Rule("ebii", 61, Array(0, {""}))

        var rule1Found = false
        var rule2Found = false
        for (rule in rules!!) {
            if (rule == rule1) {
                rule1Found = true
            }
            if (rule == rule2) {
                rule2Found = true
            }
        }
        assertTrue(rule1Found)
        assertTrue(rule2Found)
    }

    @Test
    fun searchBottom() {
        val bottom = Day7.searchBottom(rules!!)
        assertEquals("tknk", bottom)
    }

    @Test
    fun determineCorrectWeight() {
        val tree = Day7.Tree.create(rules!!)
        val correctWeight = Day7.determineCorrectWeight(tree)
        assertEquals(60, correctWeight)
    }
}