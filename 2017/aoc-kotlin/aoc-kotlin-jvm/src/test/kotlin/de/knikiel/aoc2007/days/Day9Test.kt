package de.knikiel.aoc2007.days

import org.junit.Test

import org.junit.Assert.*

class Day9Test {

    @Test
    fun parseGroup() {
        var testString = "{}"
        var endIdx = Day9.parseGroup(testString, 0).idx
        assertEquals(testString.length-1, endIdx)
        testString = "{{},{}}"
        endIdx = Day9.parseGroup(testString, 0).idx
        assertEquals(testString.length-1, endIdx)
        testString = "{<{},{{}}>}"
        endIdx = Day9.parseGroup(testString, 0).idx
        assertEquals(testString.length-1, endIdx)
        testString = "{{<!>},{<!>},{<!>},{<a>}}"
        endIdx = Day9.parseGroup(testString, 0).idx
        assertEquals(testString.length-1, endIdx)
    }

    @Test
    fun parseGarbage() {
        var testString = "<>"
        var endIdx = Day9.parseGarbage(testString, 0).idx
        assertEquals(testString.length-1, endIdx)
        testString = "<dsfnäajdsfdjskf9>"
        endIdx = Day9.parseGarbage(testString, 0).idx
        assertEquals(testString.length-1, endIdx)
        testString = "<<<<<>"
        endIdx = Day9.parseGarbage(testString, 0).idx
        assertEquals(testString.length-1, endIdx)
        testString = "<!!!>>"
        endIdx = Day9.parseGarbage(testString, 0).idx
        assertEquals(testString.length-1, endIdx)
    }

    @Test
    fun computeScore() {
        var testString = "{}"
        var tree = Day9.parse(testString)
        var score = Day9.computeScore(tree)
        print(tree)
        assertEquals(1, score)

        testString = "{{},{}}"
        tree = Day9.parse(testString)
        score = Day9.computeScore(tree)
        assertEquals(5, score)

        testString = "{<{},{{}}>}"
        tree = Day9.parse(testString)
        score = Day9.computeScore(tree)
        assertEquals(1, score)

        testString = "{{<!>},{<!>},{<!>},{<a>}}"
        tree = Day9.parse(testString)
        score = Day9.computeScore(tree)
        assertEquals(3, score)

        testString = "{{<!!>},{<!!>},{<!!>},{<!!>}}"
        tree = Day9.parse(testString)
        score = Day9.computeScore(tree)
        assertEquals(9, score)

        testString = "{{<a!>},{<a!>},{<a!>},{<ab>}}"
        tree = Day9.parse(testString)
        score = Day9.computeScore(tree)
        assertEquals(3, score)
    }

    @Test
    fun countGarbage() {
        var testString = "<>"
        var tree = Day9.parseGarbage(testString, 0).tree
        var count = Day9.countGarbage(tree)
        assertEquals(0, count)

        testString = "<<<<>"
        tree = Day9.parseGarbage(testString, 0).tree
        count = Day9.countGarbage(tree)
        assertEquals(3, count)

        testString = "<!!!>>"
        tree = Day9.parseGarbage(testString, 0).tree
        count = Day9.countGarbage(tree)
        assertEquals(0, count)
    }
}