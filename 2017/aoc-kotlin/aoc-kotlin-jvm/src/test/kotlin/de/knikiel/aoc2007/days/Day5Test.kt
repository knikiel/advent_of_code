package de.knikiel.aoc2007.days

import org.junit.Test

import org.junit.Assert.*

class Day5Test {

    @Test
    fun searchExit() {
        val instructions = intArrayOf(0, 3, 0, 1, -3)
        val numSteps = Day5.searchExit(instructions)
        assertEquals(5, numSteps)
        assertArrayEquals(intArrayOf(2, 5, 0, 1, -2), instructions)
    }

    @Test
    fun searchExitDifficult() {
        val instructions = intArrayOf(0, 3, 0, 1, -3)
        val numSteps = Day5.searchExitDifficult(instructions)
        assertEquals(10, numSteps)
        assertArrayEquals(intArrayOf(2, 3, 2, 3, -1), instructions)
    }
}