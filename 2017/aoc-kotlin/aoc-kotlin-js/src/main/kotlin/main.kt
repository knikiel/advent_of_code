import de.knikiel.aoc2007.days.*
import org.w3c.dom.HTMLDivElement
import org.w3c.dom.HTMLInputElement
import org.w3c.dom.HTMLTextAreaElement
import kotlin.browser.document

fun main(args: Array<String>) {
    val dayField = document.getElementById("day-field") as HTMLInputElement
    val inputField = document.getElementById("input-field") as HTMLTextAreaElement
    val outputField = document.getElementById("message-box") as HTMLDivElement
    val button = document.getElementById("submit") as HTMLInputElement

    button.onclick = {
        try {
            val day = dayField.value.toInt()
            val input = inputField.value.split("\n")
            val output = when (day) {
                1 -> Day1.computeSolution(input)
                2 -> Day2.computeSolution(input)
                3 -> Day3.computeSolution(input)
                4 -> Day4.computeSolution(input)
                5 -> Day5.computeSolution(input)
                6 -> Day6.computeSolution(input)
                7 -> Day7.computeSolution(input)
                8 -> Day8.computeSolution(input)
                9 -> Day9.computeSolution(input)
                10 -> Day10.computeSolution(input)
                12 -> Day12.computeSolution(input)
                13 -> Day13.computeSolution(input)
                14 -> Day14.computeSolution(input)
                //15 -> Day15.computeSolution(input)
                16 -> Day16.computeSolution(input)
                else -> "NOT A GOOD DAY, SIR!"
            }
            outputField.innerText = output
        }
        catch (e: Exception) {
            outputField.innerText = "Error: Something wrong with input."
        }
        Unit
    }
}